# Fundación Providencia

## Colocar en servidor
* Borrar de public index.php y .htaccess
* Reemplazar .htaccesDeploy por .htaccess e indexDeploy.php por index.php
* Borrar de app/Providers AppServiceProvider.php
* Reemplazar AppServiceProviderDeploy.php por AppServiceProvider.php
* Cambiar el nombre la carpeta public por html
* Desde voyager hacer symlink si lo requiere 
* Cambiar el .env.example por .env



## Notas
* Este proyecto usa webpack(mix) para minificar varios archivos js, no olvidar instalar npm y correr npm run dev o npm run production al hacer cambios en el archivo main.js
* URL actual del [servidor](https://giveorg.digitalcoaster.mx)

This is a test

## Para Frontend
* Los estilos del proyecto están en archivos SCSS

## Tecnologías
* Laravel 5.6

## Instalación para desarrollo

1. Clonar el Proyecto
2. Ejecutar en la terminal ``` $ composer install``` dentro de la carpeta del proyecto
3. Ejecutar en la terminal ``` $ npm install``` dentro de la carpeta del proyecto


## Notas 

Por Razones ajenas a DC en este proyecto public se llama html por que si no no sirve en su servidor


Nota 

Para migrar breads locales 
````php artisan iseed data_types,data_rows,menus,menu_items,roles,permissions,permission_role,settings````


