<?php

namespace App\Exports;

use App\Model\Proyecto;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProyectoExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Proyecto::all();
    }
}
