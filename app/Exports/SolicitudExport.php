<?php

namespace App\Exports;

use App\Model\Solicitud;
use Maatwebsite\Excel\Concerns\FromCollection;

class SolicitudExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Solicitud::all();
    }
}
