<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

//Models
use App\Model\Proyecto;
use App\Model\Donation;
use App\Model\Donor;

class AdminController extends Controller
{
    public function proyectos(Request $request){
        $res = array();
        $res['status'] = 200;
        
        $projects = Proyecto::where('name', 'like', '%' . $request->searchQuery . '%')->where('is_visible', 1)->get();
        if(!$projects){
            $res['msg'] = 'no_results';
            return response()->json($res);
        }

        $res['msg'] = 'search_results';
        $res['content']['projects'] = $projects;
        return response()->json($res);
    }

    public function donaciones(Request $request){
        $res = array();
        $donationsNew = array();
        $res['status'] = 200;
        //fromDate toDate
        if($request->fromDate!='' && $request->fromDate!=null && $request->toDate!='' && $request->toDate!=null){
            $date1 = new Carbon($request->fromDate);
            $date2 = new Carbon($request->toDate);

            $donations = Donation::whereBetween('created_at', [$date1, $date2])
            ->where(function($query) use($request) {
                $query ->orWhere('authorization', 'like', '%' . $request->searchQuery . '%')
                ->orWhere('processor_id', 'like', '%' . $request->searchQuery . '%');
             })->get();
        }else{
            $donations = Donation::where('authorization', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('processor_id', 'like', '%' . $request->searchQuery . '%')
            ->get();
        }
        
        if(count($donations)!=0){
            if($request->rfc){
                foreach($donations as $don){
                    if(!is_null($don->rfc)){
                        $don->project;
                        $don->donor;
                        array_push($donationsNew, $don);
                    }
                }
            }else{
                foreach($donations as $don){
                    $don->project;
                    $don->donor;
                    array_push($donationsNew, $don);
                }
            }

            $res['rfc']= $request->rfc;
            $res['msg'] = 'search_results';
            $res['content']['donations'] = $donationsNew;
            return response()->json($res);
        }

        if($request->fromDate!='' && $request->fromDate!=null && $request->toDate!='' && $request->toDate!=null){
            $donors = Donor::whereBetween('created_at', [$date1, $date2])
            ->where(function($query) use($request) {
                $query->orWhere('name', 'like', '%' . $request->searchQuery . '%')
                    ->orWhere('lastname', 'like', '%' . $request->searchQuery . '%')
                    ->orWhere('email', 'like', '%' . $request->searchQuery . '%');
             })->get();
        }else{
            $donors = Donor::where('name', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('lastname', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('email', 'like', '%' . $request->searchQuery . '%')
            ->get();
        }
        

        if(count($donors)==0){
            $res['msg'] = 'no_results';
            return response()->json($res);
        }

        $searchDonations = array();
        foreach($donors as $dan){
            array_push($searchDonations, $dan->id);
        }

        $donations = Donation::whereIn('donor_id', $searchDonations)->get();

        if($donations){
            if($request->rfc){
                foreach($donations as $don){
                    if(!is_null($don->rfc)){
                        $don->project;
                        $don->donor;
                        array_push($donationsNew, $don);
                    }
                }
            }else{
                foreach($donations as $don){
                    $don->project;
                    $don->donor;
                    array_push($donationsNew, $don);
                }
            }
            
            $res['rfc']= $request->rfc;
            $res['msg'] = 'search_results';
            $res['content']['donations'] = $donationsNew;
            return response()->json($res);
        }else{
            $res['msg'] = 'no_results';
            return response()->json($res);
        }
    }

    public function donacionesProyecto(Request $request){
        $res = array();
        $donationsNew = array();
        $res['status'] = 200;
        //fromDate toDate
        if($request->fromDate!='' && $request->fromDate!=null && $request->toDate!='' && $request->toDate!=null){
            $date1 = new Carbon($request->fromDate);
            $date2 = new Carbon($request->toDate);

            $donations = Donation::whereBetween('created_at', [$date1, $date2])
            ->where(function($query) use($request) {
                $query ->orWhere('authorization', 'like', '%' . $request->searchQuery . '%')
                ->orWhere('processor_id', 'like', '%' . $request->searchQuery . '%');
             })->get();
        }else{
            $donations = Donation::where('authorization', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('processor_id', 'like', '%' . $request->searchQuery . '%')
            ->get();
        }
        
        if(count($donations)!=0){
            if($request->rfc){
                foreach($donations as $don){
                    if(!is_null($don->rfc)){
                        $don->project;
                        $don->donor;
                        if($don->project_id == $request->id){
                            array_push($donationsNew, $don);
                        } 
                    }
                }
            }else{
                foreach($donations as $don){
                    $don->project;
                    $don->donor;
                    if($don->project_id == $request->id){
                        array_push($donationsNew, $don);
                    } 
                }
            }

            $res['rfc']= $request->rfc;
            $res['id']= $request->id;
            $res['msg'] = 'search_results';
            $res['content']['donations'] = $donationsNew;
            return response()->json($res);
        }

        if($request->fromDate!='' && $request->fromDate!=null && $request->toDate!='' && $request->toDate!=null){
            $donors = Donor::whereBetween('created_at', [$date1, $date2])
            ->where(function($query) use($request) {
                $query->orWhere('name', 'like', '%' . $request->searchQuery . '%')
                    ->orWhere('lastname', 'like', '%' . $request->searchQuery . '%')
                    ->orWhere('email', 'like', '%' . $request->searchQuery . '%');
             })->get();
        }else{
            $donors = Donor::where('name', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('lastname', 'like', '%' . $request->searchQuery . '%')
            ->orWhere('email', 'like', '%' . $request->searchQuery . '%')
            ->get();
        }
        
        if(count($donors)==0){
            $res['msg'] = 'no_results';
            return response()->json($res);
        }

        $searchDonations = array();
        foreach($donors as $dan){
            array_push($searchDonations, $dan->id);
        }

        $donations = Donation::whereIn('donor_id', $searchDonations)->get();

        if($donations){
            if($request->rfc){
                foreach($donations as $don){
                    if(!is_null($don->rfc)){
                        $don->project;
                        $don->donor;
                        if($don->project_id == $request->id){
                            array_push($donationsNew, $don);
                        } 
                    }
                }
            }else{
                foreach($donations as $don){
                    $don->project;
                    $don->donor;
                    if($don->project_id == $request->id){
                        array_push($donationsNew, $don);
                    } 
                }
            }
            
            $res['rfc']= $request->rfc;
            $res['msg'] = 'search_results';
            $res['content']['donations'] = $donationsNew;
            return response()->json($res);
        }else{
            $res['msg'] = 'no_results';
            return response()->json($res);
        }
    }
    
    public function getProjectInfo(Request $r){
        setlocale(LC_MONETARY, 'en_US');
        $project = Proyecto::query()->where('id', $r->project_id)->first();

        $res = array();
        if ($project) {
            $donations = Donation::query()->where('project_id', $project->id)->where('status', 'completed')->get();
            foreach ($donations as $don) {
                $donor = Donor::find($don->donor_id);
                if ($donor) {
                    if(is_null($donor->lastname)){
                        $don->donor_name = $donor->name.' '.$donor->lastname;
                    }else{
                        $don->donor_name = $donor->name;
                    }
                    $don->donor_email = $donor->email;
                }
                $don->date_parsed = Carbon::parse($don->created_at)->setTimezone('America/Mexico_City')->format('d/m/Y h:i:s');
            }
            $res['status'] = 200;
            $res['msg'] = 'project_found';
            $res['content']['total_amount'] = $donations->sum('amount');
            $res['content']['total_donations'] = sizeof($donations);
            $res['content']['donations'] = $donations;
            $res['content']['project'] = $project;

        }
        return response()->json($res);

    }

    public function getProjectInfoDonacion(Request $r){
        setlocale(LC_MONETARY, 'en_US');
        $project = Proyecto::query()->where('id', $r->project_id)->first();
        $res = array();
        if ($project) {
            $donations = Donation::query()->where('project_id', $project->id)->where('status', 'completed')->get();
            foreach ($donations as $don) {
                $donor = Donor::find($don->donor_id);
                if ($donor) {
                    if(is_null($donor->lastname)){
                        $don->donor_name = $donor->name.' '.$donor->lastname;
                    }else{
                        $don->donor_name = $donor->name;
                    }
                    $don->donor_email = $donor->email;
                }
                $don->date_parsed = Carbon::parse($don->created_at)->setTimezone('America/Mexico_City')->format('d/m/Y h:i:s');
            }
            $res['status'] = 200;
            $res['msg'] = 'project_found';
            $res['content']['total_amount'] = $donations->sum('amount');
            $res['content']['total_donations'] = sizeof($donations);
            $res['content']['donations'] = $donations;
            $res['content']['project'] = $project;

        }
        return response()->json($res);

    }

    public function viewReceipt($don_id)
    {
        $donation = Donation::find($don_id);
        if (!$donation) {
            return ('<p style="font-family: Arial, Helvetica, sans-serif;">No existe recibo de esta transacción</p>');
        }
        $donation->donor_name = Donor::find($donation->donor_id)->name;
        $donation->donor_lastname = Donor::find($donation->donor_id)->lastname;
        $donation->donor_email = Donor::find($donation->donor_id)->email;
        $donation->project_name = Proyecto::find($donation->project_id)->name;
        $donation->date_parsed = Carbon::parse($donation->created_at)->setTimezone('America/Mexico_City')->format('d/m/Y h:i:s');

        return view('mails.paymentReceipt')->with('donation', $donation);
    }


}
