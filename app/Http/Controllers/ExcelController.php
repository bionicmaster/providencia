<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use App\Exports\DonationExport;
use App\Exports\NewsletterExport;
use App\Exports\ProyectoExport;
use App\Exports\SolicitudExport;

class ExcelController extends Controller
{
   public function donation($id){
    if($id=='all'){
        return Excel::download(new DonationExport(), 'donaciones.xlsx');
    }
   }

   public function newsletter($id){
    if($id=='all'){
        return Excel::download(new NewsletterExport(), 'newsletter.xlsx');
    }
    
   }

   public function proyecto($id){
    if($id=='all'){
        return Excel::download(new ProyectoExport(), 'proyectos.xlsx');
    }
   }

   public function solicitud($id){
    if($id=='all'){
        return Excel::download(new SolicitudExport(), 'solicitudes.xlsx');
    }
   }

}
