<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Model\Solicitud;

//Otros
use Carbon\Carbon;
use Mail;

class FormController extends Controller
{

    public function form(Request $r){

        $validatedData = $r->validate([
            'nombre' => 'required',
            'cargo' => 'required',
            'correo' => 'required',
            'razonSocial' => 'required',
            'figura' => 'required',
            'responsable' => 'required',
            'web' => 'required',
            'presidente' => 'required',
            'mision' => 'required',
            'organizacion' => 'required',
            'descripcion' => 'required',
            'hacienda' => 'required',
            'principales' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'extension' => 'required',
            'fecha_constitucion' => 'required',
            'rfc' => 'required|max:13',
            'autorizada' => 'required',
            'areas_interes' => 'required',
            'recursos_complementarios' => 'required',
            'acta_constitutiva' => 'required|file',
            'actas_notariales' => 'required',
        ]);

        $solicitud = new Solicitud();
        $solicitud->nombre=$r->input('nombre');
        $solicitud->cargo=$r->input('cargo');
        $solicitud->correo=$r->input('correo');
        $solicitud->razonSocial=$r->input('razonSocial');
        $solicitud->figura=$r->input('figura');
        $solicitud->responsable=$r->input('responsable');
        $solicitud->web=$r->input('web');
        $solicitud->presidente=$r->input('presidente');
        $solicitud->mision=$r->input('mision');
        $solicitud->organizacion=$r->input('organizacion');
        $solicitud->descripcion=$r->input('descripcion');
        $solicitud->principales=$r->input('principales');
        $solicitud->direccion=$r->input('direccion');
        $solicitud->telefono=$r->input('telefono');
        $solicitud->extension=$r->input('extension');
        $solicitud->fecha_constitucion=$r->input('fecha_constitucion');
        $solicitud->rfc=$r->input('rfc');
        $solicitud->autorizada=$r->input('autorizada');
        $solicitud->areas_interes=json_encode($r->input('areas_interes'));
        $solicitud->recursos_complementarios=$r->input('recursos_complementarios');
        $solicitud->hacienda=$this->processFile($r->file('hacienda'));
        $solicitud->acta_constitutiva=$this->processFile($r->file('acta_constitutiva'));
        $solicitud->actas_notariales=$this->processFile($r->file('actas_notariales'));
        $solicitud->save();

        $data['email']=$r->input('correo');
        $data['name'] =$r->input('nombre');

        //Corrreo de bienvenida
        Mail::send('mails.formulario', $data, function ($message) use($data) {
            $message->from('noreply@providencia.org.mx', 'Providencia');
            $message->to($data['email'], $data['name']);
            $message->subject('Gracias por registrarte');
        });

        Mail::send('mails.emisor', $data, function ($message) use($data) {
            $message->from('noreply@providencia.org.mx', 'Providencia');
            $message->to('contacto@providencia.org.mx', 'Administración Providencia');
            $message->subject('Nueva Solicitud');
        });

        $mensaje = 'Tu solicitud ha sido realizada con exito';

        return view('thanks',compact('mensaje'));
    }

    public function processFile($file){
        $folder='/storage/';
        $today = new Carbon();
        $date = $today->year . "/" . $today->month . "/".date("s"). "/";
        $image_extension = $file->getClientOriginalExtension();
        $image_name = $today->year.$today->month.date("H_i_s")."_tmp.".$image_extension;
        $file->move(public_path($folder . $date), $image_name);
        return $date . $image_name;
    }



}
