<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use App\Model\Donation;
use App\Model\Empresa;
use App\Model\Newsletter;
use App\Model\Organizacion;
use App\Model\Proyecto;
use App\Model\Video;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

//Models

//Para peticiones

class HomeController extends Controller
{
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->take(2)->get();

        $projects = Proyecto::where("is_visible", 1)
            ->where("is_live", 1)
            ->get();
        return view('home', ['projects' => $projects, 'blogs' => $blogs]);
    }

    public function newsletter(Request $request)
    {
        $data = request()->validate([
            'name' => [],
            'email' => ['required', 'email'],
        ], [
            'email.required' => 'Inserte un email',
            'email.email' => 'El valor ingresado no es un email',
        ]);

        Newsletter::create([
            'name' => $data['name'],
            'email' => $data['email'],
        ]);

        //Hago peticion
        $client = new Client(['timeout' => 30.0]);
        $response = $client->post('https://restapi.fromdoppler.com/accounts/gufles%40gmail.com/lists/27309365/subscribers?api_key=E7098FAB04A3545AAEDB9DBF386D182F', [
            'json' => [
                "email" => $data['email'],
                "belongsToLists" => ["27309365"],
                "manualUnsubscriptionReason" => "administrative",
                "unsubscriptionComment" => "string",
                "status" => "active",
                "canBeReactivated" => true,
                "isBeingReactivated" => true,
                "score" => 3,
                "_links" => [
                    ["href" => "https://providencia.org.mx",
                        "description" => "https://providencia.org.mx",
                        "rel" => "https://providencia.org.mx"],
                ],
            ]
        ]);

        return back()->with('success', 'Gracias por suscribirte');
    }

    public function blogs()
    {

        $blogs = Blog::orderBy('created_at', 'desc')->get();
        return view('blogs', ['blogs' => $blogs]);
    }

    public function blog($id)
    {

        $blog = Blog::where(['id' => $id])->first();
        return view('blog', ['blog' => $blog]);
    }

    public function iamprovidence()
    {
        $empresas = Empresa::all();
        $organizaciones = Organizacion::all();
        return view('iamprovidence', ['empresas' => $empresas, 'organizaciones' => $organizaciones]);
    }

    public function video()
    {

        $videos = Video::all();

        $categorias = array();
        foreach ($videos as $video) {
            if (!in_array($video->categoria, $categorias)) {
                $categorias[$video->categoria_english] = $video->categoria;
            }
        }

        return view('video', ['videos' => $videos, 'categorias' => $categorias, 'categorias' => $categorias]);
    }

    function project($id)
    {
        if (isset($_GET['modal'])) {
            $modal = $_GET['modal'];
        } else {
            $modal = 0;
        }

        /** @var Proyecto $project */
        $project = Proyecto::query()
            ->where('id', $id)
            ->orWhere('friendly_url', $id)
            ->first();
        if (!$project) {
            abort(404);
        }
        $objetivos = $project->objetivos()->get();
        /** Hasta aqui se acaba el seteo */
        if ($project->donative_view == 1) {
            $project->load(['donations.donor' => function ($query) {
                $query->orderByDesc('id');
            }]);
            /** Desde aquí hay que setear */
            $cantidad = 0;
            $donantes = 0;
            $showgoal = $project->show_goal;
            $date = $project->finish_date;
            $meta = $project->meta;
            $quienes = array();
            /** @var Donation[]|Collection $donations */
            $donations = $project->donations;
            foreach ($donations as $donation) {
                $cantidad += $donation->amount;
                $donantes += 1;
                $donor = $donation->donor;
                $row = array();
                $row['id'] = $donantes;
                $row['fecha'] = $donor->created_at->format('Y-m-d');
                $row['cantidad'] = $donation->amount;
                $row['nombre'] = $donor->name;
                $row['anonimo'] = $donation->anonymous;
                array_push($quienes, $row);
            }
            $quienes = collect($quienes);
            $porcentaje = ($meta > 0) ? round($cantidad / $meta * 100, 0) : 0;
            $por1 = ($porcentaje < 50) ? round($porcentaje, 0) * 2 : 100;
            $por2 = ($porcentaje > 50) ? (round($porcentaje, 0) - 50) * 2 : 0;
            $fechagoal = Carbon::createFromFormat('Y-m-d', $date);
            $quedan = $fechagoal->diff(Carbon::today())->days;
            return view('projectdonative', compact(['project', 'objetivos', 'modal', 'cantidad', 'quienes', 'donantes', 'meta', 'porcentaje', 'showgoal', 'por1', 'por2', 'quedan']));
        }
        return view('project', compact(['project', 'objetivos', 'modal']));
    }

    function projectFriendly($url)
    {
        if (isset($_GET['modal'])) {
            $modal = $_GET['modal'];
        } else {
            $modal = 0;
        }

        /** @var Proyecto $project */
        $project = Proyecto::query()
            ->where('id', $url)
            ->orWhere('friendly_url', $url)
            ->first();
        $objetivos = $project->objetivos()->get();
        /** Hasta aqui se acaba el seteo */
        if ($project->donative_view == 1) {
            $project->load(['donations.donor' => function ($query) {
                $query->orderByDesc('id');
            }]);
            /** Desde aquí hay que setear */
            $cantidad = 0;
            $donantes = 0;
            $showgoal = $project->show_goal;
            $date = $project->finish_date;
            $meta = $project->meta;
            $quienes = array();
            /** @var Donation[]|Collection $donations */
            $donations = $project->donations;
            foreach ($donations as $donation) {
                $cantidad += $donation->amount;
                $donantes += 1;
                $donor = $donation->donor;
                $row = array();
                $row['id'] = $donantes;
                $row['fecha'] = $donor->created_at->format('Y-m-d');
                $row['cantidad'] = $donation->amount;
                $row['nombre'] = $donor->name;
                $row['anonimo'] = $donation->anonymous;
                array_push($quienes, $row);
            }
            $quienes = collect($quienes);
            $porcentaje = ($meta > 0) ? round($cantidad / $meta * 100, 0) : 0;
            $por1 = ($porcentaje < 50) ? round($porcentaje, 0) * 2 : 100;
            $por2 = ($porcentaje > 50) ? (round($porcentaje, 0) - 50) * 2 : 0;
            $fechagoal = Carbon::createFromFormat('Y-m-d', $date);
            $quedan = $fechagoal->diff(Carbon::today())->days;
            return view('projectdonative', compact(['project', 'objetivos', 'modal', 'cantidad', 'quienes', 'donantes', 'meta', 'porcentaje', 'showgoal', 'por1', 'por2', 'quedan']));
        }
        return view('project', compact(['project', 'objetivos', 'modal']));

    }

    public function category($cat)
    {
        $blogs = Blog::orderBy('created_at', 'desc')->take(2)->get();
        $projects = Proyecto::where("categoria", $cat)->where("is_visible", 1)->get();
        return view('category')->with('projects', $projects)->with('cat', $cat)->with('blogs', $blogs);
    }


}
