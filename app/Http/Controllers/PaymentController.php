<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

//Models
use App\Model\Proyecto;
use App\Model\Donation;
use App\Model\Donor;
use App\Model\Suscription;
use App\Model\Webhook;
use App\Model\NotificationEmailProject;

use Openpay;
use OpenpayApiTransactionError;
use OpenpayApiRequestError;
use OpenpayApiConnectionError;
use OpenpayApiError;
use OpenpayApiAuthError;

use Mail;

class PaymentController extends Controller
{
    public function donative(Request $r)
    {
        if (env('OPENPAY_MODE') == 'dev') {
            $openpayId = env('OPENPAY_SANDBOX_ID');
            $openpayPk = env('OPENPAY_SANDBOX_PK');
            $sandboxJS = 'OpenPay.setSandboxMode(true);';
        } else {
            $openpayId = env('OPENPAY_LIVE_ID');
            $openpayPk = env('OPENPAY_LIVE_PK');
            $sandboxJS = 'OpenPay.setSandboxMode(false);';
        }

        if (!isset($_GET['project_id'])) {
            return redirect('/');
        } else {
            $project = Proyecto::find($_GET['project_id']);
            if (!$project) {
                return redirect('/');
            }
        }

        return view('donative', compact(['project', 'openpayId', 'openpayPk', 'sandboxJS']));
    }

    public function generaFicha(Request $request)
    {
        if (env('OPENPAY_MODE') == 'dev') {
            //TODO quitar mis credenciales
            $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'));
            Openpay::setProductionMode(false);
            $urlSpei = 'https://sandbox-dashboard.openpay.mx/spei-pdf/' . env('OPENPAY_SANDBOX_ID');
            $urlPaynet = 'https://sandbox-dashboard.openpay.mx/paynet-pdf/' . env('OPENPAY_SANDBOX_ID');
        } else {
            $openpay = Openpay::getInstance(env('OPENPAY_LIVE_ID'), env('OPENPAY_LIVE_SK'));
            Openpay::setProductionMode(true);
            $urlSpei = 'https://dashboard.openpay.mx/spei-pdf/' . env('OPENPAY_LIVE_ID');
            $urlPaynet = 'https://dashboard.openpay.mx/paynet-pdf/' . env('OPENPAY_LIVE_ID');
        }

        $project = Proyecto::find($request->id);

        if ($request->cfdi == 'false') {
            $dataRFC = 'Sin RFC';
        } else {
            $dataRFC = 'RFC: ' . $request->rfc;
        }

        $customer = array(
            'name' => $request->nombre,
            'last_name' => $request->paterno . ' ' . $request->materno,
            'phone_number' => $request->telefono,
            'email' => $request->email
        );

        $datatime = date('Y-m-d', strtotime("+1 day"));
        $datatime .= 'T23:45:00';

        $chargeData = array(
            'method' => $request->tipo == 'transferencia' ? 'bank_account' : 'store',
            'amount' => (double)$request->monto,
            'description' => 'Donación a: ' . $project->name . ' ' . $dataRFC,
            'order_id' => $request->id . '-' . date("Ymd") . random_int(10, 999999) . random_int(10, 999999),
            'customer' => $customer,
            'due_date' => $datatime);


        $charge = $openpay->charges->create($chargeData);

        if (!is_null($charge->error_message)) {
            return response()->json(array('result' => 'error', 'msg' => 'Servicio no disponible, intentalo mas tarde'), 200);
        }

        $response = $request->tipo == 'transferencia' ? array(
            "id" => $charge->id,
            "description" => $charge->description,
            "error_message" => $charge->error_message,
            "authorization" => $charge->authorization,
            "amount" => $charge->amount,
            "operation_type" => $charge->operation_type,
            "payment_method" => array(
                "type" => $charge->payment_method->type,
                "bank" => $charge->payment_method->bank,
                "agreement" => $charge->payment_method->agreement,
                "clabe" => $charge->payment_method->clabe,
                "name" => $charge->payment_method->name,
            ),
            "order_id" => $charge->order_id,
            "transaction_type" => $charge->transaction_type,
            "creation_date" => $charge->creation_date,
            "currency" => $charge->currency,
            "status" => $charge->status,
            "method" => $charge->method,
            "receipt" => $urlSpei . '/' . $charge->id,
        ) : array(
            "id" => $charge->id,
            "description" => $charge->description,
            "error_message" => $charge->error_message,
            "authorization" => $charge->authorization,
            "amount" => $charge->amount,
            "operation_type" => $charge->operation_type,
            "payment_method" => array(
                "type" => $charge->payment_method->type,
                "reference" => $charge->payment_method->reference,
                "barcode_url" => $charge->payment_method->barcode_url,
            ),
            "order_id" => $charge->order_id,
            "transaction_type" => $charge->transaction_type,
            "creation_date" => $charge->creation_date,
            "currency" => $charge->currency,
            "status" => $charge->status,
            "method" => $charge->method,
            "receipt" => $urlPaynet . '/' . $charge->payment_method->reference,
        );

        //Guardar datos en base, primero DONOR
        $donor = new Donor();
        $donor->name = $request->nombre;
        $donor->lastname = $request->paterno . ' ' . $request->materno;
        $donor->email = $request->email;
        $donor->openpay_id = 'not-set';
        $donor->is_monthly = false;
        $donor->openpay_card_token = 'no-subscription';
        $donor->save();

        //Ahora donación
        $donation = new Donation();
        $donation->donor_id = $donor->id;
        $donation->authorization = $charge->authorization;
        $donation->processor_id = $charge->id;
        $donation->amount = $request->monto;
        $donation->project_id = $request->id;
        $donation->razon = $request->razon;
        $donation->status = $charge->status;

        if ($request->cfdi == 'false') {
            $donation->wants_cfdi = 0;
            $donation->rfc = $request->rfc;
        } else {
            $donation->wants_cfdi = 1;
            $donation->rfc = $request->rfc;
        }

        if ($request->anonymous == 'false') {
            $donation->anonymous = 0;
        } else {
            $donation->anonymous = 1;
        }

        $donation->save();

        //Ahora hay que enviar correo
        $data['email'] = $request->email;
        $data['name'] = $request->nombre;
        $data['organization'] = $project->name;
        $data['linkUrl'] = $response["receipt"];
        //donativo unico
        Mail::send('mails.unico_pago', $data, function ($message) use ($data) {
            $message->from('noreply@providencia.org.mx', 'Providencia');
            $message->to($data['email'], $data['name']);
            $message->subject('Pago de donativo');
        });

        return response()->json(array('result' => 'success', 'msg' => 'Generación exitosa', 'data' => $response), 200);

    }

    public function hook(Request $request)
    {
        $webhook = new Webhook();
        $webhook->response = $request->getContent();
        $webhook->save();

        if ($request->has('type') && !is_null($request->type) && $request->type == 'charge.succeeded') {
            if ($request->has('transaction') && !is_null($request->transaction)) {
                $transaction = collect(json_decode(json_encode($request->transaction)));
                if ($transaction->has('id') && !is_null($transaction['id']) &&
                    $transaction->has('authorization') && !is_null($transaction['authorization']) &&
                    $transaction->has('status') && !is_null($transaction['status'])) {
                    if ($transaction->has('subscription_id') && !is_null($transaction['subscription_id'])) {
                        //Aqui suscripciones
                        $card = collect(json_decode(json_encode($request->transaction['card'])));
                        $suscription = Suscription::query()
                            ->where('suscription_id', $transaction['subscription_id'])
                            ->where('customer_id', $card['customer_id'])
                            ->where('card_id', $card['id'])
                            ->first();

                        if ($suscription) {
                            //Guardo datos de transacción
                            $donor = new Donor();
                            $donor->name = $suscription->name;
                            $donor->lastname = $suscription->lastname;
                            $donor->email = $suscription->email;
                            $donor->openpay_id = 'not-set';
                            $donor->is_monthly = true;
                            $donor->openpay_card_token = 'subscription';
                            $donor->save();

                            //Ahora donación
                            $donation = new Donation();
                            $donation->donor_id = $donor->id;
                            $donation->authorization = $transaction['authorization'];
                            $donation->processor_id = $transaction['id'];
                            $donation->amount = $suscription->amount;
                            $donation->project_id = $suscription->project_id;
                            $donation->razon = $suscription->razon;
                            $donation->status = $transaction['status'];
                            $donation->wants_cfdi = $suscription->wants_cfdi;
                            $donation->rfc = $suscription->rfc;
                            $donation->anonymous = $suscription->anonymous;
                            $donation->save();

                            $project = $suscription->project;

                            //Mail a usuario donativo mensual
                            $data['email'] = $donor->email;
                            $data['name'] = $donor->name . ' ' . $donor->lastname;
                            $data['organization'] = $project->name;
                            Mail::send('mails.recurrente_donativo', $data, function ($message) use ($data) {
                                $message->from('noreply@providencia.org.mx', 'Providencia');
                                $message->to($data['email'], $data['name']);
                                $message->subject('Gracias por tu donativo');
                            });

                            $notifications = NotificationEmailProject::query()->where('project_id', $project->id)->get();
                            if ($notifications) {
                                foreach ($notifications as $notification) {
                                    if (filter_var($notification->email, FILTER_VALIDATE_EMAIL)) {
                                        //Mail a usuario
                                        $data['email'] = $notification->email;
                                        $data['amount'] = $transaction['amount'];
                                        $data['authorization'] = $transaction['authorization'];
                                        $data['id'] = $transaction['id'];
                                        Mail::send('mails.notificacion', $data, function ($message) use ($data) {
                                            $message->from('noreply@providencia.org.mx', 'Providencia');
                                            $message->to($data['email'], $data['name']);
                                            $message->subject('Aviso de donación');
                                        });
                                    }
                                }
                            }
                        }
                    } else {
                        //Aqui pago con tarjetas o tienda o transferencia
                        $donation = Donation::query()->where('processor_id', $transaction['id'])->first();
                        if ($donation) {
                            $donation->authorization = $transaction['authorization'];
                            $donation->status = $transaction['status'];
                            $donation->save();
                            $donor = $donation->donor;
                            $project = $donation->project;

                            //Mail a usuario
                            $data['email'] = $donor->email;
                            $data['name'] = $donor->name . ' ' . $donor->lastname;
                            $data['organization'] = $project->name;
                            Mail::send('mails.unico_donativo', $data, function ($message) use ($data) {
                                $message->from('noreply@providencia.org.mx', 'Providencia');
                                $message->to($data['email'], $data['name']);
                                $message->subject('Gracias por tu donativo');
                            });

                            $notifications = NotificationEmailProject::query()->where('project_id', $project->id)->get();
                            if ($notifications) {
                                foreach ($notifications as $notification) {
                                    if (filter_var($notification->email, FILTER_VALIDATE_EMAIL)) {
                                        //Mail a usuario
                                        $data['email'] = $notification->email;
                                        $data['amount'] = $transaction['amount'];
                                        $data['authorization'] = $transaction['authorization'];
                                        $data['id'] = $transaction['id'];
                                        Mail::send('mails.notificacion', $data, function ($message) use ($data) {
                                            $message->from('noreply@providencia.org.mx', 'Providencia');
                                            $message->to($data['email'], $data['name']);
                                            $message->subject('Aviso de donación');
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return response()->json(array('result' => 'success', 'msg' => 'Generación exitosa'), 200);
    }

    public function card(Request $request)
    {
        if (env('OPENPAY_MODE') == 'dev') {
            $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'));
            Openpay::setProductionMode(false);
        } else {
            $openpay = Openpay::getInstance(env('OPENPAY_LIVE_ID'), env('OPENPAY_LIVE_SK'));
            Openpay::setProductionMode(true);
        }

        $project = Proyecto::find($request->id);

        if ($request->cfdi == 'false') {
            $dataRFC = 'Sin RFC';
        } else {
            $dataRFC = 'RFC: ' . $request->rfc;
        }

        $customer = array(
            'name' => $request->nombre,
            'last_name' => $request->paterno . ' ' . $request->materno,
            'phone_number' => $request->telefono,
            'email' => $request->email
        );

        $chargeData = array(
            'method' => 'card',
            'source_id' => $request->token_id,
            'redirect_url' => 'https://providencia.org.mx/thanks',
            'amount' => (double)$request->monto,
            'device_session_id' => $request->device,
            'description' => 'Donación a: ' . $project->name . ' ' . $dataRFC,
            'order_id' => $request->id . '-' . date("Ymd") . random_int(10, 999999) . random_int(10, 999999),
            'use_card_points' => $request->points,
            'use_3d_secure' => 'true',
            'capture' => true,
            'customer' => $customer
        );

        $charge = $openpay->charges->create($chargeData);

        if (!is_null($charge->error_message)) {
            return response()->json(array('result' => 'error', 'msg' => 'Servicio no disponible, intentalo mas tarde'), 200);
        }

        $response = array(
            "id" => $charge->id,
            "description" => $charge->description,
            "error_message" => $charge->error_message,
            "authorization" => $charge->authorization,
            "amount" => $charge->amount,
            "operation_type" => $charge->operation_type,
            "order_id" => $charge->order_id,
            "transaction_type" => $charge->transaction_type,
            "creation_date" => $charge->creation_date,
            "currency" => $charge->currency,
            "status" => $charge->status,
            "method" => $charge->method,
            "type" => $charge->payment_method->type,
            "url" => $charge->payment_method->url
        );

        //Guardar datos en base, primero DONOR
        $donor = new Donor();
        $donor->name = $request->nombre;
        $donor->lastname = $request->paterno . ' ' . $request->materno;
        $donor->email = $request->email;
        $donor->openpay_id = 'not-set';
        $donor->is_monthly = false;
        $donor->openpay_card_token = 'no-subscription';
        $donor->save();

        //Ahora donación
        $donation = new Donation();
        $donation->donor_id = $donor->id;
        $donation->authorization = $charge->authorization;
        $donation->processor_id = $charge->id;
        $donation->amount = $request->monto;
        $donation->project_id = $request->id;
        $donation->razon = $request->razon;
        $donation->status = $charge->status;

        if ($request->cfdi == 'false') {
            $donation->wants_cfdi = 0;
            $donation->rfc = $request->rfc;
        } else {
            $donation->wants_cfdi = 1;
            $donation->rfc = $request->rfc;
        }

        if ($request->anonymous == 'false') {
            $donation->anonymous = 0;
        } else {
            $donation->anonymous = 1;
        }

        $donation->save();

        if (!is_null($charge->payment_method->type) && $charge->payment_method->type === 'redirect' && !is_null
        ($charge->payment_method->url)) {
            return response()->json(array('result' => 'success', 'msg' => 'Generación exitosa', 'data' => $response, 'url' => $charge->payment_method->url), 200);
        }

        return response()->json(array('result' => 'success', 'msg' => 'Generación exitosa', 'data' => $response, 'url' => null), 200);

    }

    public function suscripcion(Request $request)
    {
        if (env('OPENPAY_MODE') == 'dev') {
            $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'));
            Openpay::setProductionMode(false);
        } else {
            $openpay = Openpay::getInstance(env('OPENPAY_LIVE_ID'), env('OPENPAY_LIVE_SK'));
            Openpay::setProductionMode(true);
        }

        $project = Proyecto::find($request->id);

        if ($request->cfdi == 'false') {
            $dataRFC = 'Sin RFC';
        } else {
            $dataRFC = 'RFC: ' . $request->rfc;
        }

        //Creo el plan
        $planDataRequest = array(
            'amount' => (double)$request->monto,
            'status_after_retry' => 'cancelled',
            'retry_times' => 2,
            'name' => $project->name . ' ' . date("Y-m-d") . ' No.' . random_int(10, 999999) . ' ' . $dataRFC,
            'repeat_unit' => 'month',
            'trial_days' => '0',
            'repeat_every' => '1',
            'currency' => 'MXN');

        $plan = $openpay->plans->add($planDataRequest);

        if (is_null($plan->id)) {
            return response()->json(array('result' => 'error', 'msg' => 'Error en servicio, intente más tarde'), 200);
        }

        //Checo primero si existe el customer
        $customers = $openpay->customers->getList([]);
        $customer = null;
        if (count($customers) != 0) {
            foreach ($customers as $custom) {
                if ($custom->email == $request->email) {
                    $customer = $custom;
                }
            }
        }

        //En caso de que no este en l lista
        if (is_null($customer)) {
            $customerData = array(
                'name' => $request->nombre,
                'last_name' => $request->paterno . ' ' . $request->materno,
                'phone_number' => $request->telefono,
                'email' => $request->email
            );

            $customer = $openpay->customers->add($customerData);
        }

        if (is_null($customer->id)) {
            return response()->json(array('result' => 'error', 'msg' => 'Error en servicio, intente más tarde'), 200);
        }

        //Guardo tarjeta
        $cardDataRequest = array(
            'holder_name' => $request->cardname,
            'card_number' => $request->card,
            'cvv2' => $request->cvv,
            'expiration_month' => $request->month,
            'expiration_year' => $request->year);

        $card = $customer->cards->add($cardDataRequest);

        if (is_null($card->id)) {
            return response()->json(array('result' => 'error', 'msg' => 'Error en servicio, intente más tarde'), 200);
        }

        //Hago la suscripción
        $subscriptionDataRequest = array(
            'plan_id' => $plan->id,
            'card_id' => $card->id);

        $customer = $openpay->customers->get($customer->id);
        $subscription = $customer->subscriptions->add($subscriptionDataRequest);

        if (is_null($subscription->id)) {
            return response()->json(array('result' => 'error', 'msg' => 'Error en servicio, intente más tarde'), 200);
        }

        $response = array(
            "id" => $subscription->id,
            "creation_date" => $subscription->creation_date,
            "customer_id" => $subscription->customer_id,
            "card_id" => $card->id
        );

        //Guardar en suscripción
        $suscription = new Suscription ();
        $suscription->name = $request->nombre;
        $suscription->lastname = $request->paterno . ' ' . $request->materno;
        $suscription->email = $request->email;
        $suscription->amount = $request->monto;

        if ($request->cfdi == 'false') {
            $suscription->wants_cfdi = 0;
            $suscription->rfc = $request->rfc;
        } else {
            $suscription->wants_cfdi = 1;
            $suscription->rfc = $request->rfc;
        }

        if ($request->anonymous == 'false') {
            $suscription->anonymous = 0;
        } else {
            $suscription->anonymous = 1;
        }

        $suscription->project_id = $request->id;
        $suscription->suscription_id = $subscription->id;
        $suscription->customer_id = $subscription->customer_id;
        $suscription->card_id = $card->id;
        $suscription->razon = $request->razon;

        $suscription->save();

        return response()->json(array('result' => 'success', 'msg' => 'Generación exitosa', 'data' => $response), 200);

    }

    //Esto lo hizo miriam
    public function getDonationOpenpay(Request $request)
    {

        $date = $request->validate([
            'dateGte' => ['required', 'date'],
            'dateLte' => ['required', 'date'],
        ], [
            'dateGte.required' => 'Ingresa la fecha con este formato YYYY',
            'dateGte.date' => 'Ingresa el año en formato YYYY',
            'dateLte.required' => 'Ingresa la fecha con este formato YYYY',
            'dateLte.date' => 'Ingresa el año en formato YYYY',
        ]);

        $donationsList = $this->getDonationOpenpayInfo($date);

        $charges = [];

        foreach ($donationsList as $donation) {
            $charges[] = [
                'status' => $donation->status,
                'amount' => $donation->amount,
                'description' => $donation->description,
                'method' => $donation->method,
                'currency' => $donation->currency
            ];
        }

        return json_encode($charges);
    }

    //Esto tambien lo hizo miriam
    public function getDonationOpenpayInfo($dates)
    {
        Openpay::setProductionMode(true);
        // llaves
        $openpay = Openpay::getInstance('mwca6tzxfjjuok8pikdk', 'sk_21856a1a06814916a3399eb77490d1ba');
        // $openpay = Openpay::getInstance('mszeob8lefdqdbmdmczi', 'sk_ac808adb70b14a82ae698a61d32e7ce8');
        $searchParams = array(
            'creation[gte]' => $dates['dateGte'],
            'creation[lte]' => $dates['dateLte'],
            'offset' => 0,
            'limit' => 100);
        /* $customer = $openpay->customers->get('aczqbcdsgx9yngofzlil');
        $chargeList = $customer->charges->getList($searchParams); */
        $chargeList = $openpay->charges->getList($searchParams);
        return $chargeList;
    }

}
