<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Donation
 * @package App\Model
 *
 * @property Donor $donor
 */
class Donation extends Model
{
    protected $table = 'donations';

    protected $casts = [
        'anonymous' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Donor
     */
    public function donor() {
        return $this->belongsTo(Donor::class, 'donor_id', 'id');
    }

    public function project() {
        return $this->belongsTo(Proyecto::class, 'project_id', 'id');
    }
}
