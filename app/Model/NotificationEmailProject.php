<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotificationEmailProject extends Model
{
    protected $table = 'notification_email_projects';
}
