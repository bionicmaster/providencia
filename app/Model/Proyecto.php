<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Proyecto
 * @package App\Model
 *
 * @property Donation[]|Collection $donations
 */
class Proyecto extends Model
{
    protected $table = 'proyectos';

    protected $casts = [
      'donative_view' => 'boolean',
      'show_goal'=>'boolean'
    ];

    public function objetivos()
    {
        return $this->belongsToMany(Objetivo::class,'objetivos_proyecto');
    }

    public function donations()
    {
        return $this->hasMany(Donation::class, 'project_id', 'id');
    }
}
