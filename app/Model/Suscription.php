<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Suscription extends Model
{
    protected $table = 'suscriptions';

    public function project() {
        return $this->belongsTo(Proyecto::class, 'project_id', 'id');
    }
}
