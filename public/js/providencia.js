//change color logo
var pathname = window.location.pathname;
var logoImg = $('.navbar-brand_img');

if(pathname == "/areas/educacion" 
  || pathname == "/areas/salud" 
  || pathname == "/areas/comunidad" 
  || pathname == "/areas/desarrollo" 
  || pathname == "/providencia/crowfunding" 
 // || pathname == "/como-donar"
  || pathname == "/formulario"
  || pathname == "/como-subir-proyecto"
  || pathname == "/preguntas-frecuentes"
  || pathname == "/soy-providencia"
  || pathname == "/causas-permanentes"
  || pathname == "/el-pozo-de-vida"
  || pathname == "/mexico-tierra-de-amaranto"
  || pathname == "/hospital-escandon"
  || pathname == "/christel-house-mexico"){
    logoImg.attr('src', '../images/providencia/logo-providencia-white.png');
}
if(pathname == "/que-hacemos" || pathname == "/el-pozo-de-vida"){
    $('.nav-link').addClass('nav-link--black');
}

//navigation mobile
$('.navbar-toggler').click(function(){
    $('.main-navigation').toggleClass('mobile');
});

// donativo unico / mensual
$('.tipo-donativo').click(function(){
    $('.tipo-donativo').removeClass('active');
    $(this).addClass('active');
    setTimeout(() => {
        var tipoDonativo = $('input[name=tipo-donativo]:checked').val();
        console.log(tipoDonativo);
    }, 100);
});

//total price
$('.modal-multipagos_price').click(function(){
    $('.modal-multipagos_price').removeClass('active');
    $(this).addClass('active');
    setTimeout(() => {        
        var price =  $('input[name=price]:checked').val();
        document.getElementById("price").value = price; 
    }, 100);
})
