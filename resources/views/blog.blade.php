@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$blog->title}}" />
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/blog/{{$blog->id}}" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('storage/'.$blog->image)}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>{{$blog->title}} | Providencia</title>
@endsection

@section('content')
@include('layout.navigation')
    <!-- HEADER -->

    <section class="providencia-header" style="background-image: url('{{asset('storage/'.$blog->image)}}');">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2 class="providencia-header_title text-white espanol"><br>
                        {{$blog->subtitle1}}{{$blog->subtitle2}}
                    </h2>
                    <h2 class="providencia-header_title text-white english">
                        {{$blog->subtitle1_english}}{{$blog->subtitle2_english}}
                    </h2>
                </div>
            </div>
        </div>
    </section>

    <!-- INTRO -->
    <section class="providencia-intro">
        <div class="container">
            <br>
            <div class="row">
                <div class="col-12 text-center espanol">
                    <h1 class="text-blue providencia-intro_title" style="text-transform: uppercase">{{$blog->title}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$blog->subtitle1}} {{$blog->subtitle2}}</h3>
                    {!! $blog->area1 !!}
                </div>
                <div class="col-12 text-center english">
                    <h1 class="text-blue providencia-intro_title" style="text-transform: uppercase">{{$blog->title_english}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$blog->subtitle1_english}} {{$blog->subtitle2_english}}</h3>
                    {!! $blog->area1_english !!}
                </div>
            </div>
        </div>
    </section>

    
    <!-- MISION VISION -->
    <section class="providencia-vision bg-blue text-white text-center">
        <div class="container-fluid">
            <!-- mission -->
            <div class="row">
                <div class="col-12 espanol">
                    <hr class="providencia-vision_line">
                    {!!$blog->text!!}
                </div>
                <div class="col-12 english">
                    <hr class="providencia-vision_line">
                    {!! $blog->text_english !!}
                </div>
            </div>
        </div>
    </section>

    <section class="providencia-vision bg-blue text-white text-center">
        <div class="container-fluid">
            <!-- mission -->
            <div class="row">
                <div class="col-12 espanol">
                    <hr class="providencia-vision_line">
                    {!! $blog->area2 !!}
                </div>
                <div class="col-12 english">
                    <hr class="providencia-vision_line">
                    {!! $blog->area2_english !!}
                </div>
            </div>
        </div>
    </section>

    <section class="providencia-video">
        <div class="container-fluid">
            <div class="row espanol">
                <div class="col-12 pd-0">
                    {!! $blog->extramedia !!}
                </div>
            </div>
            <div class="row english">
                <div class="col-12 pd-0">
                    {!! $blog->extramedia_english !!}
                </div>
            </div>
        </div>
    </section>
    <section class="providencia-vision bg-blue text-white text-center" style="margin-top: -17px;">

        <div class="container-fluid espanol">
            {!! $blog->area3 !!}
        </div>
        <div class="container-fluid english">
            {!! $blog->area3_english !!}
        </div>
    </section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection
