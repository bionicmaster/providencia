@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Blog" /> 
    <meta name="keywords" content="providencia, Providencia, blog, Blog"/>
    <meta property="og:url" content="https://www.providencia.org.mx/blogs" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
    <title>Blog | Providencia</title>
@endsection

@section('content')

@include('layout.navigation')


<!-- HEADER -->
<section class="header">
    <div class="header-fade">
        <div class="header-slider header-blog">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 pt-5 text-right">
                        <h2 style=" line-height: .9;margin-top: 60px;margin-left: 100px;font-family: roboto-bold,Arial,Helvetica,sans-serif;font-weight: 700;font-size: 59px !important;" class="providencia-header_title text-white">Blogs</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- NEWS -->
<section class="news" style="margin-top:100px; margin-bottom:100px;">
    <div class="container-fluid">
        <div class="row">
            <!-- News 1 -->
            @foreach ($blogs as $blog)
            <div class="col-12 col-md-6 pd-0 espanol">
                <div style="overflow: hidden;">
                    <a class="d-flex justify-content-center align-items-center news_new news_new--1" style=" background-image: url('{{asset('storage/'.$blog->image)}}')" href="{{route('blog', ['id'=>$blog->id])}}">
                            {{$blog->title}}
                         <div class="text-white pd-10">
                            <h3 class="news_title mb-0"></h3>
                            <p class="news_btn" href="{{route('blog', ['id'=>$blog->id])}}">Ver más »</p>
                        </div> 
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-6 pd-0 english">
                <div style="overflow: hidden;">
                    <a class="d-flex justify-content-center align-items-center news_new news_new--1" style=" background-image: url('{{asset('storage/'.$blog->image)}}')" href="{{route('blog', ['id'=>$blog->id])}}">
                            {{$blog->title_english}}
                         <div class="text-white pd-10">
                            <h3 class="news_title mb-0"></h3>
                            <p class="news_btn" href="{{route('blog', ['id'=>$blog->id])}}">See more »</p>
                        </div> 
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section('js')
    <script type="text/javascript" src="{{asset('js/jquery-migrate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        $('body').addClass('providencia-home');
        //header slider
        $('.header-fade').slick({
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });

        function registerSuscrito() {


            $('.contacto-submit').attr('disabled', true);
            let formData = new FormData();


            formData.append("email", $('#email').val());
            formData.append("name", $('#name').val());

            axios.post('suscribir', formData
                , {

                }).then(function (response) {

                if (response.data.msg === 'ok') {
                    user = response.data.user;
                    $('.contactanos_exclusivo-container').fadeOut('fast', function () {
                        $('#graciasporSuscrito').fadeIn();
                    });
                }
            })
                .catch(function (error) {
                    console.log(error);
                });


        }


    </script>
@endsection