@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Fundación Providencia" />
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" />
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <title>Fundación Providencia | Providencia</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
@endsection

@section('content')
    <?php
    $tipoProyectos['salud'] = "";
    $tipoProyectos[''] = "Educación";
    $tipoProyectos[''] = "";
    $tipoProyectos['desarrollo'] = "";
    ?>
    @include('layout.navigation')

<!-- HEADER -->
<section class="header">
    <div class="header-fade">
        <div class="header-slider header-slider_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text2.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow btn-covid" href="/covid19">DONAR COVID-19</a>
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-slider header-slider_2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text1.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-slider header-slider_3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text1.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- AREA -->
<section class="areas">
    <div class="container">
        <div class="row">
            <div class="col-12 espanol">
                <h2 class="text-center areas_title">
                    @if ($cat=='salud')
                        Salud & Nutrición
                    @elseif ($cat=='educacion')
                        Educación
                    @elseif ($cat=='comunidad')
                        Comunidad
                    @else
                        Desarrollo Social
                    @endif
                </h2>
            </div>
            <div class="col-12 english">
                <h2 class="text-center areas_title">
                    @if ($cat=='salud')
                        health and nutrition
                    @elseif ($cat=='educacion')
                        Education
                    @elseif ($cat=='comunidad')
                        Comumnity
                    @else
                        Social development
                    @endif
                </h2>
            </div>
        </div>
    </div>
</section>

<!-- PROJECTS -->
<section class="projects" id="projects">
    <div class="container-fluid">
        <div class="row">

            @foreach($projects as $project)

            <div class="col-12 col-md-6 pd-0">
                <div class="projects_container bg-blue">
                    <div style="height: 37.2%">
                        <h3 class="projects_title text-center text-white espanol"><b>{{$project->name}}</b></h3>
                        <h3 class="projects_title text-center text-white english"><b>{{$project->name_english}}</b></h3>
                        <h4 class="projects_subtitle text-center text-white espanol">{{$project->subtitulo1}} {{$project->subtitulo2}}</h4>
                        <h4 class="projects_subtitle text-center text-white english">{{$project->subtitulo1_english_english}}</h4>
                        <p class="projects_description text-white espanol">{{$project->description}}</p>
                        <p class="projects_description text-white english">{{$project->description_english}}</p>
                        <p class="text-right espanol"><a href="{{url($project->friendly_url)}}" class="proyects_link text-white">Ver más »</a></p>
                        <p class="text-right english"><a href="{{url($project->friendly_url)}}" class="proyects_link text-white">See more »</a></p>
                    </div>
                    <img class="projects_img" style=" height: 50%; object-fit: cover;" src="{{asset('storage/'.$project->banner_home)}}" alt="{{$project->name}}">
                    <div style="display: table;  width:auto;  margin-right:auto;margin-left:auto;">
                        <a href="{{route('project',['id'=>$project->id])}}?modal=1" class="projects_btn text-blue hvr-grow espanol">DONAR</a>
                        <a href="{{route('project',['id'=>$project->id])}}?modal=1" class="projects_btn text-blue hvr-grow english">DONATE</a>
                    </div>
                </div>
                <div class="projects-objetivos text-center mx-auto">
                    <img class="projects-objetivos_title img-fluid" src="{{asset('/images/logo-objetivos.png')}}" alt="Objetivos de desarrollo sostenible">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-1.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-2.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-3.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-6.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-8.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-11.png')}}" alt="objetivo de desarrollo">
                    <img class="projects-objetivos_elem" src="{{asset('/images/objetivos/objetivo-17.png')}}" alt="objetivo de desarrollo">
                </div>
            </div>

            @endforeach

        </div>
    </div>
</section>

<!-- BEPART -->
@include('layout.bepart')

<!-- Blogs -->
@include('layout.blogs')

<!-- Newsletter -->
@include('layout.newsletter')

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section('js')
<script type="text/javascript" src="{{asset('js/jquery-migrate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    //header slider
    $('.header-fade').slick({
        infinite: true,
        slidesToShow: 1,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
</script>
<!--Alertas de sweet Alert-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>

@if ($errors->any())
<script>
    $(function() {
        swal({
            title: "Error",
            text: "Error en email ingresado",
            icon: "error",
            button: "OK",
        });
    });
</script>
@endif
@if ($message = Session::get('success'))
<script>
    $(function() {
        swal({
            title: "Gracias por registrarte",
            icon: "success",
            button: "OK",
        });
    });
</script>
@endif

@endsection

@section('styles')

<style>
    #animated_image:hover {
        overflow: hidden;
        transition: all 1.5s ease;
        transform: scale(1.3);
    }
</style>
@endsection
