@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Crowfounding" /> 
    <meta name="keywords" content="providencia, Providencia, Crowfounding, crowfounding, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <title>Crowfounding | Providencia</title>
    
@endsection

@section('content')

    @extends('layout.navigation')

    <!-- HEADER -->
    <header class="providencia-header providencia-header--crowfunding">
    </header>

    <!-- TEXT -->
    <section class="providencia-crowfunding">
        <div class="container">
            <div class="row espanol">
                <div class="col-12 text-center">
                    <h1 class="providencia-title">CROWFUNDING</h1>
                    <h3 class="providencia-subtitle">¿Qué son Proyectos de Crowdfunding?</h3>
                    <div class="providencia-text">La palabra se compone de “Crowd” “Multitud” y “Funding” “Fondeo”, en otras palabra un grupo de personas o empresas “Multitud”, aportan donativo por única vez “Fondeo” a un proyecto publicado en la plataforma con un monto determinado a recauda (meta), en un período de tiempo específico (se recomienda no sean más de 45 días) podemos resumir entonces como “Fondeo Colectivo”.</div>
                </div>
            </div>
            <div class="row english">
                <div class="col-12 text-center">
                    <h1 class="providencia-title">CROWFUNDING</h1>
                    <h3 class="providencia-subtitle">What are Crowdfunding Projects?</h3>
                    <div class="providencia-text">The word is made up of “Crowd”  and “Funding” , in other words a group of people or companies “Crowd” contribute a one-time donation “Funding” to a project published on the platform with an amount determined to collect (goal), in a specific period of time (it is recommended not to be more than 45 days) we can then summarize as “Collective Funding”.</div>
                </div>
            </div>
        </div>
    </section>
    <!-- PAYMENT -->
    @include('layout.payments')

    <!-- FOOTER -->
    @include('layout.footer')

@endsection