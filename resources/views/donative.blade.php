@extends('layout.master')
@section('head')
    <meta property="og:title" content="{{$project->name}}"/>
    <meta name="keywords" content="providencia, Providencia, Donar, donar, Dona, dona"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:image" content="{{asset('logo.png')}}">
    <title>Donar {{$project->name}} | Providencia</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.openpay.mx/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://js.openpay.mx/openpay-data.v1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('content')

    <style>
        #loader {
            display: none;
        }

        svg {
            width: 100%;
            max-width: 200px;
            border-radius: 3px;
            box-shadow: 2px 2px 5px #fff;
            background: #fff;
            fill: none;
            stroke: #7b97ac;
            stroke-linecap: round;
            stroke-width: 8%
        }

        use {
            stroke: #95d600;
            animation: a 1.2s linear infinite
        }

        @keyframes a {
            to {
                stroke-dashoffset: 0px
            }
        }

        .modal-content-error {
            border-radius: 0px;
            border: none;
            text-align: center;
        }

        .modal-body-error {
            min-height: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .errorBar {
            height: 70px;
            background: #ff3636;
            position: absolute;
            width: 100%;
            text-align: center;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 99999;
            top: 0;
            color: white;
            font-size: 2.5vw;
            animation-duration: all .200s;
        }

        .dona_ventajas ul li::before {
            content: "•";
            color: #1580b4 !important;
            font-weight: bold;
            display: inline-block;
            width: 1em;
            margin-left: -1em;
        }
    </style>

    <section class="dona">
        <div class="container-fluid bg-blue pt-2 pb-2" style="background:#1580b4;">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center text-white mb-0"><b>{{$project->name}}</b></h2>
                </div>
            </div>
        </div>
        <div class="container pt-5 pb-5">
            <div class="row">
                <!-- MÉTODO DE PAGO -->
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;">Método de pago
                                <i class="fas fa-lock"></i></p>
                            <div>
                                <img class="logo-openpay" src="{{asset('/images/openpay.png')}}"
                                     alt="Open Pay">
                            </div>
                        </div>
                    </div>
                    <!-- FORM -->
                    <div class="row">
                        <div class="col-12">
                            <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;">Datos del Representante Legal</p>
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="fisica" class="dona_representante-btn active"> PERSONA FÍSICA</label>
                            <input type="radio" name="representante" id="fisica" value="fisica" checked>
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="moral" class="dona_representante-btn"> PERSONA MORAL</label>
                            <input type="radio" name="representante" id="moral" value="moral">
                        </div>

                        <div id="tipo_pago_multiples_title" class="col-12">
                            <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;">Selecciona tipo de pago</p>
                        </div>

                        <div id="tipo_pago_multiples" class="col-12 col-md-6">
                            <select name="tipo_pago" id="tipo_pago">
                                <option value="tarjeta" selected>Tarjeta</option> 
                                <option value="conveniencia" >Tiendas de conveniencia</option>
                                <option value="transferencia">Transferencia interbancaria</option>
                              </select>
                        </div>
                        <br><br>

                        <!-- donar -->
                        <div class="row align-items-center row-donar">
                            <div class="col-7 col-md-8 text-left pr-0">
                                <div id="img_tarjetas">
                                    <img class="modal-multipagos_logos" src="{{asset('/images/logo-mastercard.png')}}" alt="master card" style="height:50px">
                                    <img class="modal-multipagos_logos" src="{{asset('/images/logo-visa.jpg')}}" alt="visa" style="height:50px">
                                    <img class="modal-multipagos_logos" src="{{asset('/images/logo-american.png')}}" alt="american express" style="height:50px">
                                </div>
                                <div id="img_tiendas">
                                    <img class="modal-multipagos_logos" src="{{asset('/images/logo-tiendas-solo.png')}}" alt="tiendas" style="height:50px">
                                </div>
                                <div id="img_transferencia">
                                    <img class="modal-multipagos_logos" src="{{asset('/images/logo-transferencia.png')}}" alt="transferencia">
                                </div>
                            </div>
                        </div>

                        <form class="w-100" id="subscription-form">
                            <label id="moralTitle">Razón Social<sup>*</sup></label>
                            <input type="text" placeholder="Razón Social" id="razon_social"><br>
                            <label id="moralRep">Nombre completo del representante legal<sup>*</sup></label>
                            <label id="fisicaShow">Nombre completo<sup>*</sup></label>
                            <input id="nombre" type="text" placeholder="Nombre(s)"><br>
                            <input id="paterno" type="text" placeholder="Apellido Paterno">
                            <input id="materno" type="text" placeholder="Apellido Materno"><br>
                            <div class="row">
                                <div class="col-12 col-md-6 pd-">
                                    <label>Email<sup>*</sup></label>
                                    <input id="email" class="w-100" type="email" placeholder="ejemplo@ejemplo.com">
                                </div>
                                <div class="col-12 col-md-6 pd-">
                                    <label>Celular a 10 dígitos<sup>*</sup></label>
                                    <input id="celular" class="w-100" type="tel" placeholder="5578987667" maxlength="10" pattern="[0-9]{10}">
                                </div>
                            </div>
                            <label>País de nacionalidad<sup>*</sup></label>
                            <select id="nationality" >
                                <option value="Afganistán" >Afganistán</option>
                                <option value="Albania" >Albania</option>
                                <option value="Alemania" >Alemania</option>
                                <option value="Andorra" >Andorra</option>
                                <option value="Angola" >Angola</option>
                                <option value="Anguila" >Anguila</option>
                                <option value="Antártida" >Antártida</option>
                                <option value="Antigua y Barbuda" >Antigua y Barbuda</option>
                                <option value="Antillas holandesas" >Antillas holandesas</option>
                                <option value="Arabia Saudí" >Arabia Saudí</option>
                                <option value="Argelia" >Argelia</option>
                                <option value="Argentina" >Argentina</option>
                                <option value="Armenia" >Armenia</option>
                                <option value="Aruba" >Aruba</option>
                                <option value="Australia" >Australia</option>
                                <option value="Austria" >Austria</option>
                                <option value="Azerbaiyán" >Azerbaiyán</option>
                                <option value="Bahamas" >Bahamas</option>
                                <option value="Bahrein" >Bahrein</option>
                                <option value="Bangladesh" >Bangladesh</option>
                                <option value="Barbados" >Barbados</option>
                                <option value="Bélgica" >Bélgica</option>
                                <option value="Belice" >Belice</option>
                                <option value="Benín" >Benín</option>
                                <option value="Bermudas" >Bermudas</option>
                                <option value="Bhután" >Bhután</option>
                                <option value="Bielorrusia" >Bielorrusia</option>
                                <option value="Birmania" >Birmania</option>
                                <option value="Bolivia" >Bolivia</option>
                                <option value="Bosnia y Herzegovina" >Bosnia y Herzegovina</option>
                                <option value="Botsuana" >Botsuana</option>
                                <option value="Brasil" >Brasil</option>
                                <option value="Brunei" >Brunei</option>
                                <option value="Bulgaria" >Bulgaria</option>
                                <option value="Burkina Faso" >Burkina Faso</option>
                                <option value="Burundi" >Burundi</option>
                                <option value="Cabo Verde" >Cabo Verde</option>
                                <option value="Camboya" >Camboya</option>
                                <option value="Camerún" >Camerún</option>
                                <option value="Canadá" >Canadá</option>
                                <option value="Chad" >Chad</option>
                                <option value="Chile" >Chile</option>
                                <option value="China" >China</option>
                                <option value="Chipre" >Chipre</option>
                                <option value="Ciudad estado del Vaticano" >Ciudad estado del Vaticano</option>
                                <option value="Colombia" >Colombia</option>
                                <option value="Comores" >Comores</option>
                                <option value="Congo" >Congo</option>
                                <option value="Corea" >Corea</option>
                                <option value="Corea del Norte" >Corea del Norte</option>
                                <option value="Costa del Marfíl" >Costa del Marfíl</option>
                                <option value="Costa Rica" >Costa Rica</option>
                                <option value="Croacia" >Croacia</option>
                                <option value="Cuba" >Cuba</option>
                                <option value="Dinamarca" >Dinamarca</option>
                                <option value="Djibouri" >Djibouri</option>
                                <option value="Dominica" >Dominica</option>
                                <option value="Ecuador" >Ecuador</option>
                                <option value="Egipto" >Egipto</option>
                                <option value="El Salvador" >El Salvador</option>
                                <option value="Emiratos Arabes Unidos" >Emiratos Arabes Unidos</option>
                                <option value="Eritrea" >Eritrea</option>
                                <option value="Eslovaquia" >Eslovaquia</option>
                                <option value="Eslovenia" >Eslovenia</option>
                                <option value="España" >España</option>
                                <option value="Estados Unidos" >Estados Unidos</option>
                                <option value="Estonia" >Estonia</option>
                                <option value="c" >Etiopía</option>
                                <option value="Ex-República Yugoslava de Macedonia" >Ex-República Yugoslava de Macedonia</option>
                                <option value="Filipinas" >Filipinas</option>
                                <option value="Finlandia" >Finlandia</option>
                                <option value="Francia" >Francia</option>
                                <option value="Gabón" >Gabón</option>
                                <option value="Gambia" >Gambia</option>
                                <option value="Georgia" >Georgia</option>
                                <option value="Georgia del Sur y las islas Sandwich del Sur" >Georgia del Sur y las islas Sandwich del Sur</option>
                                <option value="Ghana" >Ghana</option>
                                <option value="Gibraltar" >Gibraltar</option>
                                <option value="Granada" >Granada</option>
                                <option value="Grecia" >Grecia</option>
                                <option value="Groenlandia" >Groenlandia</option>
                                <option value="Guadalupe" >Guadalupe</option>
                                <option value="Guam" >Guam</option>
                                <option value="Guatemala" >Guatemala</option>
                                <option value="Guayana" >Guayana</option>
                                <option value="Guayana francesa" >Guayana francesa</option>
                                <option value="Guinea" >Guinea</option>
                                <option value="Guinea Ecuatorial" >Guinea Ecuatorial</option>
                                <option value="Guinea-Bissau" >Guinea-Bissau</option>
                                <option value="Haití" >Haití</option>
                                <option value="Holanda" >Holanda</option>
                                <option value="Honduras" >Honduras</option>
                                <option value="Hong Kong R. A. E" >Hong Kong R. A. E</option>
                                <option value="Hungría" >Hungría</option>
                                <option value="India" >India</option>
                                <option value="Indonesia" >Indonesia</option>
                                <option value="Irak" >Irak</option>
                                <option value="Irán" >Irán</option>
                                <option value="Irlanda" >Irlanda</option>
                                <option value="Isla Bouvet" >Isla Bouvet</option>
                                <option value="Isla Christmas" >Isla Christmas</option>
                                <option value="Isla Heard e Islas McDonald" >Isla Heard e Islas McDonald</option>
                                <option value="Islandia" >Islandia</option>
                                <option value="Islas Caimán" >Islas Caimán</option>
                                <option value="Islas Cook" >Islas Cook</option>
                                <option value="Islas de Cocos o Keeling" >Islas de Cocos o Keeling</option>
                                <option value="Islas Faroe" >Islas Faroe</option>
                                <option value="Islas Fiyi" >Islas Fiyi</option>
                                <option value="Islas Malvinas Islas Falkland" >Islas Malvinas Islas Falkland</option>
                                <option value="Islas Marianas del norte" >Islas Marianas del norte</option>
                                <option value="Islas Marshall" >Islas Marshall</option>
                                <option value="Islas menores de Estados Unidos" >Islas menores de Estados Unidos</option>
                                <option value="Islas Palau" >Islas Palau</option>
                                <option value="Islas Salomón" >Islas Salomón</option>
                                <option value="Islas Tokelau" >Islas Tokelau</option>
                                <option value="Islas Turks y Caicos" >Islas Turks y Caicos</option>
                                <option value="Islas Vírgenes EE.UU." >Islas Vírgenes EE.UU.</option>
                                <option value="Islas Vírgenes Reino Unido" >Islas Vírgenes Reino Unido</option>
                                <option value="Israel" >Israel</option>
                                <option value="Italia" >Italia</option>
                                <option value="Jamaica" >Jamaica</option>
                                <option value="Japón" >Japón</option>
                                <option value="Jordania" >Jordania</option>
                                <option value="Kazajistán" >Kazajistán</option>
                                <option value="Kenia" >Kenia</option>
                                <option value="Kirguizistán" >Kirguizistán</option>
                                <option value="Kiribati" >Kiribati</option>
                                <option value="Kuwait" >Kuwait</option>
                                <option value="Laos" >Laos</option>
                                <option value="Lesoto" >Lesoto</option>
                                <option value="Letonia" >Letonia</option>
                                <option value="Líbano" >Líbano</option>
                                <option value="Liberia" >Liberia</option>
                                <option value="Libia" >Libia</option>
                                <option value="Liechtenstein" >Liechtenstein</option>
                                <option value="Lituania" >Lituania</option>
                                <option value="Luxemburgo" >Luxemburgo</option>
                                <option value="Macao R. A. E" >Macao R. A. E</option>
                                <option value="Madagascar" >Madagascar</option>
                                <option value="Malasia" >Malasia</option>
                                <option value="Malawi" >Malawi</option>
                                <option value="Maldivas" >Maldivas</option>
                                <option value="Malí" >Malí</option>
                                <option value="Malta" >Malta</option>
                                <option value="Marruecos" >Marruecos</option>
                                <option value="Martinica" >Martinica</option>
                                <option value="Mauricio" >Mauricio</option>
                                <option value="Mauritania" >Mauritania</option>
                                <option value="Mayotte" >Mayotte</option>
                                <option value="México" selected>México</option>
                                <option value="Micronesia" >Micronesia</option>
                                <option value="Moldavia" >Moldavia</option>
                                <option value="Mónaco" >Mónaco</option>
                                <option value="Mongolia" >Mongolia</option>
                                <option value="Montserrat" >Montserrat</option>
                                <option value="Mozambique" >Mozambique</option>
                                <option value="Namibia" >Namibia</option>
                                <option value="Nauru" >Nauru</option>
                                <option value="Nepal" >Nepal</option>
                                <option value="Nicaragua" >Nicaragua</option>
                                <option value="Níger" >Níger</option>
                                <option value="Nigeria" >Nigeria</option>
                                <option value="Niue" >Niue</option>
                                <option value="Norfolk" >Norfolk</option>
                                <option value="Noruega" >Noruega</option>
                                <option value="Nueva Caledonia" >Nueva Caledonia</option>
                                <option value="Nueva Zelanda" >Nueva Zelanda</option>
                                <option value="Omán" >Omán</option>
                                <option value="Panamá" >Panamá</option>
                                <option value="Papua Nueva Guinea" >Papua Nueva Guinea</option>
                                <option value="Paquistán" >Paquistán</option>
                                <option value="Paraguay" >Paraguay</option>
                                <option value="Perú" >Perú</option>
                                <option value="Pitcairn" >Pitcairn</option>
                                <option value="Polinesia francesa" >Polinesia francesa</option>
                                <option value="Polonia" >Polonia</option>
                                <option value="Portugal" >Portugal</option>
                                <option value="Puerto Rico" >Puerto Rico</option>
                                <option value="Qatar" >Qatar</option>
                                <option value="Reino Unido" >Reino Unido</option>
                                <option value="República Centroafricana" >República Centroafricana</option>
                                <option value="República Checa" >República Checa</option>
                                <option value="República de Sudáfrica" >República de Sudáfrica</option>
                                <option value="República Democrática del Congo Zaire" >República Democrática del Congo Zaire</option>
                                <option value="República Dominicana" >República Dominicana</option>
                                <option value="Reunión" >Reunión</option>
                                <option value="Ruanda" >Ruanda</option>
                                <option value="Rumania" >Rumania</option>
                                <option value="Rusia" >Rusia</option>
                                <option value="Samoa" >Samoa</option>
                                <option value="Samoa occidental" >Samoa occidental</option>
                                <option value="San Kitts y Nevis" >San Kitts y Nevis</option>
                                <option value="San Marino" >San Marino</option>
                                <option value="San Pierre y Miquelon" >San Pierre y Miquelon</option>
                                <option value="San Vicente e Islas Granadinas" >San Vicente e Islas Granadinas</option>
                                <option value="Santa Helena" >Santa Helena</option>
                                <option value="Santa Lucía" >Santa Lucía</option>
                                <option value="Santo Tomé y Príncipe" >Santo Tomé y Príncipe</option>
                                <option value="Senegal" >Senegal</option>
                                <option value="Serbia y Montenegro" >Serbia y Montenegro</option>
                                <option value="Sychelles" >Seychelles</option>
                                <option value="Sierra Leona" >Sierra Leona</option>
                                <option value="Singapur" >Singapur</option>
                                <option value="Siria" >Siria</option>
                                <option value="Somalia" >Somalia</option>
                                <option value="Sri Lanka" >Sri Lanka</option>
                                <option value="Suazilandia" >Suazilandia</option>
                                <option value="Sudán" >Sudán</option>
                                <option value="Suecia" >Suecia</option>
                                <option value="Suiza" >Suiza</option>
                                <option value="Surinam" >Surinam</option>
                                <option value="Svalbard" >Svalbard</option>
                                <option value="Tailandia" >Tailandia</option>
                                <option value="Taiwán" >Taiwán</option>
                                <option value="Tanzania" >Tanzania</option>
                                <option value="Tayikistán" >Tayikistán</option>
                                <option value="Territorios británicos del océano Indico" >Territorios británicos del océano Indico</option>
                                <option value="Territorios franceses del sur" >Territorios franceses del sur</option>
                                <option value="Timor Oriental" >Timor Oriental</option>
                                <option value="Togo" >Togo</option>
                                <option value="Tonga" >Tonga</option>
                                <option value="Trinidad y Tobago" >Trinidad y Tobago</option>
                                <option value="Túnez" >Túnez</option>
                                <option value="Turkmenistán" >Turkmenistán</option>
                                <option value="Turquía" >Turquía</option>
                                <option value="Tuvalu" >Tuvalu</option>
                                <option value="Ucrania" >Ucrania</option>
                                <option value="Uganda" >Uganda</option>
                                <option value="Uruguay" >Uruguay</option>
                                <option value="Uzbekistán" >Uzbekistán</option>
                                <option value="Vanuatu" >Vanuatu</option>
                                <option value="Venezuela" >Venezuela</option>
                                <option value="Vietnam" >Vietnam</option>
                                <option value="Wallis y Futuna" >Wallis y Futuna</option>
                                <option value="Yemen" >Yemen</option>
                                <option value="Zambia" >Zambia</option>
                                <option value="Zimbabue" >Zimbabue</option>
                            </select>
                            <br><br>
                            <div id="datos_tarjeta_form">
                                <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;">Datos de
                                    Tarjeta</p>
                                <label>Nombre Tarjetahabiente</label>
                                <input id="nombre_tarjeta" type="text" placeholder="Como aparece en la tarjeta"><br>
                                <label>Número de tarjeta <i class="far fa-credit-card"></i></label>
                                <input id="numero_tarjeta" type="number" placeholder="XXXXXXXXXX" maxlength="16" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                                <input id="cvv" class="cvv" type="number" placeholder="CVV" maxlength="4" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"><br>
                                <label>Fecha de expiración</label>
                                <input id="month" type="number" placeholder="MM" style="width: 50%;max-width: 120px;" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                                <input id="year" type="number" placeholder="AA" style="width: 50%; max-width: 120px;" maxlength="2" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                        </form>
                    </div>
                </div>


                <!-- CONFIRMACIÓN -->
                <div class="col-12 col-md-6">
                    <!-- cantidad -->
                    <div class="bg-gray border-gray pt-7 pb-7 pl-5 pr-5">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <p class="mb-0 dona_confirmacion">Confirmación</p>
                            </div>
                            <div class="col-12 col-md-6">
                                <p class="mb-0 dona_quantity" id="amount"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <p class="mb-0 dona_confirmacion">Tipo de donativo</p>
                            </div>
                            <div class="col-12 col-md-6">
                                <p class="mb-0 dona_quantity" id="donative_type"></p>
                            </div>
                        </div>
                    </div>
                    <!-- ventajas -->
                    <div class="dona_ventajas pt-5 pb-5"
                         style="border-bottom: 1.5px solid #1580b4!important;border-top: 1.5px solid #1580b4!important;">
                        <ul class="mb-0">
                            <li>Puedes pagar con puntos Bancomer</li>
                            <li>Donativo en efectivo, tarjeta o transferencia</li>
                            <li>Meses sin intereses</li>
                            <li>Deduce impuestos</li>
                            <li>Sitio Seguro</li>
                        </ul>
                    </div>
                    <!-- recibo -->
                    <div class="bg-gray border-gray pd-5">
                        <div class="row">
                            <div class="col-1 pr-0">
                                <input type="checkbox" id="anonymous">
                            </div>
                            <div class="col-10">
                                <label for="anonymous">¿Deseas aparecer como donante anonimo?</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1 pr-0">
                                <input type="checkbox" id="cfdi">
                            </div>
                            <div class="col-10">
                                <label for="cfdi">¿Deseas emitir CFDI deducible de impuestos?</label>
                                <input class="rfc" type="text" placeholder="RFC" id="rfc">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1 pr-0">
                                <input type="checkbox"  id="terms">
                            </div> 
                            <div class="col-10">
                                <label for="terms">Acepto los <a href="{{route('legal')}}">Términos y condiciones & Aviso de Privacidad</a></label>
                            </div>
                        </div>
                    </div>
                    <!-- botones -->
                    <div class="mt-5 mb-5">
                        <div class="row">
                            <div class="col-12 btns-container">
                                <table >
                                    <tr>
                                      <th>
                                        <button href="javascript:window.history.back();"class="dona_btns dona_regresar mg-3" style="color:white;">REGRESAR</button>
                                      </th>
                                      <th>
                                        <button  class="dona_btns btn-blue mg-3" id="pay-button" style="background-color: #1580b4!important; color:white;">DONAR</button>
                                      </th> 
                                    </tr>
                                  </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="#" method="POST" id="payment-form">
            <input type="hidden" name="use_card_points" id="use_card_points" value="false">
            <input type="hidden" data-openpay-card="holder_name" id="op-name">
            <input type="hidden" data-openpay-card="card_number" id="op-number">
            <input type="hidden" data-openpay-card="expiration_month" id="op-month">
            <input type="hidden" data-openpay-card="expiration_year" id="op-year">
            <input type="hidden" data-openpay-card="cvv2" id="op-cvv">
        </form>
    </section>

    <!-- FOOTER -->
    @include('layout.footer')

@endsection

@section('js')

    <script>
        //Persona fisica
        $(function() {
            //inicio openpaay
            OpenPay.setId('{{$openpayId}}');
            OpenPay.setApiKey('{{$openpayPk}}');
            {{$sandboxJS}}
            deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
            
            //Oculto peersona moral
            $('#moralTitle').css('display', 'none');
            $('#razon_social').css('display', 'none');
            $('#moralRep').css('display', 'none');
            moral ='no';

            //Oculto imagenes de tienda y transferencia
            $('#img_transferencia').css('display', 'none');
            $('#img_tiendas').css('display', 'none');

            //Checo si es pago unico o a meses con monto y id del proyecto
            modality = "{{$_GET["modality"]}}"; //once or month
            amount = "{{$_GET["amount"]}}"; //Obtengo el monto
            project_id = "{{$_GET["project_id"]}}"; 

            //Coloco el monto en el id amount y muestro el tipo de donativo
            $('#amount').html('<b>$ ' + amount + ' MXN</b>');
            if(modality==='once'){
                $('#donative_type').html('<b>Unico</b>');
            }else{
                $('#donative_type').html('<b>Mensual</b>');
                //Oculto el selector de pagos porque solo se pueden realizar compras de transferencia y tienda en pago unico
                $('#tipo_pago_multiples').css('display', 'none');
                $('#tipo_pago_multiples_title').css('display', 'none');
                $('#img_transferencias').css('display', 'none');
                $('#img_tiendas').css('display', 'none');
            }
              
        });

        //Abrir y cerrar rfc
        $('#cfdi').change(function () {
            if ($(this).is(":checked")) {
                $('.rfc').addClass('active');
            } else {
                $('.rfc').removeClass('active');
            }
            $('#cfdi').val($(this).is(':checked'));
        });

        //En caso de seleccionar otro tipo
        $('#moral').click(function(){
            $('#moralTitle').css('display', 'block');
            $('#razon_social').css('display', 'block');
            $('#moralRep').css('display', 'block');
            $('#fisicaShow').css('display', 'none');
            moral ='si';
        });

        $('#fisica').click(function(){
            $('#moralTitle').css('display', 'none');
            $('#razon_social').css('display', 'none');
            $('#moralRep').css('display', 'none');
            $('#fisicaShow').css('display', 'block');
            moral ='no';
        });

        //Cambia el color de los botones del tipo de persona
        $('.dona_representante-btn').click(function () {
            personType = $(this).val();
            $('.dona_representante-btn').removeClass('active');
            $(this).addClass('active');
        });

        //Selecciono el tipo de pago
        $('#tipo_pago').on('change', function() {
            if($('#tipo_pago').val()!='tarjeta'){
                //Oculto datos de tarjeta
                $('#datos_tarjeta_form').css('display', 'none');
            }else{
                //Muestro datos de tarjeta
                $('#datos_tarjeta_form').css('display', 'block');
            }
            //Imagenes del tipo de pago seleccionado
            if($('#tipo_pago').val()=='conveniencia'){
                $('#img_transferencia').css('display', 'none');
                $('#img_tiendas').css('display', 'block');
                $('#img_tarjetas').css('display', 'none');
            }else if($('#tipo_pago').val()=='transferencia'){
                $('#img_transferencia').css('display', 'block');
                $('#img_tiendas').css('display', 'none');
                $('#img_tarjetas').css('display', 'none');
            }else if($('#tipo_pago').val()=='tarjeta'){
                $('#img_transferencia').css('display', 'none');
                $('#img_tiendas').css('display', 'none');
                $('#img_tarjetas').css('display', 'block');
            }
        });
        
        $('#pay-button').on('click', function (event) {
            //Solo se puede hacer con javascript
            loaderSwal();
            msgError = "Completa los siguientes puntos:"
            event.preventDefault()
            $("#pay-button").attr("disabled", true);
            //Valido por modalidad
            if(modality==='once'){
                if(moral ==='no'){
                    if($('#tipo_pago').val()!='tarjeta'){
                        //Es pago unico persona fisica en tienda o banco
                        if(validateTiendaFisica()){
                            //ir al pago unico tienda 
                            generaPagoTienda($('#tipo_pago').val());
                        }else{
                            errorSwal(msgError);
                        }
                    }else{
                        //Es pago unico persona fisica con tarjeta
                        if(validateFisica()){
                            //ir al pago unico
                            asignDataToHiddenForm();
                            OpenPay.token.extractFormAndCreate('payment-form', success_callbak, error_callbak);
                        }else{
                            errorSwal(msgError);
                        }
                    }
                }else{
                    if($('#tipo_pago').val()!='tarjeta'){
                        //Es pago unico persona moral en tienda o banco
                        if(validateTiendaMoral()){
                            //ir al pago unico tienda
                            generaPagoTienda($('#tipo_pago').val());
                        }else{
                            errorSwal(msgError);
                        }
                    }else{
                        //Es pago unico persona moral con tarjeta
                        if(validateMoral()){
                            //ir al pago unico
                            asignDataToHiddenForm();
                            OpenPay.token.extractFormAndCreate('payment-form', success_callbak, error_callbak);
                        }else{
                            errorSwal(msgError);
                        }
                    }
                }
            }else{
                if(moral ==='no'){
                    //'Es pago mensual persona fisica
                    if(validateFisica()){
                        //ir al pago mensual
                        generaPagoSuscripcion();
                    }else{
                        errorSwal(msgError);
                    }
                }else{
                    //Es pago mensual persona moral
                    if(validateMoral()){
                        //ir al pago mensual
                        generaPagoSuscripcion();
                    }else{
                        errorSwal(msgError);
                    }
                }
            }
        });

        function errorSwal(mensaje){
            $("#pay-button").attr("disabled", false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: mensaje,
            });
        }

        function loaderSwal(){
            Swal.fire({
                title: 'Cargando',
                text: 'Procesando Pago',
                imageUrl: "{{asset('images/loader.gif')}}",
                imageWidth: 100,
                imageHeight: 100,
                imageAlt: 'Loading ...',
                allowOutsideClick: false,
                showConfirmButton: false
            });
        }

        function validateMoral(){
            errores = 0;
            if($('#razon_social').val().length< 2){
                msgError += '<br>- Nombre de razón social' ;
                errores += 1;
            }

            if($('#nombre').val().length< 2){
                msgError += '<br>- Nombre(s)' ;
                errores += 1;
            }

            if($('#paterno').val().length< 2){
                msgError += '<br>- Apellido paterno' ;
                errores += 1;
            }

            if($('#materno').val().length< 2){
                msgError += '<br>- Apellido materno' ;
                errores += 1;
            }

            if(!validateEmail($('#email').val()) || $('#email').val().length< 2){
                msgError += '<br>- Email Valido' ;
                errores += 1;
            }

            if(!validateNumber($('#celular').val()) || $('#celular').val().length< 10){
                msgError += '<br>- Celular de 10 dígitos' ;
                errores += 1;
            }

            if($('#nombre_tarjeta').val().length< 2){
                msgError += '<br>- Nombre del tarjetahabiente' ;
                errores += 1;
            }

            if(!validateNumber($('#numero_tarjeta').val()) || $('#numero_tarjeta').val().length< 15){
                msgError += '<br>- Numero de tarjeta' ;
                errores += 1;
            }

            if(!validateNumber($('#cvv').val()) || $('#cvv').val().length< 3){
                msgError += '<br>- Código de verificación CVV ó CVC' ;
                errores += 1;
            }

            if(!validateNumber($('#month').val()) || $('#month').val()< 1 || $('#month').val()> 12){
                msgError += '<br>- Mes de la tarjeta' ;
                errores += 1;
            }

            if(!validateNumber($('#year').val()) || $('#year').val()< 20 ){ //Año 2020
                msgError += '<br>- Año de la tarjeta' ;
                errores += 1;
            }

            if ($('#cfdi').prop('checked')) {
                if (!validateRFC($('#rfc').val())) {
                    msgError += '<br>- Si requieres CFDI debes ingresar un RFC válido';
                    errores += 1;
                }
            }

            if (!$('#terms').prop('checked')) {
                msgError += '<br>- Acepta los terminos y condiciones';
                errores += 1;
            }

            if(errores == 0){
                return true;
            }

            return false;
        }

        function validateFisica(){
            errores = 0;
            if($('#nombre').val().length< 2){
                msgError += '<br>- Nombre(s)' ;
                errores += 1;
            }

            if($('#paterno').val().length< 2){
                msgError += '<br>- Apellido paterno' ;
                errores += 1;
            }

            if($('#materno').val().length< 2){
                msgError += '<br>- Apellido materno' ;
                errores += 1;
            }

            if(!validateEmail($('#email').val()) || $('#email').val().length< 2){
                msgError += '<br>- Email Valido' ;
                errores += 1;
            }

            if(!validateNumber($('#celular').val()) || $('#celular').val().length< 10){
                msgError += '<br>- Celular de 10 dígitos' ;
                errores += 1;
            }

            if($('#nombre_tarjeta').val().length< 2){
                msgError += '<br>- Nombre del tarjetahabiente' ;
                errores += 1;
            }

            if(!validateNumber($('#numero_tarjeta').val()) || $('#numero_tarjeta').val().length< 15){
                msgError += '<br>- Numero de tarjeta' ;
                errores += 1;
            }

            if(!validateNumber($('#cvv').val()) || $('#cvv').val().length< 3){
                msgError += '<br>- Código de verificación CVV ó CVC' ;
                errores += 1;
            }

            if(!validateNumber($('#month').val()) || $('#month').val()< 1 || $('#month').val()> 12){
                msgError += '<br>- Mes de la tarjeta' ;
                errores += 1;
            }

            if(!validateNumber($('#year').val()) || $('#year').val()< 20 ){ //Año 2020
                msgError += '<br>- Año de la tarjeta' ;
                errores += 1;
            }

            if ($('#cfdi').prop('checked')) {
                if (!validateRFC($('#rfc').val())) {
                    msgError += '<br>- Si requieres CFDI debes ingresar un RFC válido';
                    errores += 1;
                }
            }

            if (!$('#terms').prop('checked')) {
                msgError += '<br>- Acepta los terminos y condiciones';
                errores += 1;
            }

            if(errores == 0){
                return true;
            }

            return false;
        }

        function validateTiendaMoral(){
            errores = 0;
            if($('#razon_social').val().length< 2){
                msgError += '<br>- Nombre de razón social' ;
                errores += 1;
            }

            if($('#nombre').val().length< 2){
                msgError += '<br>- Nombre(s)' ;
                errores += 1;
            }

            if($('#paterno').val().length< 2){
                msgError += '<br>- Apellido paterno' ;
                errores += 1;
            }

            if($('#materno').val().length< 2){
                msgError += '<br>- Apellido materno' ;
                errores += 1;
            }

            if(!validateEmail($('#email').val()) || $('#email').val().length< 2){
                msgError += '<br>- Email Valido' ;
                errores += 1;
            }

            if(!validateNumber($('#celular').val()) || $('#celular').val().length< 10){
                msgError += '<br>- Celular de 10 dígitos' ;
                errores += 1;
            }

            if ($('#cfdi').prop('checked')) {
                if (!validateRFC($('#rfc').val())) {
                    msgError += '<br>- Si requieres CFDI debes ingresar un RFC válido';
                    errores += 1;
                }
            }

            if (!$('#terms').prop('checked')) {
                msgError += '<br>- Acepta los terminos y condiciones';
                errores += 1;
            }

            if(errores == 0){
                return true;
            }

            return false;
        }

        function validateTiendaFisica(){
            errores = 0;
            if($('#nombre').val().length< 2){
                msgError += '<br>- Nombre(s)' ;
                errores += 1;
            }

            if($('#paterno').val().length< 2){
                msgError += '<br>- Apellido paterno' ;
                errores += 1;
            }

            if($('#materno').val().length< 2){
                msgError += '<br>- Apellido materno' ;
                errores += 1;
            }

            if(!validateEmail($('#email').val()) || $('#email').val().length< 2){
                msgError += '<br>- Email Valido' ;
                errores += 1;
            }

            if(!validateNumber($('#celular').val()) || $('#celular').val().length< 10){
                msgError += '<br>- Celular de 10 dígitos' ;
                errores += 1;
            }

            if ($('#cfdi').prop('checked')) {
                if (!validateRFC($('#rfc').val())) {
                    msgError += '<br>- Si requieres CFDI debes ingresar un RFC válido';
                    errores += 1;
                }
            }

            if (!$('#terms').prop('checked')) {
                msgError += '<br>- Acepta los terminos y condiciones';
                errores += 1;
            }

            if(errores == 0){
                return true;
            }

            return false;
        }

        function validateEmail(mail) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
                return true;
            }
                return false;
        }

        function validateNumber(number) {
            if(/^\d+$/.test(number)){
                return true;
            }
                return false;
        }

        function validateRFC(rfc_, aceptarGenerico = true) {
            let rfc = rfc_.toUpperCase();

            const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
            var validado = rfc.match(re);

            if (!validado)  //Coincide con el formato general del regex?
                return false;

            //Separar el dígito verificador del resto del RFC
            const digitoVerificador = validado.pop(),
                rfcSinDigito = validado.slice(1).join(''),
                len = rfcSinDigito.length,

                //Obtener el digito esperado
                diccionario = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
                indice = len + 1;
            var suma,
                digitoEsperado;

            if (len == 12) suma = 0;
            else suma = 481; //Ajuste para persona moral

            for (var i = 0; i < len; i++)
                suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
            digitoEsperado = 11 - suma % 11;
            if (digitoEsperado == 11) digitoEsperado = 0;
            else if (digitoEsperado == 10) digitoEsperado = "A";

            //El dígito verificador coincide con el esperado?
            // o es un RFC Genérico (ventas a público general)?
            if ((digitoVerificador != digitoEsperado)
                && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
                return false;
            else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
                return false;
            return true;
        }

        function generaPagoTienda(tipoPago){

            $.post("{{route('genera_ficha')}}",
            {
                razon: $('#razon_social').val(),
                nombre: $('#nombre').val(),
                paterno: $('#paterno').val(),
                materno: $('#materno').val(),
                country:$('#nationality').val(),
                email: $('#email').val(),
                telefono: $('#celular').val(),
                anonymous: $("#anonymous").prop('checked')?'true':'false',
                cfdi:$("#cfdi").prop('checked')?'true':'false',
                rfc: $('#rfc').val(),
                tipo: tipoPago,
                id:project_id,
                moral: moral,
                monto: amount,
            }).done(function(data){
                $("#pay-button").attr("disabled", false);
                if(data["result"]=='error'){
                    errorSwal(data["msg"]);
                }else if(data["result"]=='success'){
                    window.location.assign(data["data"]["receipt"]);
                }else{
                    errorSwal('Revisa tu información e inténtalo de nuevo');
                }
            }).fail(function(data) {
                $("#pay-button").attr("disabled", false);
                errorSwal('Revisa tu información e inténtalo de nuevo');
            });
        }

        function generaPagoCard(tokenData, pointsData){

            $.post("{{route('card')}}",
            {
                razon: $('#razon_social').val(),
                nombre: $('#nombre').val(),
                paterno: $('#paterno').val(),
                materno: $('#materno').val(),
                country:$('#nationality').val(),
                email: $('#email').val(),
                telefono: $('#celular').val(),
                anonymous: $("#anonymous").prop('checked')?'true':'false',
                cfdi:$("#cfdi").prop('checked')?'true':'false',
                rfc: $('#rfc').val(),
                id:project_id,
                moral: moral,
                monto: amount,
                token_id:tokenData,
                device:deviceSessionId,
                points:pointsData
            }).done(function(data){
                $("#pay-button").attr("disabled", false);
                if(data["result"]=='error'){
                    errorSwal(data["msg"]);
                }else if(data["result"]=='success'){
                    if(data["url"]!=null){
                        window.location.assign(data["url"]);
                    }else{
                        window.location.assign("{{route('thanks')}}");
                    }
                }else{
                    errorSwal('Revisa tu información e inténtalo de nuevo');
                }
            }).fail(function(data) {
                $("#pay-button").attr("disabled", false);
                errorSwal('Revisa tu información e inténtalo de nuevo');
            });
        }

        function asignDataToHiddenForm(){
            $('#op-name').val($('#nombre_tarjeta').val());
            $('#op-number').val($('#numero_tarjeta').val());
            $('#op-month').val($('#month').val());
            $('#op-year').val($('#year').val());
            $('#op-cvv').val($('#cvv').val());
        }

        //Resultado exitoso de openpay
        var success_callbak = function(response) {
              var token_id = response.data.id; 
              if(response.data.card.points_card){
                //Tarjeta con puntos, ostrar cuadro de puntos
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                });

                swalWithBootstrapButtons.fire({
                    title: 'Puedes utilizar puntos',
                    text: "¿Deseas utilizar puntos BBVA?",
                    imageUrl: "{{asset('images/points.gif')}}",
                    imageWidth: 100,
                    imageHeight: 100,
                    imageAlt: 'Points ...',
                    allowOutsideClick: false,
                    showCancelButton: true,
                    confirmButtonText: 'Si, usar puntos',
                    cancelButtonText: 'No usar puntos',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        //Si usar puntos
                        generaPagoCard(token_id, true);
                        loaderSwal();
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        //No usar puntos
                        generaPagoCard(token_id, false);
                        loaderSwal();
                    }
                });
              }else{
                //tarjeta sin puntos
                generaPagoCard(token_id, false);
              }
        };

        //Error de openpay
        var error_callbak = function(response) {
            var desc = response.data.description != undefined ?
                response.data.description : response.message;
            $("#pay-button").attr("disabled", false);
            errorSwal("ERROR [" + response.status + "] " + desc);
        };

        //Susccripción
        function generaPagoSuscripcion(){

            $.post("{{route('suscripcion')}}",
            {
                razon: $('#razon_social').val(),
                nombre: $('#nombre').val(),
                paterno: $('#paterno').val(),
                materno: $('#materno').val(),
                country:$('#nationality').val(),
                email: $('#email').val(),
                telefono: $('#celular').val(),
                anonymous: $("#anonymous").prop('checked')?'true':'false',
                cfdi:$("#cfdi").prop('checked')?'true':'false',
                rfc: $('#rfc').val(),
                id:project_id,
                moral: moral,
                monto: amount,
                cardname:$('#nombre_tarjeta').val(),
                card:$('#numero_tarjeta').val(),
                month:$('#month').val(),
                year:$('#year').val(),
                cvv:$('#cvv').val()
            }).done(function(data){
                $("#pay-button").attr("disabled", false);
                if(data["result"]=='error'){
                    errorSwal(data["msg"]);
                }else if(data["result"]=='success'){
                    window.location.assign("{{route('thanks')}}");
                }else{
                    errorSwal('Revisa tu información e inténtalo de nuevo');
                }
            }).fail(function(data) {
                $("#pay-button").attr("disabled", false);
                errorSwal('Revisa tu información e inténtalo de nuevo');
            });
        }

    </script>

@endsection
