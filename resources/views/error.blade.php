
@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Error" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:image" content="{{asset('logo.png')}}">
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>Error | Providencia</title>
@endsection

@section('content')

    <style>
        #loader {
            display: none;
        }

        svg {
            width: 100%;
            max-width: 200px;
            border-radius: 3px;
            box-shadow: 2px 2px 5px #fff;
            background: #fff;
            fill: none;
            stroke: #7b97ac;
            stroke-linecap: round;
            stroke-width: 8%
        }

        use {
            stroke: #95d600;
            animation: a 1.2s linear infinite
        }

        @keyframes a {
            to {
                stroke-dashoffset: 0px
            }
        }

        .modal-content-error {
            border-radius: 0px;
            border: none;
            text-align: center;
        }

        .modal-body-error {
            min-height: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .errorBar {
            height: 70px;
            background: #ff3636;
            position: absolute;
            width: 100%;
            text-align: center;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 99999;
            top: 0;
            color: white;
            font-size: 2.5vw;
            animation-duration: all .200s;
        }

        .dona_ventajas ul li::before {
            content: "•";
            color: #1580b4 !important;
            font-weight: bold;
            display: inline-block;
            width: 1em;
            margin-left: -1em;
        }
        .errCode{
            font-size: 2vw;
        }
    </style>



    <section class="dona">
        <!-- BUTTONS -->
        <div class="container-fluid bg-blue pt-2 pb-2" style="background:#1580b4;">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center text-white mb-0"><b>¡Error!</b>
                    </h2>
                </div>
            </div>
        </div>
        <div class="container pt-5 pb-5">
            <div class="row">
                <!-- MÉTODO DE PAGO -->
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;">Ocurrió el siguiente error con tu método de pago:
                            </p>
                        </div>
                    </div>





                    <div class="col-12">
                        <div class="mainError">

                            <p class="errCode">
                            {{$res->msg}}
                            </p>
                        </div>

                    </div>

                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Inicio</button>
                    </div>
                </div>


            </div>
        </div>
        <div class="modal fade" id="notificationModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content notif-content">
                    <div class="modal-body notif-body">
                        <p class="notif-msg" id="error_msgTxt"></p>
                    </div>

                </div>
            </div>
        </div>



    </section>

    <!-- FOOTER -->
    @include('layout.footer')

@endsection

