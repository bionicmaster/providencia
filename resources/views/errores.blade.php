@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Errores" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>Errores | Providencia</title>
@endsection

@section('content')
    <header class="header-thanks">
        @include('partials.main-navigation')
        <div class="jumbotron bg-transparent text-white text-center">
            
<style type="text/css">
    

    .errormsg{
    background: #ffffffb3;
    font-size: 2vw;
    height: 150px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #000000a3;
    font-weight: bold;
    }

    .button_thingy{
            height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    }
</style>
         
<div class="errormsg"> {{$res->msg}}</div>



<div class="col-xs-12 button_thingy">
    <button onclick="window.location.href = '{{route('home')}}'" class="btn btn-info"> Inicio </button>
</div>

        </div>
    </header>
    @include('layout.footer')

@endsection
    
