@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Preguntas Frecuentes" /> 
    <meta name="keywords" content="providencia, Providencia, Preguntas, Frecuentes, preguntas, frecuentes"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}" />
    <title>FAQs | Providencia</title>
@endsection

@section('content')

@include('layout.navigation')


<!-- HEADER -->
<header class="providencia-header providencia-header--faq">
</header>

<section class="faq pt-10 pb-10">
    <div class="container">
        <!-- title -->
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="providencia-title pb-3 espanol">PREGUNTAS FRECUENTES</h1>
                <h1 class="providencia-title pb-3 english">FAQ</h1>
            </div>
        </div>

        <div class="row">
            <!-- PARA DONANTES -->
            <div class="col-12 col-md-6 faq_column">
                <h2 class="providencia-subtitle text-center mt-10 mb-10 espanol">PARA DONANTES</h2>
                <h2 class="providencia-subtitle text-center mt-10 mb-10 english">FOR DONORS</h2>
                <!-- accordion -->
                <div id="accordion">
                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse1">
                            <div class="card-header"><span class="card_number">1</span> <span class="card_title"> Sobre esta plataforma </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse1">
                            <div class="card-header pl-0"><span class="card_number">1</span> <span class="card_title"> About this platform </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse1" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Fundación Providencia desarolla la primera plataforma de crowdfunding 100% filantrópica. Su objetivo es conectar héroes con instituciones filantrópicas y brindar apoyo a las instituciones para profesionalizarse tanto en el área administrativa, legal, operativa y de comunicación. Es la única plataforma de crowdfunding que no cobra comisión por las transacciones.</div>
                            <div class="card-body english">Fundación Providencia develops the first 100% philanthropic crowdfunding platform. Its objective is to connect heroes with philanthropic institutions and provide support to institutions to professionalize both in the administrative, legal, operational and communication areas. It is the only crowdfunding platform that does not charge a commission for transactions.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse2">
                            <div class="card-header"><span class="card_number">2</span> <span class="card_title"> ¿Qué es el Crowd Funding? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse2">
                            <div class="card-header pl-0"><span class="card_number">2</span> <span class="card_title"> What is Crowd Funding? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse2" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">El crowdfunding es un sistema de financiación colectiva para la procuración de fondos. Somos la primer plataforma de crowdfunding 100% filantrópica y que el único objetivo es impulsar a las OSC de alto impacto social, no cobra un solo peso de comisión entregando así a la OSC la cantidad TOTAL del monto recolectado.</div>
                            <div class="card-body english">Crowdfunding is a crowdfunding system for fundraising. We are the first 100% philanthropic crowdfunding platform and that the only objective is to promote OSC with high social impact, it does not charge a single weight of commission, thus giving the OSC the TOTAL amount of the amount collected.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse3">
                            <div class="card-header"><span class="card_number">3</span> <span class="card_title"> ¿Cómo puedo donar? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse3">
                            <div class="card-header pl-0"><span class="card_number">3</span> <span class="card_title"> How can I donate? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse3" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Para donar debes ingresar al sitio y seleccionar la opción “Donar a un proyecto”. Dentro de esta sección encontrarás la opción de conocer tanto los Proyectos Permanentes como los Proyectos Temporales que participan. Una vez elegido el proyecto con el cual quieres colaborar da clic en la opción “Donar”. De lado derecho encontrarás la sección de donativos en la cual podrás elegir la cantidad que deseas aportar a la causa. Una vez elegido el monto aparecerá una pestaña en la cual se verifica que el monto sea el correcto. Una vez hecho esto da clic en la opción “Donar” que se encuentra en la parte inferior. Continúa con el proceso hasta llegar a las opciones de Método de pago y elige el que más te convenga.</div>
                            <div class="card-body english">To donate you must enter the site and select the option “Donate to a project”. Within this section you will find the option to know both the Permanent Projects and the Temporary Projects that participate. Once you have chosen the project with which you want to collaborate, click on the “Donate” option. On the right side you will find the donations section where you can choose the amount you want to contribute to the cause. Once the amount has been chosen, a tab will appear in which the amount is verified to be correct. Once this is done, click on the “Donate” option at the bottom. Continue the process until you reach the Payment method options and choose the one that suits you best.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse4">
                            <div class="card-header"><span class="card_number">4</span> <span class="card_title"> ¿Cuáles son los métodos de pago? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse4">
                            <div class="card-header pl-0"><span class="card_number">4</span> <span class="card_title"> What are the payment methods? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse4" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">
                                Nuestra plataforma que permite distintos métodos de pago. Tu donativo se puede pagar de las siguientes maneras. <br>
                                • Puntos Bancomer<br>
                                • En efectivo<br>
                                • Con tarjeta de crédito o débito<br>
                                • Efectivo (Oxxo)<br>
                                • Transferencia<br>
                            </div>
                            <div class="card-body english">
                                Our platform that allows different payment methods. Your donation can be paid in the following ways. <br>
                                • Bancomer Points <br>
                                • Cash <br>
                                • By credit or debit card <br>
                                • Cash (Oxxo) <br>
                                • Transfer <br>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse5">
                            <div class="card-header"><span class="card_number">5</span> <span class="card_title"> ¿Las donaciones son deducibles de impuestos? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse5">
                            <div class="card-header pl-0"><span class="card_number">5</span> <span class="card_title"> Are donations tax deductible? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse5" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Así es, todas las donaciones son libres de impuestos, por lo que es importante que tu organización sea una A.C o una I.A.P y que puedas recibir donativos deducibles de impuestos sobre la renta y tener todas tus declaraciones en forma.</div>
                            <div class="card-body english">That's right, all donations are tax-free, so it is important that your organization is an A.C or an I.A.P and that you can receive income tax deductible donations and have all your returns in shape.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse6">
                            <div class="card-header"><span class="card_number">6</span> <span class="card_title"> ¿Cómo puedo obtener CFDI deducible de impuestos por mi donación? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse6">
                            <div class="card-header pl-0"><span class="card_number">6</span> <span class="card_title"> How can I obtain a tax deductible CFDI for my donation? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse6" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Al inicio del proceso de donación aparece un botón el cual te permite realizar tu CFDI deducible de impuestos. Presionando ese botón y completando tus datos se generará una factura la cual recibirás en tu correo electrónico en el transcurso de 10 días con la confirmación de tu factura así como la aprobación de tu donativo.</div>
                            <div class="card-body english">At the beginning of the donation process, a button appears which allows you to make your CFDI tax deductible. Pressing that button and completing your information, an invoice will be generated which you will receive in your email within 10 days with the confirmation of your invoice as well as the approval of your donation.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse7">
                            <div class="card-header"><span class="card_number">7</span> <span class="card_title"> ¿Qué diferencia hay entre Proyectos Permanentes y Proyectos Temporales? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse7">
                            <div class="card-header pl-0"><span class="card_number">7</span> <span class="card_title"> What is the difference between Permanent Projects and Temporary Projects? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse7" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">
                                <b>Proyectos Permanentes</b><br>
                                Los proyectos permanentes son aquellos que te permiten hacer donativos de una sola expedición o domiciliar donativos a Organizaciones cuya causa empatice con tu objetivo de ayuda.
                                <br><br>
                                A diferencia de los Proyectos Temporales que plantean la necesidad inmediata que posee la Organización, los Proyectos Permanentes son la razón de ser de la OSC y su necesidad del día a día.

                                <br><br>
                                <b>Proyectos Temporales</b><br>
                                Los proyectos temporales son proyectos específicos que las Organizaciones requieren con urgencia y cuya meta debe cumplirse en un corto plazo (30-60 días).
                                <br><br>
                                Los Proyectos Temporales, a diferencia de los Proyectos Permanentes, buscan la procuración de fondos para cubrir las necesidades de un futuro inmediato de la OSC y depende del Crowdfunding para lograr su meta. No importa si tu aportación es grande o chica, ya que para alcanzar una meta colectiva, el primer paso es individual.
                            </div>
                            <div class="card-body english">
                                <b> Permanent Projects </b> <br>
                                Permanent projects are those that allow you to make donations from a single expedition or to domicile donations to Organizations whose cause empathizes with your aid objective.
                                <br><br>
                                Unlike Temporary Projects that pose the immediate need of the Organization, Permanent Projects are the raison d'être of the CSO and its day-to-day need.
                               
                                <br><br>
                                <b> Temporary Projects </b> <br>
                                Temporary projects are specific projects that Organizations urgently require and whose goal must be met in the short term (30-60 days).
                                <br><br>
                                Temporary Projects, unlike Permanent Projects, seek to obtain funds to cover the needs of the immediate future of the CSO and depend on Crowdfunding to achieve their goal. It does not matter if your contribution is large or small, since to achieve a collective goal, the first step is individual.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse8">
                            <div class="card-header"><span class="card_number">8</span> <span class="card_title"> ¿Por qué somos una plataforma 100% filantrópica? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse8">
                            <div class="card-header pl-0"><span class="card_number">8</span> <span class="card_title"> Why are we a 100% philanthropic platform? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse8" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">
                                Roberto González Moreno es un empresario mexicano que dedica gran parte de su tiempo a la labor social. Él y su familia trabajaban con múltiples organizaciones sociales y evocaban la pobreza e impulsaban la educación. Sin embargo, después de un tiempo afrontó la realidad que sin importar la capacidad económica y la voluntad de cualquier empresario, es imposible que una persona o un grupo de personas puedan resolver todos los problemas sociales. Es así como se plantean dos puntos importantes: la conciencia social y el saber dónde ayudar. Teniendo en cuenta estas dos variables nace Fundación Providencia donde buscan la fórmula para multiplicar la capacidad de ayuda y aumentar la cantidad de donantes.
                                <br><br>
                                Más tarde surge la idea de un crowdfunding que ayude a las OSC, orientando a los donatarios para saber dónde ayudar y al mismo tiempo formar un ecosistema filantrópico en donde se cree una red de voluntarios y donantes.
                                <br><br>
                                Fundación Providencia se da cuenta que se manifiestan dos problemáticas en las OSC, tanto en la parte fiscal y administrativa, como en la de mercadotecnia y publicidad. Por lo que Roberto González decide hacer una alianza con la agencia de mercadotecnia. Ambos fundan una plataforma de crowdfunding, que además ofrece asesoría para ayudar a las OSC inscritas a resolver problemáticas y tener mayor impacto, focalizando su capacidad de ayuda. Sin cobrar comisión, nuestra plataforma tiene como objetvo impulsar a miles de organizaciones sociales.
                            </div>
                            <div class="card-body english">
                                Roberto González Moreno is a Mexican businessman who dedicates much of his time to social work. He and his family worked with multiple social organizations and evoked poverty and promoted education. However, after a time he faced the reality that regardless of the economic capacity and will of any entrepreneur, it is impossible for one person or a group of people to solve all social problems. This is how two important points are raised: social awareness and knowing where to help. Taking these two variables into account, the Providencia Foundation was born, where they look for the formula to multiply aid capacity and increase the number of donors.
                                <br><br>
                                Later, the idea of ​​a crowdfunding that helps OSC arises, guiding donors to know where to help and at the same time form a philanthropic ecosystem where a network of volunteers and donors is created.
                                <br><br>
                                Fundación Providencia realizes that there are two problems in OSC, both fiscally and administratively, as well as in marketing and advertising. So Roberto González decides to make an alliance with the marketing agency. Both found a crowdfunding platform, which also offers advice to help registered OSC solve problems and have a greater impact, focusing their capacity to help. Without charging commission, our platform aims to boost thousands of social organizations.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse9">
                            <div class="card-header"><span class="card_number">9</span> <span class="card_title"> ¿Por qué no cobramos comisión? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse9">
                            <div class="card-header pl-0"><span class="card_number">9</span> <span class="card_title"> Why do we not charge commission? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse9" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Nuestra plataforma no cobra comisión ya que el único objetivo de Fundación Providencia, es impulsar a las OSC de alto impacto social, creando el ecosistema más importante de organizaciones sociales no lucrativas en México.</div>
                            <div class="card-body english">Our platform does not charge commission since the sole objective of the Providencia Foundation is to promote OSC with high social impact, creating the most important ecosystem of non-profit social organizations in Mexico.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse10">
                            <div class="card-header"><span class="card_number">10</span> <span class="card_title"> ¿Es seguro donar en la plataforma? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse10">
                            <div class="card-header pl-0"><span class="card_number">10</span> <span class="card_title"> Is it safe to donate on the platform? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse10" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">
                                ¡Por supuesto! Utilizamos el sistema de multipagos Bancomer a través del cual podrás pagar con puntos Bancomer, hacer donativos a través de cualquier tarjeta de crédito o débito y realizar depósitos en efectivo en Oxxo.
                                <br><br>
                                Tu seguridad es primero, por eso nos aseguramos que el sitio utiliza todos los candados de Bancomer. Además contamos con un servicio privado https de alta
                                <br><br>
                                seguridad, y con un equipo de expertos en ciberseguridad que pueden guiarte en un chat durante todo el proceso.
                            </div>
                            <div class="card-body english">
                                Of course! We use the Bancomer multi-payment system through which you can pay with Bancomer points, make donations through any credit or debit card and make cash deposits at Oxxo.
                                <br><br>
                                Your security comes first, that's why we make sure that the site uses all Bancomer locks. We also have a private https registration service
                                <br><br>
                                security, and with a team of cybersecurity experts who can guide you in a chat throughout the process.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse11">
                            <div class="card-header"><span class="card_number">11</span> <span class="card_title"> ¿Puedo domiciliar mi pago? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse11">
                            <div class="card-header pl-0"><span class="card_number">11</span> <span class="card_title"> Can I domicile my payment? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse11" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">Así es, dentro del sitio podrás encontrar la opción del cargo permanente, la cual te permitirá seguir ayudando sin tener preocupaciones de olvidarlo, y tener la comodidad de no estar realizando el proceso de pago cada vez que quieras ayudar.</div>
                            <div class="card-body english">That's right, within the site you can find the permanent charge option, which will allow you to continue helping without having to worry about forgetting it, and have the comfort of not being in the payment process every time you want to help.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapse12">
                            <div class="card-header"><span class="card_number">12</span> <span class="card_title"> ¿Qué significa domiciliar mi pago? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapse12">
                            <div class="card-header pl-0"><span class="card_number">12</span> <span class="card_title"> What does direct debit my payment mean? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapse12" class="collapse" data-parent="#accordion">
                            <div class="card-body espanol">La Domiciliación es una manera de realizar tus pagos de forma automática con cargo ya sea a tu tarjeta de nómina, cuenta de cheques, o tarjeta de débito o crédito. De esta manera ya no tendrás que preocuparte por estar al corriente de los pagos y tendrás la comodidad de liberarte de preocupaciones.</div>
                            <div class="card-body english">Direct Debit is a way to make your payments automatically charged either to your payroll card, checking account, or debit or credit card. In this way you will no longer have to worry about being up to date with payments and you will have the comfort of freeing yourself from worries.</div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- PARA ORGANIZACIONES -->
            <div class="col-12 col-md-6 faq_column">
                <h2 class="providencia-subtitle text-center mt-10 mb-10 espanol">PARA ORGANIZACIONES</h2>
                <h2 class="providencia-subtitle text-center mt-10 mb-10 english">FOR ORGANIZATIONS</h2>
                <!-- accordion -->
                <div id="accordion2">
                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapseR1">
                            <div class="card-header"><span class="card_number">1</span> <span class="card_title"> Sobre esta plataforma </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapseR1">
                            <div class="card-header pl-0"><span class="card_number">1</span> <span class="card_title"> About this platform </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapseR1" class="collapse" data-parent="#accordion2">
                            <div class="card-body espanol">Fundación Providencia desarolla la primera plataforma de crowdfunding 100% filantrópica. Su objetivo es conectar héroes con instituciones filantrópicas y brindar apoyo a las instituciones para profesionalizarse tanto en el área administrativa, legal, operativa y de comunicación. Es la única plataforma de crowdfunding que no cobra comisión por las transacciones.</div>
                            <div class="card-body english">Fundación Providencia develops the first 100% philanthropic crowdfunding platform. Its objective is to connect heroes with philanthropic institutions and provide support to institutions to professionalize both in the administrative, legal, operational and communication areas. It is the only crowdfunding platform that does not charge a commission for transactions.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapseR2">
                            <div class="card-header"><span class="card_number">2</span> <span class="card_title"> ¿Qué es el Crowd Funding? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapseR2">
                            <div class="card-header pl-0"><span class="card_number">2</span> <span class="card_title"> What is Crowd Funding? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapseR2" class="collapse" data-parent="#accordion2">
                            <div class="card-body espanol">El crowdfunding es un sistema de financiación colectiva para la procuración de fondos. Somos la primer plataforma de crowdfunding 100% filantrópica y que el único objetivo es impulsar a las OSC de alto impacto social, no cobra un solo peso de comisión entregando así a la OSC la cantidad TOTAL del monto recolectado.</div>
                            <div class="card-body english">Crowdfunding is a crowdfunding system for fundraising. We are the first 100% philanthropic crowdfunding platform and that the only objective is to promote OSC with high social impact, it does not charge a single weight of commission, thus giving the OSC the TOTAL amount of the amount collected.</div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapseR3">
                            <div class="card-header"><span class="card_number">3</span> <span class="card_title"> ¿Qué diferencia hay entre Proyectos Permanentes y Proyectos Temporales? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapseR3">
                            <div class="card-header pl-0"><span class="card_number">3</span> <span class="card_title"> What is the difference between Permanent Projects and Temporary Projects? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapseR3" class="collapse" data-parent="#accordion2">
                            <div class="card-body espanol">
                                <b>Proyectos Permanentes </b><br>
                                Los proyectos permanentes son aquellos que te permiten hacer donativos de una sola expedición o domiciliar donativos a Organizaciones cuya causa empatice con tu objetivo de ayuda.
                                <br><br>
                                A diferencia de los Proyectos Temporales que plantean la necesidad inmediata que posee la Organización, los Proyectos Permanentes son la razón de ser de la OSC y su necesidad del día a día.
                                <br><br>
                                <b>Proyectos Temporales</b><br>
                                Los proyectos temporales son proyectos específicos que las Organizaciones requieren con urgencia y cuya meta debe cumplirse en un corto plazo (30-60 días).
                                <br><br>
                                Los Proyectos Temporales, a diferencia de los Proyectos Permanentes, buscan la procuración de fondos para cubrir las necesidades de un futuro inmediato de la OSC y depende del Crowdfunding para lograr su meta. No importa si tu aportación es grande o chica, ya que para alcanzar una meta colectiva, el primer paso es individual.
                            </div>
                            <div class="card-body english">
                                <b>Permanent Projects </b> <br>
                                Permanent projects are those that allow you to make donations from a single expedition or to domicile donations to Organizations whose cause empathizes with your aid objective.
                                <br><br>
                                Unlike Temporary Projects that pose the immediate need of the Organization, Permanent Projects are the raison d'être of the OSC and its day-to-day need.
                                <br><br>
                                <b> Temporary Projects </b> <br>
                                Temporary projects are specific projects that Organizations urgently require and whose goal must be met in the short term (30-60 days).
                                <br><br>
                                Temporary Projects, unlike Permanent Projects, seek to obtain funds to cover the needs of the immediate future of the OSC and depend on Crowdfunding to achieve their goal. It does not matter if your contribution is large or small, since to achieve a collective goal, the first step is individual.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapseR4">
                            <div class="card-header"><span class="card_number">4</span> <span class="card_title"> ¿Por qué somos una plataforma 100% filantrópica? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapseR4">
                            <div class="card-header pl-0"><span class="card_number">4</span> <span class="card_title"> Why are we a 100% philanthropic platform? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapseR4" class="collapse" data-parent="#accordion2">
                            <div class="card-body espanol">
                                Roberto González Moreno es un empresario mexicano que dedica gran parte de su tiempo a la labor social. Él y su familia trabajaban con múltiples organizaciones sociales y evocaban la pobreza e impulsaban la educación. Sin embargo, después de un tiempo afrontó la realidad que sin importar la capacidad económica y la voluntad de cualquier empresario, es imposible que una persona o un grupo de personas puedan resolver todos los problemas sociales. Es así como se plantean dos puntos importantes: la conciencia social y el saber dónde ayudar. Teniendo en cuenta estas dos variables nace Fundación Providencia donde buscan la fórmula para multiplicar la capacidad de ayuda y aumentar la cantidad de donantes.
                                <br><br>
                                Más tarde surge la idea de un crowdfunding que ayude a las OSC, orientando a los donatarios para saber dónde ayudar y al mismo tiempo formar un ecosistema filantrópico en donde se cree una red de voluntarios y donantes.
                                <br><br>
                                Fundación Providencia se da cuenta que se manifiestan dos problemáticas en las OSC, tanto en la parte fiscal y administrativa, como en la de mercadotecnia y publicidad. Por lo que Roberto González decide hacer una alianza con la agencia de mercadotecnia. Ambos fundan una plataforma de crowdfunding, que además ofrece asesoría para ayudar a las OSC inscritas a resolver problemáticas y tener mayor impacto, focalizando su capacidad de ayuda. Sin cobrar comisión, nuestra plataforma tiene como objetvo impulsar a miles de organizaciones sociales.
                            </div>
                            <div class="card-body english">
                                Roberto González Moreno is a Mexican businessman who dedicates much of his time to social work. He and his family worked with multiple social organizations and evoked poverty and promoted education. However, after a time he faced the reality that regardless of the economic capacity and will of any entrepreneur, it is impossible for one person or a group of people to solve all social problems. This is how two important points are raised: social awareness and knowing where to help. Taking these two variables into account, the Providencia Foundation was born, where they look for the formula to multiply aid capacity and increase the number of donors.
                                <br><br>
                                Later, the idea of ​​a crowdfunding that helps OSC arises, guiding donors to know where to help and at the same time form a philanthropic ecosystem where a network of volunteers and donors is created.
                                <br><br>
                                Fundación Providencia realizes that there are two problems in OSC, both fiscally and administratively, as well as in marketing and advertising. So Roberto González decides to make an alliance with the marketing agency. Both found a crowdfunding platform, which also offers advice to help registered OSC solve problems and have a greater impact, focusing their capacity to help. Without charging commission, our platform aims to boost thousands of social organizations.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <a class="card-link espanol" data-toggle="collapse" href="#collapseR5">
                            <div class="card-header"><span class="card_number">5</span> <span class="card_title"> ¿Por qué no cobramos comisión? </span><span class="card_icon"></span></div>
                        </a>
                        <a class="card-link english" data-toggle="collapse" href="#collapseR5">
                            <div class="card-header pl-0"><span class="card_number">5</span> <span class="card_title"> Why do we not charge commission? </span><span class="card_icon"></span></div>
                        </a>
                        <div id="collapseR5" class="collapse" data-parent="#accordion2">
                            <div class="card-body espanol">Nuestra plataforma no cobra comisión ya que el único objetivo de Fundación Providencia, es impulsar a las OSC de alto impacto social, creando el ecosistema más importante de organizaciones sociales no lucrativas en México.</div>
                            <div class="card-body english">Our platform does not charge commission since the sole objective of the Providencia Foundation is to promote OSC with high social impact, creating the most important ecosystem of non-profit social organizations in Mexico.</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section('js')
<script>
    var changeCardIcon = function() {
        var cardLink = $('.card-link');

        cardLink.click(function() {
            cardLink.removeClass('blue');
            $('.card').removeClass('blueBorder');
            var selectedLink = $(this);

            setTimeout(function() {
                if (selectedLink.hasClass("collapsed")) {
                    selectedLink.removeClass('blue');
                } else {
                    selectedLink.addClass('blue');
                    selectedLink.closest('.card').addClass('blueBorder');
                }
            }, 100);

        })

    };
    changeCardIcon();
</script>
@endsection