@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Formulario" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición, Formulario, formulario"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}" />
    <title>Subir Proyecto| Providencia</title>
@endsection
@section('head')

<title>Fundación Providencia</title>
@endsection

@section('content')

@include('layout.navigation')

<!-- HEADER -->
<header class="providencia-header providencia-header--subir">
</header>

<!-- FORM -->
<section class="providencia-subir pt-10 pb-10">
    <div class="container">
        <!-- payment methods -->
        <div class="row">
            <div class="col-12 providencia-text text-center">
                <h3 class="providencia-subtitle pb-5 espanol">FORMULARIO</h3>
                <h3 class="providencia-subtitle pb-5 english">FORM</h3>
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                            @endif

                            {!! Form::open(array('url' => '/save/form','id' =>'solicitud','data-toggle'=>'validator' ,'enctype'=>'multipart/form-data','role'=>'form')) !!}

                            <div class="form-group">
                                {{Form::label('nombre', 'Nombre de quien elabora la solicitud:', array('class' => 'espanol'))}}
                                {{Form::label('nombre', 'Name:', array('class' => 'english'))}}
                                {{Form::text('nombre','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('cargo', 'Cargo:', array('class' => 'espanol'))}}
                                {{Form::label('cargo', 'Position:', array('class' => 'english'))}}
                                {{Form::text('cargo','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('correo', 'Correo electrónico:', array('class' => 'espanol'))}}
                                {{Form::label('correo', 'Email:', array('class' => 'english'))}}
                                {{Form::email('correo','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('razonSocial', 'Razón Social / Nombre Completo de la organización:', array('class' => 'espanol'))}}
                                {{Form::label('razonSocial', 'Organization:', array('class' => 'english'))}}
                                {{Form::text('razonSocial','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('figura', 'Figura Legal:', array('class' => 'espanol'))}}
                                {{Form::label('figura', 'Legal figure:', array('class' => 'english'))}}
                                {{Form::select('figura',
                                    array(
                                    'Asociación Civil' => 'Asociación Civil',
                                    'Institución de Asistencia Privada' => 'Institución de Asistencia Privada',
                                    'Institución de Beneficencia Privada'=>'Institución de Beneficencia Privada',
                                    'Institución Educativa'=>'Institución Educativa',
                                    'Sin Figura Legal'=>'Sin Figura Legal',
                                    'Sociedad Civil con Deducibilidad'=>'Sociedad Civil con Deducibilidad',
                                    ))}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('responsable', 'Responsable del proyecto que se presenta:', array('class' => 'espanol'))}}
                                {{Form::label('responsable', 'Responsible for the project presented:', array('class' => 'english'))}}
                                {{Form::text('responsable','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('web', 'Página Web:', array('class' => 'espanol'))}}
                                {{Form::label('web', 'Webpage:', array('class' => 'english'))}}
                                {{Form::text('web','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('presidente', 'Nombre Presidente/Director General:', array('class' => 'espanol'))}}
                                {{Form::label('presidente', 'Name President / CEO:', array('class' => 'english'))}}
                                {{Form::text('presidente','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('mision', '¿Cual es el objetivo Central de su organización? (misión):', array('class' => 'espanol'))}}
                                {{Form::label('mision', 'What is the central objective of your organization? (mission):', array('class' => 'english'))}}
                                {{Form::text('mision','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('organizacion', '¿Su organización forma parte de una o más asociaciones y/o está vinculada institucionalmente a otra organización?', array('class' => 'espanol'))}}
                                {{Form::label('organizacion', 'Your organization is part of one or more associations or is institutionally linked to another organization?', array('class' => 'english'))}}
                                {{Form::text('organizacion','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('descripcion', 'Descripción completa del proyecto y monto de apoyo solicitado:', array('class' => 'espanol'))}}
                                {{Form::label('descripcion', 'Complete description of the project and amount of support requested:', array('class' => 'english'))}}
                                {{Form::text('descripcion','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-file">
                                {{Form::label('hacienda', 'Alta ante la Secretaría de Hacienda', array('class' => 'espanol'))}}
                                {{Form::label('hacienda', 'Registration with the Ministry of Finance', array('class' => 'english'))}}
                                {{Form::file('hacienda' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                {{Form::label('principales', '¿Quienes son sus principales donantes?', array('class' => 'espanol'))}}
                                {{Form::label('principales', 'Who are your main donors?', array('class' => 'english'))}}
                                {{Form::text('principales','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('direccion', 'Dirección:', array('class' => 'espanol'))}}
                                {{Form::label('direccion', 'Address:', array('class' => 'english'))}}
                                {{Form::text('direccion','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                {{Form::label('telefono', 'Teléfono:', array('class' => 'espanol'))}}
                                {{Form::label('telefono', 'Phone:', array('class' => 'english'))}}
                                {{Form::text('telefono','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                {{Form::label('extension', 'Extensión:', array('class' => 'espanol'))}}
                                {{Form::label('extension', 'Extension:', array('class' => 'english'))}}
                                {{Form::text('extension','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('fecha_constitucion', 'Fecha de Constitución:', array('class' => 'espanol'))}}
                                {{Form::label('fecha_constitucion', 'Constitution date:', array('class' => 'english'))}}
                                {{Form::text('fecha_constitucion','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('rfc', ' RFC (Incluir homoclave)', array('class' => 'espanol'))}}
                                {{Form::label('rfc', ' RFC (Include homoclave)', array('class' => 'english'))}}
                                {{Form::text('rfc','' ,['required' => 'required'])}}
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                {{Form::label('autorizada', ' ¿Esta autorizada para expedir recibos deducibles de impuestos? ', array('class' => 'espanol'))}}
                                {{Form::label('autorizada', ' Is it authorized to issue tax deductible receipts? ', array('class' => 'english'))}}
                                {{Form::select('autorizada',array(
                                    'Si'=>'Si',
                                    'No'=>'No',
                                    'Tramite'=>'En trámite',
                                    )
                                    )}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('poblacion','Poblacion a la que atiende, Población en general:', array('class' => 'espanol'))}}
                                {{Form::label('poblacion','Population served, General population:', array('class' => 'english'))}}
                                {{Form::select('poblacion',array(
                                    'En beneficio de los mismos asociados'=>'En beneficio de los mismos asociados',
                                    'Ofreciendo servicios a terceros'=>'Ofreciendo servicios a terceros',
                                    'Otorgando donativos a otros'=>'Otorgando donativos a otros',
                                    'Otorgando servicios y apoyo a organizaciones o instituciones sin fines de lucro'=>'Otorgando servicios y apoyo a organizaciones o instituciones sin fines de lucro',
                                    'Otros, especifique'=>'Otros, especifique'
                                    )
                                    )}}
                            </div>

                            <div class="form-group">
                                {{Form::label('areas_interes', 'Áreas de Operación de su Organización', array('class' => 'espanol'))}}
                                {{Form::label('areas_interes', 'Areas of Operation of your Organization', array('class' => 'english'))}}
                                {{Form::select('areas_interes',array(
                                    'Educación'=>'Educación',
                                    'Salud y Nutrición'=>'Salud y Nutrición',
                                    'Comunidad'=>'Comunidad',
                                    'Desarrollo Social'=>'Desarrollo Social',
                                    'Otros,'=>'Otros'
                                    ),null,array('multiple'=>'multiple','name'=>'areas_interes[]'))}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                {{Form::label('recursos_complementarios', '¿Ha manejado esquemas de recursos complementarios?', array('class' => 'espanol'))}}
                                {{Form::label('recursos_complementarios', 'Has managed complementary resource schemes?', array('class' => 'english'))}}
                                {{Form::text('recursos_complementarios')}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-file">
                                {{Form::label('acta_constitutiva', 'Copia del Acta Constitutiva de la Organización y organigrama actual de su operación', array('class' => 'espanol'))}}
                                {{Form::label('acta_constitutiva', 'Copy of the Articles of Incorporation of the Organization and current organization chart of its operation', array('class' => 'english'))}}
                                {{Form::file('acta_constitutiva')}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group has-file">
                                {{Form::label('actas_notariales', 'Copia Actas notariales de los apoderados legales', array('class' => 'espanol'))}}
                                {{Form::label('actas_notariales', 'Copy of notarized acts of the legal representatives', array('class' => 'english'))}}
                                {{Form::file('actas_notariales')}}
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">

                            </div>
                            {!! Form::submit() !!}

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section ('js')
<script type="text/javascript" src="{{asset('js/validation.js')}}"></script>
<script>
    @section('espjs')
        $("#figura").html('<option value="Asociación Civil">Asociación Civil</option><option value="Institución de Asistencia Privada">Institución de Asistencia Privada</option><option value="Institución de Beneficencia Privada">Institución de Beneficencia Privada</option><option value="Institución Educativa">Institución Educativa</option><option value="Sin Figura Legal">Sin Figura Legal</option><option value="Sociedad Civil con Deducibilidad">Sociedad Civil con Deducibilidad</option>');
        $("#multiple").html('<option value="Educación">Educación</option><option value="Salud y Nutrición">Salud y Nutrición</option><option value="Comunidad">Comunidad</option><option value="Desarrollo Social">Desarrollo Social</option><option value="Otros,">Otros</option>');
        $("#autorizada").html('<option value="Si">Si</option><option value="No">No</option><option value="Tramite">En trámite</option>');
        $("#poblacion").html('<option value="En beneficio de los mismos asociados">En beneficio de los mismos asociados</option><option value="Ofreciendo servicios a terceros">Ofreciendo servicios a terceros</option><option value="Otorgando donativos a otros">Otorgando donativos a otros</option><option value="Otorgando servicios y apoyo a organizaciones o instituciones sin fines de lucro">Otorgando servicios y apoyo a organizaciones o instituciones sin fines de lucro</option><option value="Otros">Otros</option>');
    @endsection
    @section('engjs')
        $("#figura").html('<option value="Asociación Civil">Civil Association</option><option value="Institución de Asistencia Privada">Private Assistance Institution</option><option value="Institución de Beneficencia Privada">Private Charitable Institution</option><option value="Institución Educativa">Educational institution</option><option value="Sin Figura Legal">Without Legal Figure</option><option value="Sociedad Civil con Deducibilidad">Civil Society with Deductibility</option>');
        $("#multiple").html('<option value="Educación">Education</option><option value="Salud y Nutrición">health and nutrition</option><option value="Comunidad">Community</option><option value="Desarrollo Social">Social development</option><option value="Otros,">Others</option>');
        $("#autorizada").html('<option value="Si">Yes</option><option value="No">No</option><option value="Tramite">In process</option>');
        $("#poblacion").html('<option value="En beneficio de los mismos asociados">For the benefit of the same partners</option><option value="Ofreciendo servicios a terceros">Offering services to third parties</option><option value="Otorgando donativos a otros">Giving donations to others</option><option value="Otorgando servicios y apoyo a organizaciones o instituciones sin fines de lucro">Providing services and support to non-profit organizations or institutions</option><option value="Otros">Othes</option>');
    @endsection
</script>
@endsection