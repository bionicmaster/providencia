@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Donar" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición, donar, Donar"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>Donar | Providencia</title>
@endsection

@section('content')

@include('layout.navigation')

<!-- HEADER -->
<header class="providencia-header providencia-header--donar">
</header>

<section class="providencia-donar pt-10 pb-10">
    <div class="container">
        <!-- title -->
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="providencia-title pb-3 espanol">DONAR</h1>
                <h3 class="providencia-subtitle providencia-text espanol">¿Cómo puedo donar?</h3>
                <h1 class="providencia-title pb-3 english">DONATE</h1>
                <h3 class="providencia-subtitle providencia-text english">How can you make a donation?</h3>
            </div>
        </div>

        <!-- steps -->
        <div class="row pt-10 pb-10">
            <div class="col-12 col-md-4">
                <div class="providencia-donar_step text-center">
                    <h2 class="providencia-donar_number">1</h2>
                    <p class="providencia-donar_step-text providencia-text mx-auto espanol">Elige la opción: <br><b>"Donar a un proyecto"</b><br> y escoge la categoría <br> de tu preferencia</p>
                    <p class="providencia-donar_step-text providencia-text mx-auto english">Choose: <br><b>"Donate to a project"</b><br> and select your preferred <br> category</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="providencia-donar_step text-center">
                    <h2 class="providencia-donar_number">2</h2>
                    <p class="providencia-donar_step-text providencia-text mx-auto espanol">En la barra lateral de proyectos, define la cantidad que desees</p>
                    <p class="providencia-donar_step-text providencia-text mx-auto english">In the project sidebar, define the amount you want</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="providencia-donar_step text-center">
                    <h2 class="providencia-donar_number">3</h2>
                    <p class="providencia-donar_step-text providencia-text mx-auto espanol">Selecciona tu medio de pago preferido</p>
                    <p class="providencia-donar_step-text providencia-text mx-auto english">Select your preferred payment method</p>
                </div>
            </div>
        </div>

        <!-- payment methods -->
        <div class="row">
            <div class="col-12 providencia-text text-center">
                <h3 class="providencia-subtitle pb-3 espanol">¿Cuáles son los medios de pago?</h3>
                <h3 class="providencia-subtitle pb-3 english">What are the payment methods?</h3>
                <p class="espanol">Nuestra plataforma integra medios de pago seguros: Multipagos BBVA, Oxxo, Pay y PayPal.</p>
                <p class="espanol">Our platform integrates secure payment methods: BBVA, Oxxo, Pay and PayPal multi-payments.</p>
                <p class="english">Puntos BBVA Tarjeta de crédito o débito Transferencias electrónicas En efectivo Tiendas</p>
                <p class="english">BBVA points Credit or debit card Electronic transfers Cash Stores</p>
            </div>
        </div>
    </div>
</section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection