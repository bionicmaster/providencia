@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="¿Como Subir un Proyecto?" /> 
    <meta name="keywords" content="providencia, Providencia, como, Como, subir, proyecto, Proyecto"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}" />
    <title>¿Como Subir un Proyecto? | Providencia</title>
@endsection

@section('content')

@include('layout.navigation')

<!-- HEADER -->
<header class="providencia-header providencia-header--subir">
</header>

<section class="providencia-subir pt-10 pb-10">
    <div class="container">
        <!-- title -->
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="providencia-title pb-3 espanol">SUBIR PROYECTO</h1>
                <h1 class="providencia-title pb-3 english">UPLOAD A PROJECT</h1>
                <h3 class="providencia-subtitle providencia-text espanol">¿Cómo puedo subir un proyecto?</h3>
                <h3 class="providencia-subtitle providencia-text espanol">How can I upload a project?</h3>
            </div>
        </div>

        <!-- steps -->
        <div class="row pt-10">
            <div class="col-12 col-md-4">
                <div class="providencia-subir_step text-center">
                    <h2 class="providencia-subir_number mx-auto">1</h2>
                    <img class="providencia-subir_icon mx-auto" src="{{asset('/images/icon-1.jpg')}}" alt="icon">
                    <p class="providencia-subir_step-text providencia-text mx-auto espanol">Elige la opción: <br>"Subir un proyecto" llena y envía el formulario</p>
                    <p class="providencia-subir_step-text providencia-text mx-auto english">Choose: <br>"Upload a project "fill out and submit the form</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="providencia-subir_step text-center">
                    <h2 class="providencia-subir_number mx-auto">2</h2>
                    <img class="providencia-subir_icon mx-auto" src="{{asset('/images/icon-2.jpg')}}" alt="icon">
                    <p class="providencia-subir_step-text providencia-text mx-auto espanol">Fundación Providencia se pondrá en contacto y solicitará información</p>
                    <p class="providencia-subir_step-text providencia-text mx-auto english">Providence Foundation will contact and request information</p>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="providencia-subir_step text-center">
                    <h2 class="providencia-subir_number mx-auto">3</h2>
                    <img class="providencia-subir_icon mx-auto" src="{{asset('/images/icon-3.jpg')}}" alt="icon">
                    <p class="providencia-subir_step-text providencia-text mx-auto espanol">Espera por su respuesta sobre la resolución del proyecto</p>
                    <p class="providencia-subir_step-text providencia-text mx-auto english">Waiting for your response on the resolution of the project</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection