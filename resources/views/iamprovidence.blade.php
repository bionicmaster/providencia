@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Soy Providencia" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>Soy Providencia | Providencia</title>
@endsection

@section('content')

    @extends('layout.navigation')

    <!-- HEADER -->
    <header class="providencia-header providencia-header--soy">
    </header>

    <section class="providencia-soy pt-10 pb-10">
        <div class="container">
            <!-- title -->
            <div class="row pb-10">
                <div class="col-12 text-center">
                    <h1 class="providencia-title pb-3 espanol">¡SOY PROVIDENCIA!</h1>
                    <h1 class="providencia-title pb-3 english">I AM PROVIDENCE!</h1>
                </div>
            </div>
            
            <!-- Empresas -->
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="providencia-title pb-3 espanol">EMPRESAS</h1>
                    <h1 class="providencia-title pb-3 english">ENTERPRISE</h1>
                    <div class="row providencia-soy_logos-container">
                        @foreach ($empresas as $empresa)
                            <div class="col-4 col-md-3 pd-0">
                                <div class="providencia-soy_image">
                                    <img class="providencia-soy_logo w-100" src="https://providencia.org.mx/storage/{{$empresa->imagen}}" alt="{{$empresa->nombre}}" >
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <!-- Organizaciones -->
            <div class="row mt-10">
                <div class="col-12 text-center">
                    <h1 class="providencia-title pb-3 espanol">ORGANIZACIONES</h1>
                    <h1 class="providencia-title pb-3 english">ORGANIZATIONS</h1>
                    <!--
                    <p class="providencia-text text-center mb-5">Nuestra plataforma integra medios de pago seguros: Multipagos Bancomer, Oxxo Pay y PayPal. <br>Puntos Bancomer Tarjeta de crédito o débito Transferencias electrónicas En efectivo (Oxxo) Paypal.</p>
                    -->
                    <div class="row providencia-soy_logos-container">
                         @foreach ($organizaciones as $organizacion)
                            <div class="col-4 col-md-3 pd-0">
                                <div class="providencia-soy_image">
                                    <img class="providencia-soy_logo w-100" src="https://providencia.org.mx/storage/{{$organizacion->imagen}}" alt="{{$organizacion->nombre}}" >
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- PAYMENT -->
    @include('layout.payments')

    <!-- FOOTER -->
    @include('layout.footer')

@endsection