<!-- PARTICIPATE -->
<section class="participate">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="participate_title text-center text-blue espanol">¡SÉ PARTE DE LA PROVIDENCIA!</h2>
                <h2 class="participate_title text-center text-blue english">BE PART OF PROVIDENCE!</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4">
                <div class="text-center">
                    <img class="participate_img" src="{{asset('/images/upload.png')}}" alt="Subir proyecto">
                    <a class="participate_btn text-blue hvr-grow espanol" href="{{route('upload_form')}}">Subir un proyecto</a>
                    <a class="participate_btn text-blue hvr-grow english" href="{{route('upload_form')}}">Upload a project</a>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="text-center">
                    <img class="participate_img" src="{{asset('/images/donate.png')}}" alt="Donar a proyecto">
                    <a class="participate_btn text-blue hvr-grow espanol" href="https://www.providencia.org.mx/#areas">Donar a un proyecto</a>
                    <a class="participate_btn text-blue hvr-grow english" href="https://www.providencia.org.mx/#areas">Donate to a project</a>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="text-center">
                    <img class="participate_img" src="{{asset('/images/volunteer.png')}}" alt="Voluntariado">
                    <a class="participate_btn text-blue hvr-grow espanol" href="https://crowdfunding.providencia.org.mx/voluntarios/">Voluntariado</a>
                    <a class="participate_btn text-blue hvr-grow english" href="https://crowdfunding.providencia.org.mx/voluntarios/">Volunteering</a>
                </div>
            </div>
        </div>
    </div>
</section>