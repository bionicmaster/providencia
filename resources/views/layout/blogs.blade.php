<!-- NEWS -->
<section class="news">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center blogs_title">BLOG</h2>
            </div>
        </div>
        <div class="row">
            <!-- News 1 -->
            @foreach ($blogs as $blog)
            <div class="col-12 col-md-6 pd-0 espanol">
                <div style="overflow: hidden;">
                    <a class="d-flex justify-content-center align-items-center news_new news_new--1" style=" background-image: url('https://providencia.org.mx/storage/{{$blog->image}}')" href="{{route('blog', ['id'=>$blog->id])}}">
                        {{$blog->title}}
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-6 pd-0 english">
                <div style="overflow: hidden;">
                    <a class="d-flex justify-content-center align-items-center news_new news_new--1" style=" background-image: url('https://providencia.org.mx/storage/{{$blog->image}}')" href="{{route('blog', ['id'=>$blog->id])}}">
                        {{$blog->title_english}}
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>