<footer class="footer footer-providencia">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <h4 class="espanol">ATENCIÓN AL DONANTE</h4>
                <h4 class="english">ATTENTION TO THE DONOR</h4>
                <ul>
                    <li class="espanol"><a href="{{route('faq')}}" >Preguntas frecuentes</a></li>
                    <li class="espanol"><a href="https://crowdfunding.providencia.org.mx/incentivos-fiscales/" target="_blank" >Incentivos Fiscales</a></li>
                    <li class="espanol"><a href="{{route('legal')}}" >Políticas de Privacidad</a></li>
                    <li class="english"><a href="{{route('faq')}}" >FAQ</a></li>
                    <li class="english"><a href="https://crowdfunding.providencia.org.mx/incentivos-fiscales/" target="_blank" >Tax incentives</a></li>
                    <li class="english"><a href="{{route('legal')}}" >Privacy policies</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <ul>
                </ul>
            </div>

            <div class="col-12 col-md-3 text-right">
                <img class="footer-providencia_logo img-fluid" src="{{asset('/images/logo-providencia-2.png')}}" alt="Providencia fundación">

                <div class="col-12 pd-0">
                    <a class="social-link" onclick="window.open('https://api.whatsapp.com/send?text='+document.URL, '_blank');">
                        <img class="social-link_img" src="{{asset('/images/icon-whatsapp-blue.svg')}}" alt="whatsapp">
                    </a>
                    <a class="social-link" href="https://www.instagram.com/fprovidenciamx/?hl=es-la">
                        <img class="social-link_img" src="{{asset('/images/icon-instagram-blue.svg')}}" alt="instagram">
                    </a>
                    <a class="social-link" href="https://www.youtube.com/channel/UCzA-u5lebqzQYhz6Yo9iYKg">
                        <img class="social-link_img" src="{{asset('/images/icon-youtube-blue.svg')}}" alt="youtube">
                    </a>
                    <a class="social-link" href="https://www.facebook.com/FundacionProvidenciamx/">
                        <img class="social-link_img" src="{{asset('/images/icon-fb-blue.svg')}}" alt="facebook">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-right">
                <h6>Fundación Providencia | Paseo de la Reforma 319, Lomas de Chapultepec. - CDMX 11000</h6>
                <h6>5541616861 - <a href="mailto:contacto@providencia.org.mx">contacto@providencia.org.mx</a></h6>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
    piAId = '868511';
    piCId = '35478';
    piHostname = 'pi.pardot.com';
    (function() {
        function async_load(){
            var s = document.createElement('script'); s.type = 'text/javascript';
            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
            var c = document.getElementsByTagName('script')[0];
            c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
    })();
</script>
