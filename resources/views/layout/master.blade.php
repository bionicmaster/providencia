<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" />
    <meta name="twitter:card" content="summary" />
    
    @yield('head')
    
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('/css/main.css')}}">
    <style type="text/css">
        .loader{
            background-color: #FAFAFA;
            height: 100vh;
            width: 100vw;
            position: fixed;
            left: 0;
            top: 0;
            z-index: 9;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: hidden;
        }
        .loader img{
            width: 40%;
            max-width: 170px;
        }
    </style>
    @yield('styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107170815-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107170815-1');
    </script>

    <!-- Script de digitalcoaster-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166894621-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-166894621-1');
    </script>


</head>
<body class="">
    <div class="loader" id="loader">
        <img src="{{asset('/images/loader.gif')}}" alt="Loader">
    </div>

    @yield('content')

<!-- Script -->
<script src="{{asset('/js/all.js')}}"></script>
<script src="{{asset('/js/providencia.js')}}"></script>
@yield('js')
<script>
    $(document).ready(function() {
        setTimeout(function(){ 
            $("#loader").remove();
        }, 1000);
    });
</script>
<script>
    //Languaje
    $(function() {
        if(sessionStorage.getItem("language")==null){
            sessionStorage.setItem("language", "espanol");
            $(".english").css('display','none');
            $(".espanol").css('display','block');
            @yield('espjs')
        }else if(sessionStorage.getItem("language")=='espanol'){
            sessionStorage.setItem("language", "espanol");
            $(".english").css('display','none');
            $(".espanol").css('display','block');
            @yield('espjs')
        }else{
            sessionStorage.setItem("language", "english");
            $(".english").css('display','block');
            $(".espanol").css('display','none');
            @yield('engjs')
        }
    });
    $( "#eng" ).click(function() {
        sessionStorage.setItem("language", "english");
        $(".english").css('display','block');
        $(".espanol").css('display','none');
        @yield('engjs')
    });
    $( "#esp" ).click(function() {
        sessionStorage.setItem("language", "espanol");
        $(".english").css('display','none');
        $(".espanol").css('display','block');
        @yield('espjs')
    });
</script>
<script>
    $('body').addClass('providencia-home');
</script>

<script>
    $(function() {
        clean('donativo-mensual');
    });

    let tipo = "donativo-unico";
    const clean = (selected) => {
        $('#alerta').css('display', 'none');
        if(selected !== undefined)
            tipo = selected;
            if(selected === 'donativo-mensual') {
                $('.unico').hide();
                $('.mes').show();
                $('.color').css('backgroundColor', '#1580B4');
                $('#price').val('0.00');
            } else {
                $('.mes').hide();
                $('.unico').show();
                $('.color').css('backgroundColor', '#68C3E0');
                $('#price').val('0.00');
            }
    }

    function clean2(){
        $('#alerta').css('display', 'none');
    }
</script>
</body>
</html>
