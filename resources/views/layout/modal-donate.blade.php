<div class="modal modal-multipagos" tabindex="-1" id="exampleModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body text-center">
                <div class="modal-text-donativo">
                    <h1 class="espanol"><b>¡MUCHAS GRACIAS!</b></h1>
                    <h1 class="english"><b>THANK YOU!!</b></h1>
                    <h4 class="espanol">Selecciona tu aportación.</h4>
                    <h4 class="english">Select your contribution.</h4>
                </div>

                <div class="form">

                    <!-- donativo único/ mensual -->
                    <div class="row mt-5">
                        <div class="col-6">
                                <label class="modal-multipagos_btn modal-multipagos_btn--donativo w-100 tipo-donativo color-mes espanol" for="donativo-mensual">Donativo mensual</label>
                                <label class="modal-multipagos_btn modal-multipagos_btn--donativo w-100 tipo-donativo color-mes english" for="donativo-mensual">Monthly donation</label>
                                <input id="donativo-mensual" type="radio" name="tipo-donativo" value="donativo-mensual" onclick="clean('donativo-mensual')">
                            </div>
                        <div class="col-6">
                            <label class="modal-multipagos_btn modal-multipagos_btn--donativo w-100 tipo-donativo active color-unico espanol" for="donativo-unico">Donativo único</label>
                            <label class="modal-multipagos_btn modal-multipagos_btn--donativo w-100 tipo-donativo active color-unico english" for="donativo-unico">Unique donation</label>
                            <input id="donativo-unico" type="radio" name="tipo-donativo" value="donativo-unico" checked="checked" onclick="clean('donativo-unico')">
                        </div>
                    </div>

                    <!-- cantidad -->
                    <div class="row">
                        <div class="col-12 modal-multipagos_prices-container">
                            <label class="modal-multipagos_btn modal-multipagos_price mes color " for="total-100">$100</label>
                            <input id="total-100" type="radio" name="price" value="100" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price mes color " for="total-200">$200</label>
                            <input id="total-200" type="radio" name="price" value="200" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price unico color "  for="total-300">$300</label>
                            <input id="total-300" type="radio" name="price" value="300" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price color " for="total-500">$500</label>
                            <input id="total-500" type="radio" name="price" value="500" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price color " for="total-1000">$1000</label>
                            <input id="total-1000" type="radio" name="price" value="1000" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price unico color"  for="total-5000">$5000</label>
                            <input id="total-5000" type="radio" name="price" value="5000" onclick="clean2()">

                            <label class="modal-multipagos_btn modal-multipagos_price active color espanol" for="total-otra">Otra</label>
                            <label class="modal-multipagos_btn modal-multipagos_price active color english" for="total-otra">Other</label>
                            <input id="total-otra" type="radio" name="price" value="0.00" checked="checked" onclick="clean2()">
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-4 col-md-3">
                            <div class="modal-multipagos_btn modal-multipagos_currency w-100">MXN</div>
                        </div>
                        <div class="col-8 col-md-9">
                            <input type="number" name="cantidadTexto" id="cantidadTexto" value="0">
                            <input class="modal-multipagos_total w-100" type="number" name="total" id="price" value="0.00">
                        </div>
                    </div>
                    
                    <div class="modal-text-donativo espanol">
                        <p>Tu donativo será procesado por la plataforma <b>Fundación Providencia,</b> los fondos <b>serán entregados sin cobro de comisión</b> a la Organización sin fines de lucro seleccionada.</p>
                    </div>
                    <div class="modal-text-donativo english">
                        <p>Your donation will be processed by the <b>Providence Foundation platform</b>, the funds <b>will be delivered without commission</b> to the selected non-profit Organization.</p>
                    </div>
                    
                    <!-- donar -->
                    <div class="row align-items-center row-donar">
                        <div class="col-7 col-md-8 text-left pr-0">
                            <p class="modal-multipagos_tarjetas espanol">Pago con tarjetas de crédito y débito, tiendas de conveniencia y transferencia interbancaria</p>
                            <p class="modal-multipagos_tarjetas english">Payment with credit and debit cards, convenience stores and interbank transfer.</p>
                            <p id="alertaes" class="modal-multipagos_tarjetas espanol" style="color:red; display:none;">Inserta una cantidad</p>
                            <p id="alertaen" class="modal-multipagos_tarjetas english" style="color:red; display:none;">Insert a quantity</p>
                            <img class="modal-multipagos_logos" src="{{asset('/images/logo-tiendas-solo.png')}}" alt="tiendas">
                            <img class="modal-multipagos_logos ml-0" src="{{asset('/images/logo-mastercard.png')}}" alt="master card">
                            <img class="modal-multipagos_logos" src="{{asset('/images/logo-visa.jpg')}}" alt="visa">
                            <img class="modal-multipagos_logos" src="{{asset('/images/logo-american.png')}}" alt="american express">
                            <img class="modal-multipagos_logos" src="{{asset('/images/logo-bbva.png')}}" alt="bbva wallet">
                            <img class="modal-multipagos_logos" src="{{asset('/images/logo-transferencia.png')}}" alt="transferencia">
                        </div>
                        <div class="col-5 col-md-4">
                            <button type="button" class="modal-multipagos_btn w-100 active color-mes" onclick="dona()"><i class="espanol">Donar</i><i class="english">Donate</i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    function dona(){
        
        amount =$('#price').val();

        if(amount == 0){
            if(sessionStorage.getItem("language")==null || sessionStorage.getItem("language")=='espanol'){
                $('#alertaes').css('display', 'block');
            }else{
                $('#alertaen').css('display', 'block');
            }
        }else{
            if(sessionStorage.getItem("language")==null || sessionStorage.getItem("language")=='espanol'){
                $('#alertaes').css('display', 'none');
            }else{
                $('#alertaen').css('display', 'none');
            }
            if(tipo == 'donativo-mensual'){
                window.location.href = '{{route("donative")}}?amount='+amount+'&modality=month&project_id={{$project->id}}&type='+tipo;
            }else{
                window.location.href = '{{route("donative")}}?amount='+amount+'&modality=once&project_id={{$project->id}}&type='+tipo;
            }
            
        }
    }

</script>
