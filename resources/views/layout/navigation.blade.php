<!-- MAIN NAVIGATION -->
<div class="providencia-navigation main-navigation">
    <div class="container">
      <div class="row mx-auto">
        <nav class="navbar navbar-expand-md navbar-dark" style="height: -webkit-fill-available;">
          <!-- Brand -->
    
          <a class="navbar-brand hidden-xs" href="{{route('home')}}">
            <img class="nav-logo" src="{{asset('/images/logo-providencia-2.png')}}" alt="Providencia fundación">
          </a>
    
          <!-- Collapsibe Button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
    
          <!-- Navbar links -->
    
          <div class="collapse navbar-collapse main-menu" id="collapsibleNavbar" style="margin-left:0%;">
    
            <ul class="navbar-nav">
              <li class="nav-item visible-xs">
                <a class="nav-link espanol" href="{{route('home')}}">Inicio</a>
                <a class="nav-link english" href="{{route('home')}}">Home</a>
              </li>
    
    
              <li class="nav-item">
                <div class="dropdown">
                  <a class="nav-link donate-project espanol" data-toggle="dropdown" >Donar a un proyecto</a>
                  <a class="nav-link donate-project english" data-toggle="dropdown" >Donate to a project</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item espanol" href="{{url('/#areas')}}" >Donativo recurrente</a>
                    <a class="dropdown-item espanol" href="https://crowdfunding.providencia.org.mx" >Proyectos Crowdfounding</a>
                    <a class="dropdown-item espanol" href="https://crowdfunding.providencia.org.mx/voluntarios/" >Voluntariado</a>
                    <a class="dropdown-item english" href="{{url('/#areas')}}" >Recurring Donation</a>
                    <a class="dropdown-item english" href="https://crowdfunding.providencia.org.mx" >Crowdfounding projects</a>
                    <a class="dropdown-item english" href="https://crowdfunding.providencia.org.mx/voluntarios/" >Volunteering</a>
    
                  </div>
                </div>
    
              </li>
    
    
              <li class="nav-item">
                <a class="nav-link what-do  espanol" href="{{route('whatwedo')}}" >¿Que hacemos?</a>
                <a class="nav-link what-do  english" href="{{route('whatwedo')}}" >What do we do?</a>
              </li>
    
              <li class="nav-item">
                <div class="dropdown">
                  <a class="nav-link how-works espanol" data-toggle="dropdown" >¿Como funciona?</a>
                  <a class="nav-link how-works english" data-toggle="dropdown" >How does it work?</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item espanol" href="{{route('howtodonate')}}" >¿Cómo donar?</a>
                    <a class="dropdown-item espanol" href="{{route('howtouploadaproject')}}" >¿Cómo subir un proyecto?</a>
                    <a class="dropdown-item espanol" href="{{route('faq')}}" >Preguntas frecuentes</a>
                    <a class="dropdown-item english" href="{{route('howtodonate')}}" >How to make a donation?</a>
                    <a class="dropdown-item english" href="{{route('howtouploadaproject')}}" >How to upload a project?</a>
                    <a class="dropdown-item english" href="{{route('faq')}}" >FAQ</a>
                  </div>
                </div>
              </li>
    
              <li class="nav-item">
                <a class="nav-link start-project espanol" href="{{route('upload_form')}}" >Subir un proyecto</a>
                <a class="nav-link start-project english" href="{{route('upload_form')}}" >Upload a project</a>
              </li>
    
              <li class="nav-item">
                <a class="nav-link start-project espanol" href="{{route('blogs')}}" >Blog</a>
                <a class="nav-link start-project english" href="{{route('blogs')}}" >Blog</a>
              </li>
    
              <li class="nav-item">
                <a class="nav-link espanol" href="{{route('iamprovidence')}}" >Soy Providencia</a>
                <a class="nav-link english" href="{{route('iamprovidence')}}" >I am Providence</a>
              </li>
    
              <li class="nav-item">
                <img class="banderas-esp-engli" src="{{asset('/images/languages/es.png')}}" alt="Español" id="esp">
              </li>
    
              <li class="nav-item">
                <img class="banderas-esp-engli" src="{{asset('/images/languages/en.png')}}" alt="English" id="eng">
              </li>
    
              <li>
                <a class=" btn-nav" href="{{url('/video')}}">VIDEOS</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
  
    </div>
  </div>