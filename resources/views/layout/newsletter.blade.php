<!-- NEWSLETTER -->
<section class="register">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pd-0">
                <div class="register_row bg-blue pt-5 pb-5">
                    <h2 class="text-white register_title text-center mb-0 espanol">REGÍSTRATE</h2>
                    <h2 class="text-white register_title text-center mb-0 english">SIGN UP</h2>
                    <h3 class="text-white register_subtitle text-center mb-0 espanol"><i>PARA RECIBIR NUESTRO NEWSLETTER</i></h3>
                    <h3 class="text-white register_subtitle text-center mb-0 english"><i>TO RECEIVE OUR NEWSLETTER</i></h3>
                </div>
            </div>
        </div>

        <div class="container">
            <form action="{{route('newsletter')}}" method="POST">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-12 col-md-6">
                        <input id="name" name="name" type="text" class="register_input register_input--name w-90 mx-auto" required>
                        <label class="w-100 text-center register_label text-blue espanol" for="name">Nombre</label>
                        <label class="w-100 text-center register_label text-blue english" for="name">Name</label>
                    </div>
                    <div class="col-12 col-md-6">
                        <input id="email" type="email" name="email" class="register_input register_input--mail w-90 mx-auto" required>
                        <label class="w-100 text-center register_label text-blue" for="email">E-mail</label>
                    </div>
                </div>

                <input type="submit" class="register_submit mx-auto hvr-grow contacto-submit espanol" value="Regístrate" placeholder="Regístrate">
                <input type="submit" class="register_submit mx-auto hvr-grow contacto-submit english" value="Sign up" placeholder="sign up">

            </form>
        </div>
    </div>
</section>