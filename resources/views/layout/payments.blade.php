<section class="payment-providencia">
    <div class="container">
        <hr class="payment_line">

        <div class="row">
            <div class="col-12 col-md-6">
                <p class="payment_title espanol">MEDIOS DE PAGO</p>
                <p class="payment_title english">PAYMENT METHODS</p>
                <div class="row d-flex align-items-center">
                    <div class="col-3">
                        <img class="w-100 payment_img" src="{{asset('/images/logo_openpay.png')}}" alt="Pagos">
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <p class="payment_title text-center espanol">Dona vía Whatsapp</p>
                <p class="payment_title text-center english">Donate through  Whatsapp</p>
                <a class="payment_whats text-center hvr-grow" onclick="window.open('https://api.whatsapp.com/send?text='+document.URL, '_blank');">
                    <img class="payment_whats-btn" src="{{asset('/images/icono-whatsapp.png')}}" alt="Ícono Whatsapp">
                </a>
            </div>
        </div>
    </div>
</section>