<!-- SHARE -->
<section class="providencia-share">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="providencia-share_title espanol">COMPARTIR</h2>
                <h2 class="providencia-share_title english">SHARE</h2>
                <a onclick="window.open('https://api.whatsapp.com/send?text='+document.URL, '_blank');">
                    <img class="providencia-share_link"
                            src="{{asset('/images/icon-whatsapp-blue.svg')}}" alt="whatsapp">
                </a>
                <a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+document.URL, '_blank');">
                    <img class="providencia-share_link"
                            src="{{asset('/images/icon-fb-blue.svg')}}" alt="facebook">
                </a>
            </div>
        </div>
    </div>
</section>