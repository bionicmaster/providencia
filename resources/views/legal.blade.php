@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Providencia" />
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" />
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}" />
    <title>Providencia</title>

@endsection


@section('content')

@include('layout.navigation')

<!-- HEADER -->
<section class="header">
    <div class="header-fade">
        <div class="header-slider header-slider_1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text2.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow btn-covid" href="/covid19">DONAR COVID-19</a>
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-slider header-slider_2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text1.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-slider header-slider_3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 pd-0 text-right" style="margin-top: 200px;">
                        <img class="header-slider_text" src="{{asset('images/text1.png')}}" alt="Unidos somos más">
                        <a class="header-slider_btn text-white hvr-grow" href="https://crowdfunding.providencia.org.mx">CROWDFUNDING ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Legales -->
<section class="projects margin-boders" id="projects">
    <div class="container-fluid">
        <br><br><br>
        <div class="row espanol">

            <h1 style="text-align: center;">AVISO DE <strong>PRIVACIDAD</strong> Y<strong>PROTECCIÓN DE DATOS PERSONALES</strong></h1><br>
            <p><strong>FUNDACION PROVIDENCIA A.C. (en lo sucesivo Fundación Providencia)</strong><strong>&nbsp;</strong>por este medio, le informa a usted que derivado de la relación que existe y/o llegare a existir con usted, como usuario y/o como contraparte; Fundación Providencia ha recolectado información y datos personales que a usted le pertenecen y lo seguirá haciendo para los fines que sean necesarios derivados de la(s) mencionada(s) relación(ones).</p>
            <p>Al respecto, Fundación Providencia le informa adicionalmente lo siguiente:</p>
            <ol>
                <li>Que: 1) Sus datos personales han sido y serán recolectados y almacenados por Fundación Providencia con domicilio en Paseo de la Reforma 319, Colonia Lomas de Chapultepec, C.P. 11000, Delegación Miguel Hidalgo, Ciudad de México; y 2) La propia Fundación Providencia actuará como responsable de la base de datos en la que se encontraren los mismos.</li>
                <li>Que Fundación Providencia utilizará su información y datos personales que hoy en día obran en las bases de datos de la Fundación Providencia y/o que en el futuro obraren en las propias bases de datos de Fundación Providencia, incluyendo de manera enunciativa más no limitativa, su nombre, puesto, correo electrónico, domicilio y teléfono, únicamente para efecto de: A) Poder llevar a cabo los procesos de envío de información (a través de correo electrónico y/o vía telefónica y/o por cualquier otro medio) que estuvieren relacionados con cualquier operación, acto, actividad y proceso en la(el) que usted intervenga y/o llegare a intervenir con la participación de Fundación Providencia; y B) Poder otorgar y/o formalizar todos(as) y cada uno(a) de los documentos, de los instrumentos y/o de las escrituras que estuvieren relacionados(as) con cualquier operación, acto, actividad y proceso en la(el) que usted intervenga y/o llegare a intervenir con la participación de Fundación Providencia.</li>
                <li>Que Fundación Providencia se compromete ante usted a cumplir con todo lo dispuesto por la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (en lo sucesivo denominada como la “LFPDPPP”) así como a observar en todo momento, los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad contenidos en la LFPDPPP, teniendo en cuenta a su vez, el respeto y cuidado de los Derechos ARCO (Acceder, Rectificar, Cancelar, Oponer de los cuales usted es acreedor.</li>
                <li>Que Fundación Providencia, para efecto de cualquier aclaración, duda o solicitud relacionada con el presente aviso de privacidad y/o con el manejo de sus datos personales, le agradecerá se sirva llamar al número telefónico 5541616861 o escribir al correo electrónico: contacto@providencia.org.mx</li>
                <li>Que: 1) Fundación Providencia informa a usted que de conformidad con lo dispuesto en la LFPDPPP, usted, en todo momento, tendrá el derecho: 1.1) De ejercitar sus derechos de acceso, rectificación, cancelación y oposición a sus datos personales almacenados y utilizados por la propia Fundación Providencia, así como 1.2) Para solicitar la limitación de uso o divulgación de sus datos personales; y 2) Fundación Providencia cumplirá en todo momento con cualquier aviso y/o solicitud que, en su caso, en el ejercicio de sus derechos contenidos en la LFPDPPP, usted mismo le haga llegar a Fundación Providencia mediante un correo electrónico y/o mediante un escrito que fuere suscrito y firmado por usted.</li>
                <li>Que: 1) La información y los datos personales que usted ha proporcionado y/o que en el futuro llegare a proporcionar a Fundación Providencia se almacenarán en la base de datos de la propia Fundación Providencia que, inclusive, podría localizarse fuera de los Estados Unidos Mexicanos (según fuere libremente determinado por Fundación Providencia) y 2) Fundación Providencia manifiesta a usted que los datos personales que ha recolectado de que a usted le pertenecen o que pudiese recolectar en el futuro, podrán ser transferidos a cualquier tercero con quien Fundación Providencia hubiese contratado y/o contratare servicios para el manejo de la base de datos de Fundación Providencia para los fines indicados en el presente aviso.</li>
                <li>Que Fundación Providencia tomará medidas de seguridad organizacionales y técnicas razonables para prevenir la pérdida, uso indebido, alteración o divulgación ilegal de la información y datos personales de usted.</li>
                <li>Que en caso de que por cualquier motivo Fundación Providencia llegase a realizar cualquier cambio o modificación al presente aviso de privacidad, dicho cambio le será notificado a usted por este mismo medio o por cualquier otro medio que, en su momento, Fundación Providencia considere adecuado</li>
            </ol>

            <p>&nbsp;</p>

            <h1 style="text-align: center;">TÉRMINOS Y CONDICIONES <strong>(PAGO/OPENPAY)</strong>



                <style type="text/css">
                    .tandc>p,.tandc>ul,.tandc>ul>li,.tandc>ol{
                        font-size: 15px;
                        margin: 0 0 0px;
                        line-height: 1.5;
                    }
                </style>
                <div class="tandc align-left">

                    <p class="p1">&nbsp;</p>
                    <p class="p2"><span class="s1"><strong>I. Condiciones Generales</strong></span></p>
                    <p class="p3"><span class="s2"><strong>1. Objeto.</strong></span><span class="s1">&nbsp;Los presentes términos y condiciones (en adelante las &ldquo;</span><span class="s2">Condiciones</span><span class="s1">&rdquo;) tienen por objeto regular el acceso y uso que hagan aquellas personas físicas o morales identificados como usuarios (en adelante indistintamente el &ldquo;</span><span class="s2">Usuario</span><span class="s1">&rdquo; o los &ldquo;</span><span class="s2">Usuario</span><span class="s1">s&rdquo;) del sitio web identificado como www.providencia.org.mx&nbsp;(el &ldquo;Sitio Web&rdquo;) propiedad de la asociación civil denominada Fundación Providencia A.C. (en adelante &ldquo;Fundación Providencia&rdquo;), por lo que cualquier usuario que desee acceder y usar el sitio web podrá hacerlo sujetándose en todo momento a las presentes condiciones, según éstas puedan ser modificadas de tiempo en tiempo, así como a todas las demás políticas y principios que rigen a Fundación Providencia, que se encuentren vigentes o que en su momento sean implementados por ésta.</span></p>
                    <p class="p3"><span class="s1">Asimismo, se informa a los Usuarios que las principales actividades de Fundación Providencia son la de poner a disposición del público en general, a través del Sitio Web, una plataforma en línea de fondeo colectivo conocido también como crowdfunding con el fin de promocionar y apoyar causas de beneficencia pública e iniciativas sociales (en adelante indistintamente las &ldquo;Campañas&rdquo; o los &ldquo;Proyectos&rdquo;), que mediante el Sitio Web se publicitan, sirviendo como enlace, intermediario y concentrador depositario, según sea el caso, respecto de las Campañas y la recaudación que se reciba.</span></p>
                    <p class="p3"><span class="s1">Por lo anterior, los Usuarios reconocen y aceptan, desde este momento, que las presentes Condiciones tienen un carácter obligatorio y vinculante, y&nbsp;</span><span class="s2">no están sujetas a negociación o modificación de ninguna índole</span><span class="s1">, por lo que se entienden aceptadas en su totalidad por el simple hecho de ingresar al Sitio Web y hacer uso del mismo. Por lo tanto, es responsabilidad del Usuario el deber leer, entender y aceptar todos y cada uno de los términos y condiciones establecidos en las presentes Condiciones, en el Aviso de Privacidad, así como en los demás documentos incorporados por referencia a los mismos, previo a su registro como Usuario del Sitio Web.</span></p>
                    <p class="p2"><span class="s1"><strong>II. Definiciones.</strong></span></p>
                    <p class="p3"><span class="s1">Para efectos de claridad de las presentes Condiciones, se establecen las siguientes definiciones:</span></p>
                    <ul class="ul1">
                        <li class="li4"><span class="s4">Donativo:</span><span class="s5">&nbsp;significa la cantidad de dinero en Pesos mexicanos aportada por cada Donante a favor de una o varias Campañas que sean publicadas en el Sitio Web por los Promotores a través de www.providencia.org.mx. Los donativos superiores a $16,000 MXN deberá ser confirmado por el Donante a través de los medios establecidos por Fundación Providencia AC. para tal fin. Ningún donativo es transferible entre Campañas ni entre proyectos permanentes.</span></li>
                        <li class="li4"><span class="s4">Campañas:</span><span class="s5">&nbsp;conjunto de información compartida a través de Fundación Providencia en el Sitio Web para recaudar aportaciones del público en general para atender una causa de beneficencia pública o iniciativa social que pretenda generar, a juicio de Fundación Providencia, un impacto positivo en la sociedad, aprobado por Fundación Providencia para que sea publicado en la plataforma a través de su Sitio Web.</span></li>
                        <li class="li4"><span class="s4">Comisiones de Métodos de Pago:</span><span class="s5">&nbsp;es el cobro que realizan los métodos de pago por cada aportación y/o suscripción procesada en el Sitio Web, esta comisión será descontada de la aportación hecha por el Donante.</span></li>
                        <li class="li4"><span class="s4">Condiciones:</span><span class="s5">&nbsp;Se refiere a los presentes Términos y Condiciones.</span></li>
                        <li class="li4"><span class="s4">Contracargo</span><span class="s5">: Es una reclamación que hace un donador directamente a través de su banco al no reconocer el cargo hecho a su cuenta por una donativo a una Campaña y/o Proyecto.</span></li>
                        <li class="li4"><span class="s4">Registro de Usuario:</span><span class="s5">&nbsp;significa la cuenta que reúne la información personal de un Usuario proporcionada en el Sitio Web, a través del registro de un nombre, un correo electrónico </span></li>
                        <li class="li4"><span class="s4">Donante:</span><span class="s5">&nbsp;Es el usuario que realiza una donativo de manera voluntaria de determinadas cantidades de dinero en pesos mexicanos para donar a la Campaña y/o al Proyecto.</span></li>
                        <li class="li4"><span class="s4">Meta de Recaudación:</span><span class="s5">&nbsp;Significa la cantidad de dinero en Pesos mexicanos presupuestada y establecida por cada OSC para realizar lo expuesto dentro del perfil de la Campaña, la cual no podrá ser modificada una vez haya sido publicada la Campaña.</span></li>
                        <li class="li4"><span class="s4">Medios de Pago:</span><span class="s5">&nbsp;Significa los medios que se establecen en el apartado IV.2 de las presente Condiciones a través de las cuales los Donantes podrán realizar sus Aportaciones voluntarias a favor de una o varias Campañas de cualquiera de las OSC&rsquo;s que se encuentren publicados de tiempo en tiempo en el Sitio Web.</span></li>
                        <li class="li4"><span class="s4">Pesos:</span><span class="s5">&nbsp;Significa la moneda de curso legal en los Estados Unidos Mexicanos.</span></li>
                        <li class="li4"><span class="s4">Plazo de recaudación:</span><span class="s5">&nbsp;Significa el tiempo máximo que el Proyecto solicita y estime necesario para lograr la meta de recaudación, y el tiempo máximo que la Campaña se encontrará vigente para recibir donativos en el Sitio Web. Una vez publicada la Campaña, este plazo de recaudación no es modificable.</span></li>
                        <li class="li4"><span class="s4">Registro de Usuario:</span><span class="s5">&nbsp; El registro mediante el llenado de un formulario con información personal y bancaria que efectúan los Usuarios con el fin de poder participar, ya sea como Proyectos o Donadores en el Sitio Web de Fundación Providencia.</span></li>
                        <li class="li4"><span class="s4">Servicios:</span><span class="s5">&nbsp;Significa los servicios que se describen en el apartado III.1 siguiente de las presente Condiciones.</span></li>
                        <li class="li4"><span class="s4">Sitio Web:</span><span class="s5">&nbsp;Significa el sitio de internet </span><span class="s6">www.providencia.org.mx</span></li>
                        <li class="li4"><span class="s4">Total Recaudado:</span><span class="s5">&nbsp;Es el monto total recaudado por en el perfil de Campaña </span></li>
                        <li class="li4"><span class="s4">Usuario:</span><span class="s5">&nbsp;Significa toda persona física o moral que se registre en el Sitio Web de Fundación Providencia.</span></li>
                        <li class="li4"><span class="s4">Usuario Verificado:</span><span class="s5">&nbsp;Es la posibilidad que tiene un Usuario registrado de validar su cuenta como una cuenta verificada por Fundación Providencia como confiable. Para esto, Fundación Providencia establecerá una validación de información detallada consistente de manera enunciativa más no limitativa en: (i) nombre completo; (ii) dirección; (iii) teléfono; (iv) dirección de correo electrónico; (v) datos de cuentas bancarias para efectos de la transferencia de las Aportaciones, según sea aplicable.</span></li>
                    </ul>
                    <p class="p2"><span class="s1"><strong>III. Servicios.</strong></span></p>
                    <p class="p3"><span class="s2"><strong>1. Los Servicios.</strong></span><span class="s1">&nbsp;Para efectos de claridad de las presentes Condiciones se entenderá como los servicios: (i) la puesta a disposición a los Usuarios por parte de Fundación Providencia de la plataforma tecnológica contenida en el Sitio Web para la publicación de las Campañas; (ii) la prestación de servicios de promoción, publicidad, mercadotecnia y&nbsp;<em>marketing</em>&nbsp;realizada por Fundación Providencia a través del Sitio Web, medios digitales, redes sociales o cualquier otro medio que determine Fundación Providencia de tiempo en tiempo respecto de cada Campaña para lograr la Meta de Recaudación en el Plazo de Recaudación o como apoyo a la difusión, lo anterior bajo la reserva de los intereses de la comunicación de Fundación Providencia sin que esto implique un compromiso contractual entre el promotor y Fundación Providencia; y (iii) actuar como depositario concentrador de las Aportaciones de los Donadores respecto de las Campañas y/o Proyectos (en adelante a los mencionados servicios se les denominara como los &ldquo;</span><span class="s2">Servicios</span><span class="s1">&rdquo;).</span></p>
                    <p class="p3"><span class="s1">Fundación Providencia ofrece asimismo apoyar y asesorar a cada una de las Campañas y/o Proyectos en áreas como realización de contenido, promoción y difusión para el logro de la meta establecida a recaudar.</span></p>
                    <p class="p3"><span class="s1">Asimismo, las OSC&rsquo;s reconocen y aceptan que:</span></p>
                    <ul class="ul1">
                        <li class="li4"><span class="s5">Para las Campañas: una vez terminado el Plazo de Recaudación, son ellos los que reciben de Fundación Providencia las donativos recibidos.</span></li>
                    </ul>
                    <p class="p3"><span class="s1">Las OSC&rsquo;s y Donadores reconocen y aceptan que, durante el Plazo de Recaudación para las Campañas, Fundación Providencia recibe los donativos con carácter de depositario concentrador conforme a la legislación vigente en la Ciudad de México, proveyendo únicamente de manera temporal una cuenta bancaria concentradora para tales efectos; para posteriormente transferir los donativos recaudados mediante transferencia bancaria a la OSC.</span></p>
                    <p class="p3"><span class="s2"><strong>2. Registro de Proyectos.</strong></span><span class="s1">&nbsp;Cada OSC se sujetará al procedimiento que se describe a continuación para la inclusión en el Sitio Web de las Campañas y/o Proyectos Permanentes,&nbsp;</span><span class="s2">en el entendido que</span><span class="s1">&nbsp;dichas Campañas y/o Proyectos podrán referirse a diversas causas de beneficencia pública e iniciativas sociales que se encuadren dentro de las categorías anunciadas en el Sitio Web.</span></p>
                    <p class="p3"><span class="s1">Por lo tanto, para poder hacer uso del Sitio Web, aquellas OSC&rsquo;s que pretendan enviar su proyecto para estudio, y aprobación por Fundación Providencia, deberán seguir proceso establecido, el cual está compuesto de tres pasos, los cuales son los siguientes:</span></p>
                    <p class="p3"><span class="s1"><strong>Paso uno</strong>. Llenar y enviar el formulario. En giveorg.org/osc/ Este formulario tendrá que ser llenado con información acerca de la OSC y el Proyecto que desea ser publicado en la plataforma de Fundación Providencia AC., la cual deberá ser veraz y precisa.</span></p>
                    <p class="p3"><span class="s1">Por ningún motivo se entenderá que llenando el formulario y enviándolo se tendrá por aceptada la Campaña y/o Proyecto. </span></p>
                    <p class="p3"><span class="s1"><strong>Paso dos</strong>. Contacto y solicitud de información. En esta etapa alguno de los directores de Fundación Providencia se podrá en contacto vía telefónica con la OSC que envío el formulario para solicitar y validar información.</span></p>
                    <p class="p3"><span class="s1"><strong>Paso tres</strong>. Resolución de proyectos. El proyecto será turnado al Comité de aprobación de Fundación Providencia y posteriormente se le informará a la OSC vía correo electrónico si fue autorizado o denegado el Proyecto, para su publicación.</span></p>
                    <ol class="ol1">
                        <li class="li4"><span class="s5">Una vez que fue autorizada la Campaña y se informó a la OSC, se le pedirá la documentación necesaria para su publicación, la cual consiste en lo siguiente:</span></li>
                    </ol>
                    <ol class="ol2">
                        <li class="li4"><span class="s5">Acta Constitutiva.</span></li>
                        <li class="li4"><span class="s5">Representante legal (poder e identificación).</span></li>
                        <li class="li4"><span class="s5">Comprobante de domicilio.</span></li>
                        <li class="li4"><span class="s5">CIF (Célula de Identificación Fiscal).</span></li>
                        <li class="li4"><span class="s5">Donataria Autorizada vigente (oficio de última publicación).</span></li>
                        <li class="li4"><span class="s5">Opinión de cumplimiento favorable del SAT.</span></li>
                        <li class="li4"><span class="s5">Acuse de recibo último de Reporte de Transparencia ante el SAT.</span></li>
                        <li class="li4"><span class="s5">CLUNI Clave Única de Registro (sólo si están registrados).</span></li>
                        <li class="li4"><span class="s5">Cuenta bancaria carátula del estado de cuenta).</span></li>
                        <li class="li4"><span class="s5">CIF Representante Legal.</span></li>
                    </ol>
                    <p class="p5">&nbsp;</p>
                    <ol class="ol1">
                        <li class="li4"><span class="s5">En el caso que sea aprobada la Campaña, adicionalmente, la OSC deberá entregar a Fundación Providencia a través del Sitio Web un video y al menos 6 (seis) imágenes de buena calidad relacionados a la información registrada de la Campaña, previo a la publicación de ésta, que se utilizarán para presentar la Campaña en la plataforma del Sitio Web.</span></li>
                    </ol>
                    <p class="p5">&nbsp;</p>
                    <ol class="ol1">
                        <li class="li4"><span class="s5">Una vez que Fundación Providencia reciba las fotos y los videos a que hace referencia el inciso anterior los publicará y promocionará a reserva de Fundación Providencia, a través de los medios que considere oportunos durante el Plazo de Recaudación, a efecto de que los Donadores empiecen a realizar las Aportaciones correspondientes, en el entendido que Fundación Providencia actuará en todo momento como intermediario concentrador y depositario de dichas Aportaciones. </span></li>
                    </ol>
                    <p class="p3"><span class="s1">Asimismo, y para efectos de claridad de las presentes Condiciones se establece que únicamente podrán ser Usuarios las personas que tengan capacidad de ejercicio y legal para contratar, por lo que, no podrán utilizar el Sitio Web las personas incapaces de contratar y los menores de edad.</span></p>
                    <p class="p3"><span class="s1">En el supuesto que una persona registre como Usuario a una persona moral, deberá contar con la capacidad legal y los poderes suficientes para contratar y obligar a dicha persona moral en términos de lo dispuesto por el segundo párrafo del artículo 2554 del Código Civil para el Distrito Federal, ahora Ciudad de México y sus correlativos en las diversas entidades federativas de la República Mexicana y el Código Civil Federal, en lo que resulte aplicable.</span></p>
                    <p class="p3"><span class="s2"><strong>3. Mecánica de Recaudación de Donativos.</strong></span><span class="s1">&nbsp; </span></p>
                    <ol class="ol1">
                        <ol class="ol1">
                            <li class="li4"><span class="s5">Una vez agotado el Plazo de Recaudación, Fundación Providencia realizará la transferencia correspondiente al Total Recaudado que resulte de las Aportaciones a la cuenta bancaria otorgada previamente por la OSC.</span></li>
                            <li class="li4"><span class="s5">Fundación Providencia entregará las Aportaciones obtenidas para su campaña dentro de los 15 (quince) días naturales posteriores al Fin del Plazo de Recaudación. Al entregar las Aportaciones Fundación Providencia queda sin responsabilidad alguna frente a las OSC, por lo que desde este momento liberan a Fundación Providencia de cualquier responsabilidad y reclamación conforme a los puntos mencionados anteriormente. </span></li>
                            <li class="li4"><span class="s5">Lo mismo aplicará para los Proyectos Permanentes.</span></li>
                        </ol>
                    </ol>
                    <p class="p3"><span class="s1">Fundación Providencia por ningún motivo y bajo ninguna circunstancia aceptará Campañas y/o Proyectos que:</span></p>
                    <ul class="ul1">
                        <li class="li4"><span class="s5">Impliquen la realización, ejecución o materialización de cualquier actividad ilícita o que vaya en contra de la moral y de las buenas costumbres.</span></li>
                        <li class="li4"><span class="s5">Promuevan &ldquo;Juegos&rdquo;, &ldquo;Rifas&rdquo; y &ldquo;Sorteos&rdquo; que no cuenten con el respectivo permiso de la Secretaría de Gobernación, por ende Donadora no se hace responsable de las implicaciones legales ni económicas del uso de dichas palabras en el perfil de la Campaña y/o Proyecto.</span></li>
                        <li class="li4"><span class="s5">El uso de los fondos esté destinado para cubrir honorarios de profesionales legales, costos de un proyecto ya desarrollado, créditos, intereses bancarios, deudas con terceros, rentas de bienes inmuebles, que tenga fines comerciales y o de ventas que no generen un impacto directo comprobable en una comunidad de personas.</span></li>
                    </ul>
                    <p class="p2"><span class="s1"><strong>IV. Comisiones y Medios de Pago.</strong></span></p>
                    <p class="p3"><span class="s2"><strong>1. Comisión.</strong></span><span class="s1">&nbsp;Fundación Providencia no cobrará comisión alguna por los Servicios previamente mencionados, por lo que las Aportaciones serán entregadas por Fundación Providencia de forma completa sin extraer porcentaje alguno del Total Recaudado.</span></p>
                    <p class="p3"><span class="s2"><strong>2. Medios de Pago de los donativos</strong></span><span class="s1">&nbsp;Los medios a través de los cuales los Donantes podrán realizar sus Aportaciones son los siguientes:</span></p>
                    <ol class="ol3">
                        <li class="li4"><span class="s5">Mediante los medios de pago que operan OPENPAY y/o Multipagos BBVA Bancomer se procesan los donativos con tarjetas bancarias de crédito o débito realizando el descuento directo del donativo de la tarjeta crédito o débito que corresponda, a través del sitio web <a href="http://www.providencia.org.mx"><span class="s7">www.providencia.org.mx</span></a> </span></li>
                        <li class="li4"><span class="s5">Mediante transferencia electrónica, entendido como SPEI;</span></li>
                        <li class="li4"><span class="s5">Mediante el sistema de pagos PayPal.</span></li>
                        <li class="li4"><span class="s5">Mediante depósito en efectivo en bancos, farmacias, tiendas de conveniencia , tiendas departamentales y supermercados&rdquo; que en su momento determine Fundación Providencia<span class="Apple-converted-space">&nbsp; </span>al publicar la Campaña y/o Proyecto en el Sitio Web.</span></li>
                    </ol>
                    <p class="p3"><span class="s2"><strong>3. Comisiones de Métodos de Pago.</strong></span><span class="s1">&nbsp;Los Donantes podrán realizar las Aportaciones que estimen pertinentes a cada Campaña y/o Proyecto a través de los Métodos de Pago, en el entendido que reconocen y aceptan que los proveedores de los Métodos de Pago cobrarán las comisiones correspondientes que mantengan vigentes de tiempo en tiempo y que, a manera de ejemplo, se muestran en el documento que se adjunta a las presentes Condiciones como&nbsp;<strong>Anexo Único denominado &ldquo;Comisiones de Medios de Pago.</strong></span></p>
                    <p class="p3"><span class="s1">Los Usuarios reconocen y aceptan que los porcentajes de comisiones relacionadas a las transferencias realizadas por los Donantes respecto de las aportaciones mencionadas anteriormente en estas Condiciones, no son definitivas ni Fundación Providencia tiene injerencia en las mismas, si no que se incluyen con el fin de ejemplificar los cálculos respectivos a fin de que tanto los Promotores como los Donadores las consideren al momento de realizar las Aportaciones y hacer disposición del monto Total Recaudado, según corresponda, por lo que Donadora no asume responsabilidad alguna por su variación, modificación o reforma de las Comisiones de Métodos de Pago.</span></p>
                    <p class="p2"><span class="s1"><strong>V. Modificaciones a las Condiciones.</strong></span></p>
                    <p class="p3"><span class="s1">Las presentes Condiciones no están sujetas a negociación o modificación de ninguna especie, por lo que los Usuarios entienden y aceptan en su totalidad las mismas. Asimismo, Donadora se reserva el derecho a modificar las presentes Condiciones en cualquier momento y a su entera discreción, situación que dará a conocer a través del Sitio Web a los Usuarios. En el supuesto que algún Usuario no esté de acuerdo con las Condiciones, según las mismas sean modificadas de tiempo en tiempo, deberá notificarlo expresamente a Fundación Providencia y se dará de baja de los sistemas de esta última y se cancelará el registro respectivo.</span></p>
                    <p class="p2"><span class="s1"><strong>VI. Datos Personales</strong></span></p>
                    <p class="p3"><span class="s1">El manejo, resguardo y utilización de la información relacionada con los datos personales que Fundación Providencia recabe a través del Sitio Web de cada uno de los Usuarios mediante el llenado del formulario será utilizada para los fines señalados en el aviso de privacidad de Fundación Providencia, el cual se encuentra en el Sitio Web, mismo que podrá ser actualizado, modificado o enteramente reformado de tiempo en tiempo de acuerdo con las políticas aplicables y las que en su momento determine Fundación Providencia, según sea oportunamente informado a los Usuarios vía correo electrónico, no obstante lo anterior se recomienda al Usuario revisar periódicamente las disposiciones de privacidad en caso de tener alguna objeción.</span></p>
                    <p class="p2"><span class="s1"><strong>VII. Derechos y Obligaciones de las Partes.</strong></span></p>
                    <p class="p3"><span class="s1">En relación con los Servicios los Promotores, Donadores y Fundación Providencia tendrán los siguientes derechos y obligaciones:</span></p>
                    <ol class="ol2">
                        <li class="li4"><span class="s5">Obligaciones de la <strong>OSC</strong>.</span></li>
                        <ol class="ol3">
                            <li class="li4"><span class="s5">Proporcionar toda la información solicitada por Fundación Providencia, tanto de la Campaña y/o Proyecto, como de sus datos o de su representada.</span></li>
                            <li class="li4"><span class="s5">Completar el formulario y, en su caso, enviar la información y documentación requerida, de acuerdo a lo establecido en las presentes Condiciones.</span></li>
                            <li class="li4"><span class="s5">Una vez aceptada la Campaña y/o Proyecto por parte de Fundación Providencia, crear una estrategia de comunicación para su difusión, con la finalidad de alcanzar la Meta de Recaudación en las Campañas y obtener el mayor número de Aportaciones y/o Suscripciones mensuales a los Proyectos Permanentes mediante la recaudación de las Aportaciones de los Donadores por parte de la OSC con asesoría de Fundación Providencia.</span></li>
                            <li class="li4"><span class="s5">Utilizar las cantidades netas derivadas de los donativos exclusivamente a la realización de lo estipulado en el perfil la Campaña y/o del Proyecto de conformidad con lo establecido en las presentes Condiciones. La OSC será el único responsable de la correcta utilización de las Aportaciones para la realización de lo establecido en el perfil de la Campaña, sin que pudiera imputársele a Fundación Providencia algún tipo de responsabilidad por cualquier incumplimiento de la OSC en tal sentido.</span></li>
                            <li class="li4"><span class="s5">Cumplir con todas y cada una de las obligaciones establecidas en las presentes Condiciones, y en los Contratos.</span></li>
                        </ol>
                    </ol>
                    <p class="p6">&nbsp;</p>
                    <ol class="ol2">
                        <li class="li4"><span class="s5">Obligaciones del&nbsp;</span><span class="s4"><strong>Donante</strong></span><span class="s5">.</span></li>
                        <ol class="ol3">
                            <li class="li4"><span class="s5">Proporcionar toda la información solicitada por Fundación Providencia, la cual incluirá sus datos reales, así como un correo electrónico de utilización frecuente. El Donador acepta, además, que los datos aportados al momento de realizar los donativos correspondientes, serán suficientes e idóneos para identificarlo plenamente y contactarlo.</span></li>
                            <li class="li4"><span class="s5">Entregar efectivamente las Aportaciones voluntarias a cada Campaña que sea de su interés. </span></li>
                            <li class="li4"><span class="s5">El Donante acepta y reconoce que no tendrá derecho alguno respecto de la Campaña y/o del Proyecto a los cuales realice Aportaciones de tiempo en tiempo.</span></li>
                        </ol>
                    </ol>
                    <p class="p5">&nbsp;</p>
                    <ol class="ol2">
                        <li class="li4"><span class="s5">Obligaciones de&nbsp;</span><span class="s4"><strong>Fundación Providencia</strong></span><span class="s5">.</span></li>
                        <ol class="ol3">
                            <li class="li4"><span class="s5">Aprobar, rechazar o cancelar las Campañas y/o del Proyecto, reservándose el derecho, además, de rechazar cualquier solicitud de registro como Usuario o de cancelar o suspender un registro de Usuario previamente otorgada, sin que esté obligado a comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a indemnización o resarcimiento a favor de cualquier Usuario.</span></li>
                            <li class="li4"><span class="s5">Publicar, previo acuerdo por escrito con la OSC, en el Sitio Web las Campañas y/o Proyectos, así como los cambios en los mismos, teniendo Fundación Providencia, previo acuerdo por escrito con el Promotor, el derecho de modificar la información que en dichos perfiles se contenga en casos excepcionales dentro del Sitio Web.</span></li>
                            <li class="li4"><span class="s5">Recibir, en su carácter de depositario concentrador, por parte de los Donadores, los montos de las Aportaciones a través de los Métodos de Pago, los cuales bajo ningún concepto serán utilizados para fines distintos a los establecidos en las Campañas y/o Proyectos.</span></li>
                        </ol>
                    </ol>
                    <p class="p2"><span class="s1"><strong>VIII. Obligaciones fiscales.</strong></span></p>
                    <p class="p3"><span class="s1">Cada parte de las presentes Condiciones, es decir los Usuarios y Fundación Providencia, serán responsables entre sí de las obligaciones fiscales que les correspondan a cada uno por lo que se reconoce que Fundación Providencia no será responsable, en ningún momento, por el efectivo cumplimiento de las obligaciones fiscales o impositivas establecidas por la ley vigente que se generen para la OSC y/o el Donador. Fundación Providencia no tiene participación alguna en el desarrollo de lo estipulado en el perfil de Campaña y/o Proyecto, por lo que no será responsable por el efectivo cumplimiento de su realización. En ningún caso Fundación Providencia será responsable por cualquier otro daño y/o perjuicio que pudiera causarse o se cause por las acciones u omisiones entre el Donador y/o la OSC.</span></p>
                    <p class="p3"><span class="s1">Por lo anterior, todos los impuestos, derechos y demás obligaciones fiscales que se causen y en los que incurra Fundación Providencia o la OSC con motivo del uso del Sitio Web y los Servicios serán responsabilidad exclusiva de cada uno, según corresponda. Sin embargo, las OSC&rsquo;s serán responsables del pago de cualquier Impuesto Sobre la Renta pagadero por lo atribuible a las OSC&rsquo;s como resultado de la entrega de las Aportaciones.</span></p>
                    <p class="p2"><span class="s1"><strong>IX. Alcance del uso del Sitio Web.</strong></span></p>
                    <p class="p3"><span class="s1">Los Usuarios reconocen y aceptan que Fundación Providencia no es una institución de crédito, intermediario financiero, o una empresa dedicada a otorgar préstamos, financiamientos o recaudar recursos del público en general, si no que su finalidad principal es la de proveer a los Usuarios una plataforma a través de la cual puedan recaudar recursos para la implementación de lo estipulado en el perfil de Campaña y/o Proyecto, siendo únicamente un facilitador e intermediario entre las OSC&rsquo;s y Donadores, por lo que el acceso al Sitio Web no garantiza que la OSC obtenga los fondos requeridos para la atención de lo estipulado en el perfil de Campaña y/o Proyecto y Fundación Providencia tampoco garantiza ni responde por la veracidad, exactitud, licitud, actualidad, utilidad, adecuación ni la finalidad de las Campañas frente a los Donadores. Tanto el acceso y utilización de los Servicios y del Sitio Web, así como el uso que pueda hacerse de la información y contenidos incluidos en los mismos, o que sea accesible desde los mismos, se efectúa bajo la exclusiva responsabilidad de cada Usuario.</span></p>
                    <p class="p3"><span class="s1">Por lo anterior, Fundación Providencia no responderá en ningún momento y bajo circunstancia alguna por los daños y perjuicios ya sea directos, indirectos, incidentales, especiales, punitivos o emergentes, incluidos, el lucro cesante, la pérdida de datos, la lesión personal o el daño a la propiedad, ni de perjuicios relativos, o en relación con, o de otro modo derivados de cualquier uso o percance sufrido por cualquier Usuario en relación al uso de la plataforma y por tanto del Sitio Web o relacionado con los Servicios.</span></p>
                    <p class="p3"><span class="s1">Asimismo, Fundación Providencia no será responsable de daño, responsabilidad o pérdida alguna que deriven o se relacionen con falla o falta de continuidad o la imposibilidad de utilizar el Sitio Web y/o los Servicios, aunque Fundación Providencia hubiera sido advertido de la posibilidad de dichos daños.</span></p>
                    <p class="p3"><span class="s1">Los Servicios y el uso del Sitio Web se proporcionan &ldquo;tal cual&rdquo; por lo que Fundación Providencia renuncia frente a los Usuarios a toda declaración y representación, expresa, implícita o estatutaria, no expresamente establecida en estas Condiciones, incluidas las garantías implícitas de comerciabilidad, idoneidad para un fin particular, entre otros, respecto de los Servicios y el uso del Sitio Web. Además, Fundación Providencia no hace declaración ni presta garantía alguna relativa a la fiabilidad, calidad, idoneidad o disponibilidad de lo estipulado en las Campañas y/o Proyectos o cualquiera de los Servicios del Sitio Web, o que éstos no serán interrumpidos o estén libres de errores. El Usuario conviene y acepta que todo riesgo derivado del uso que haga del Sitio Web será únicamente suyo. Los Usuarios quedan apercibidos de que la concertación de acuerdos entre Usuarios para la Recaudación de fondos respecto de las Campañas y/o Proyectos es el resultado de acuerdos libres celebrados entre Usuarios mayores de edad, que actúan bajo su plena y absoluta responsabilidad, quedando Fundación Providencia excluida expresamente de cualquier tipo de responsabilidad por cualesquiera consecuencias derivadas de los acuerdos celebrados entre los mismos Usuarios.</span></p>
                    <p class="p3"><span class="s1">No obstante lo anterior, el Usuario entiende y acepta todos los aspectos y riesgos que significa el uso del Sitio Web mediante Internet dado el carácter abierto, descentralizado y global de esta red de comunicaciones, quedando Fundación Providencia excluido expresamente cualquier tipo de responsabilidad y por cualesquiera consecuencias derivadas de dichos riesgos (incluyendo seguridad) y las consecuencias que de ellos pudieran derivarse.</span></p>
                    <p class="p3"><span class="s1">Fundación Providencia no será responsable en ningún momento de los perjuicios relativos, o en relación con, o de otro modo derivados de cualquier uso del Sitio Web. De igual manera Fundación Providencia no será responsable de cualquier daño, responsabilidad o pérdida que deriven de: (i) el uso o dependencia del Sitio Web o su incapacidad para acceder a él o utilizarlo; o (ii) cualquier transacción o relación entre la OSC y cualquier Donador, tampoco será responsable de la falta de ejecución resultante de lo estipulado en las Campañas y/o Proyectos que vayan más allá del control razonable de Fundación Providencia.</span></p>
                    <p class="p3"><span class="s1">Las OSC&rsquo;s podrán utilizar el Sitio Web de Fundación Providencia para solicitar y planificar la recaudación de Aportaciones para lo estipulado en la Campaña y/o Proyecto pero acepta que Fundación Providencia no tiene responsabilidad alguna hacia el Usuario, OSC o Donador, según sea aplicable, en relación con el uso del Sitio Web, los Servicios o cualquier Campaña que no sea como se ha establecido expresamente en estas Condiciones.</span></p>
                    <p class="p2"><span class="s1"><strong>X. Indemnización.</strong></span></p>
                    <p class="p3"><span class="s1">Al aceptar las presentes Condiciones cada Usuario declara y se obliga a indemnizar y sacar en paz y a salvo a Fundación Providencia, sus afiliadas, subsidiarias, sociedades controladoras o controladas, partes relacionadas, accionistas, administradores, apoderados, funcionarios, asesores y empleados de cualquier daño, perjuicio, reclamación, demanda, pérdida, responsabilidad y gasto (incluidos los honorarios razonables de abogados) ocasionado por el incumplimiento del Usuario respecto de cualquier disposición contenida en las presentes Condiciones o de cualquier ley o regulación aplicable a las mismas, al igual que el incumplimiento o violación de los derechos de terceros incluyendo, otros Usuarios, y por el mal uso del Sitio Web.</span></p>
                    <p class="p2"><span class="s1"><strong>XI. Propiedad intelectual e industrial.</strong></span></p>
                    <p class="p3"><span class="s1">Todos los contenidos del Sitio Web y los relativos a los Servicios, así como los programas, bases de datos, redes y archivos entre otros, que permiten a las OSC&rsquo;s y Donadores acceder y usar su cuenta de Usuario, son propiedad de Fundación Providencia y están protegidas por las leyes y los tratados internacionales de derecho de autor, marcas, patentes, modelos y diseños industriales. El uso indebido y la reproducción total o parcial de dichos contenidos quedan prohibidos a los Usuarios, salvo autorización expresa, otorgada de manera previa y por escrito por Fundación Providencia.</span></p>
                    <p class="p3"><span class="s1">Asimismo los Usuarios reconocen y aceptan que todos los derechos de propiedad intelectual e industrial, así como todo el software y los contenidos del Sitio Web, incluidos códigos HTML, textos, animaciones, imágenes, así como las marcas, nombres comerciales y/o signos distintivos mostrados, son propiedad de Fundación Providencia, o ésta cuenta con las licencias suficientes y necesarias para su uso, y las cuales están protegidos por las leyes nacionales e internacionales de propiedad intelectual e industrial, por lo que los Usuarios en ningún momento y bajo ninguna circunstancia podrán ni tendrán la facultad de reproducir, modificar, preparar obras derivadas, distribuir, licenciar, arrendar, vender, revender, transferir, exhibir públicamente, presentar públicamente, transmitir, retransmitir o explotar de otra forma el Sitio Web o los Servicios ni cualquier de los rubros mencionados anteriormente, por lo que desde este momento los Usuarios se obligan a sacar en paz y salvo a Fundación Providencia, sus afiliadas, subsidiarias, sociedades controladoras o controladas, accionistas, administradores, apoderados, funcionarios, asesores y empleados de cualquier demanda daño o perjuicio ocasionado por el incumplimiento del Usuario a la prohibición mencionada anteriormente, independientemente de la acciones legales que pudiera tomar Fundación Providencia en contra de dicho Usuario, a quien, desde el momento en que Fundación Providencia tenga conocimiento del mencionado incumplimiento le negará el acceso al Sitio Web.</span></p>
                    <p class="p3"><span class="s1">Los derechos de propiedad intelectual o industrial que se utilicen en cada una de las Campañas y/o cada uno de los Proyectos que se publiquen a través del Sitio Web son responsabilidad única y exclusiva de la OSC, quien se obliga y acepta pagar todas las regalías y otras cantidades debidas a cualquier persona o entidad relacionadas con el uso de dichos derechos en una Campaña y/o Proyecto. Derivado de lo anterior, el Promotor se obliga a indemnizar y sacar en paz y a salvo a Fundación Providencia, sus afiliadas, subsidiarias, sociedades controladoras o controladas, partes relacionadas, accionistas, administradores, apoderados, funcionarios, asesores y empleados de cualquier daño, perjuicio, reclamación, demanda, pérdida, responsabilidad y gasto (incluidos los honorarios razonables de abogados) ocasionado por el uso indebido de dichos derechos o el incumplimiento de la OSC de las condiciones que en su momento le permitan el uso de dichos derechos de propiedad intelectual o de cualquier ley o regulación aplicable a los mismos, al igual que el incumplimiento o violación de derechos de terceros. Adicionalmente cada OSC manifiesta su consentimiento en hacerse responsable y eventualmente pagar cualquier regalía que se le reclame a Fundación Providencia por el uso indebido o la infracción de los derechos de propiedad intelectual o industrial que se utilicen en la Campaña y/o en el Proyecto, según corresponda, en virtud del uso del Sitio Web.</span></p>
                    <p class="p3"><span class="s1">Derivado de lo anterior, por este medio la OSC cede a Fundación Providencia el derecho universal no exclusivo y libre de regalías para: usar, mostrar, transmitir, reproducir, codificar, copiar el contenido de la Campaña y/o Proyecto para comercializar y vender servicios a los que de tiempo en tiempo se dedique; permitir mostrar, transmitir, reproducir, descargar, distribuir, coleccionar y usar el contenido de la Campaña con otros Usuarios; y publicar el nombre (o nombres), marcas comerciales, semejanzas y materiales personales y biográficos de la OSC relacionados con la Campaña y/o Proyecto.</span></p>
                    <p class="p3"><span class="s1">No obstante, los titulares de derechos de propiedad intelectual o industrial, distintos de las OSC&rsquo;s, podrán identificar y solicitar la remoción de aquellas Campañas y/o Proyectos que a su criterio infrinjan o violen sus derechos. En caso de que Fundación Providencia sospeche que se está cometiendo o se ha cometido una actividad ilícita o infractora de derechos de propiedad intelectual o industrial, se reservará el derecho de adoptar todas las medidas legales que estime pertinentes.</span></p>
                    <p class="p3"><span class="s1">En el Sitio Web los Usuarios podrán sumar, crear, cargar, ingresar, distribuir, recolectar o publicar contenidos, videos, fragmentos de audio, comentarios en foros escritos, información, texto, fotografía, software, guiones, gráficos u otra información sin intervención alguna de Fundación Providencia y por decisión única y personal del Usuario.</span></p>
                    <p class="p3"><span class="s1">La propiedad intelectual e industrial de dicha información es responsabilidad única y exclusiva del Usuario y por tanto libera de toda responsabilidad a Fundación Providencia sobre los derechos de propiedad intelectual de dicha información.</span></p>
                    <p class="p3"><span class="s1">Finalmente, cada Usuario reconoce y acepta que el Sitio Web podrá contener o incluir enlaces a páginas que, aún dentro del Sitio Web, se encuentren bajo responsabilidad de terceros lo cual indica que no son propiedad y tampoco son operados por Fundación Providencia. En dicho caso, Fundación Providencia no puede controlar o supervisar en modo alguno dichos contenidos, materiales, acciones y/o servicios prestados por terceros, por lo que Fundación Providencia queda excluido, con toda la extensión permitida por la ley, de responsabilidad alguna de cualquier naturaleza, derivada de la existencia o posibilidad de acceso y uso de dichos contenidos, materiales, acciones y/o servicios prestados por terceros, por lo que Fundación Providencia no será en ningún caso responsable, ni siquiera de forma indirecta o subsidiaria, por productos o servicios prestados u ofertados por otras personas o entidades, o por contenidos, informaciones, comunicaciones, opiniones o manifestaciones de cualquier tipo originados o vertidos por terceros y que resulten accesibles a través del Sitio Web; no obstante lo anterior, Fundación Providencia empleará razonablemente los medios a su alcance para proveer de sistemas de seguridad que protejan de forma razonable sus sistemas y los datos contenidos en los mismos contra ataques deliberados, software malignos o dañinos. La presencia de enlaces a otros sitios web no implica una sociedad, relación, aprobación, respaldo de Fundación Providencia a dichos sitios y sus contenidos.</span></p>
                    <p class="p2"><span class="s1"><strong>XII. Notificaciones de Derechos de Autor.</strong></span></p>
                    <p class="p3"><span class="s1">Fundación Providencia podrá, a su discreción, cancelar la cuenta de los Usuarios que infrinjan los derechos de propiedad intelectual o derechos de autor de terceros. Fundación Providencia eliminará los materiales transgresores de acuerdo con las leyes mexicanas relacionadas con la propiedad industrial, la Ley Federal de Derechos de Autor, la ley estadounidense de protección de los derechos de autor (Digital Millennium Copyright) o cualquier otra ley aplicable, si se le notifica conforme lo establecido en las presente Condiciones que cierto contenido infringe los derechos de autor mencionados.</span></p>
                    <p class="p3"><span class="s1">Si algún Usuario considera que una obra de su propiedad o sobre la cual tiene derechos ha sido copiada de alguna forma que constituya una infracción a los derechos de autor, deberá hacer llegar a Fundación Providencia una notificación por escrito que contenga, al menos, la siguiente información:</span></p>
                    <ol class="ol3">
                        <li class="li4"><span class="s5">Firma física o electrónica de la persona autorizada para actuar en nombre del propietario del derecho de autor;</span></li>
                        <li class="li4"><span class="s5">Una descripción de la obra con derechos de autor que el Usuario afirma fue infringida o que está infringiendo;</span></li>
                        <li class="li4"><span class="s5">Una descripción de la ubicación del material que el Usuario afirme que fue infringido en el Sitio Web, para que Fundación Providencia ubique dicho material;</span></li>
                        <li class="li4"><span class="s5">Dirección, número telefónico y una dirección de correo electrónico;</span></li>
                        <li class="li4"><span class="s5">Una declaración escrita en la que alegue de buena fe y bajo protesta de decir verdad que el uso de la Campaña no está autorizado por el dueño del derecho de autor, su agente o la ley, y</span></li>
                        <li class="li4"><span class="s5">Una declaración por escrito en donde afirme que la información que presenta es precisa, y bajo penalización de perjurio, que es el dueño del derecho de autor o está autorizado para actuar en nombre del propietario.</span></li>
                        <li class="li4"><span class="s5">Si el Usuario o OSC considera que su Campaña y/o Proyecto fue eliminado o deshabilitado por error o por alguna identificación errónea, debe enviar a Fundación Providencia una notificación escrita que contenga, al menos, la siguiente información:</span></li>
                        <ol class="ol1">
                            <li class="li4"><span class="s5">Una firma electrónica o física de la OSC por medio de su representante;</span></li>
                            <li class="li4"><span class="s5">Una descripción de la Campaña que haya sido eliminado o cuyo acceso fue deshabilitado, y la ubicación en la que aparecía dicha Campaña antes de ser eliminado o deshabilitado;</span></li>
                            <li class="li4"><span class="s5">Una declaración hecha bajo protesta de decir verdad en la que la OSC manifieste que considera que la Campaña fue eliminado o deshabilitada como resultado de un error o una identificación errónea del material.</span></li>
                        </ol>
                    </ol>
                    <p class="p3"><span class="s1">El Usuario o OSC acepta que si no cumple con todos los requisitos de notificación mencionados, Fundación Providencia podría ignorar tales notificaciones incompletas o imprecisas sin ninguna responsabilidad.</span></p>
                    <p class="p3"><span class="s1">Las comunicaciones dirigidas a Fundación Providencia en relación con temas de derechos de autor, deberán ser enviadas a la siguiente dirección:</span></p>
                    <p class="p3"><span class="s1">Fundación Providencia </span></p>
                    <p class="p3"><span class="s1">Paseo de la Reforma 319</span></p>
                    <p class="p3"><span class="s1">Piso 1, Lomas de Chapultepec.</span></p>
                    <p class="p3"><span class="s1">CDMX, C.P. 11000.</span></p>
                    <p class="p3"><span class="s1">Teléfono: (55) 41616861</span></p>
                    <p class="p3"><span class="s1">email: legal@providencia.org.mx</span></p>
                    <p class="p5">&nbsp;</p>
                    <p class="p2"><span class="s1"><strong>XIII. Inexistencia de Relación Laboral.</strong></span></p>
                    <p class="p3"><span class="s1">El Usuario reconoce y acepta que no existe relación laboral entre la OSC y Fundación Providencia o el Donador y Fundación Providencia, siendo esta último sólo un intermediario y facilitador del del Sitio Web, por lo que no existe la prestación de un trabajo personal subordinado mediante el pago de un salario, sino que es una intermediación siendo la OSC quien solicita los Servicios y el poder usar el Sitio Web y Fundación Providencia quien provee los Servicios y autoriza el uso del Sitio Web; por ende la OSC reconoce y acepta que las únicas relaciones jurídicas existentes entre éste y Fundación Providencia son las derivadas de las presentes Condiciones y por tanto la OSC se constituye como único patrón del personal que ocupe para la realización de su Campaña y/o Proyecto, el cual se entenderá bajo su inmediata dirección y dependencia, respecto del pago de salarios ordinarios y extraordinarios, vacaciones, aguinaldos, primas de antig&uuml;edad, así como de cualquier otra prestación que por motivo de su trabajo pudiera corresponderle, quedando totalmente a su cargo todas las obligaciones que deriven de las relaciones laborales, contractuales y fiscales con sus trabajadores por lo que la OSC es el único responsable de las violaciones que en virtud de las disposiciones legales y demás ordenamientos en materia de trabajo y seguridad social se deriven frente a dicho personal, liberando a Fundación Providencia de cualquier responsabilidad solidaria y reclamación que hagan sus trabajadores al respecto, por lo que Fundación Providencia no aceptará ninguna reclamación por los conceptos antes citados, obligándose la OSC a sacar en paz y a salvo a Fundación Providencia de cualquier reclamación que intente en su contra el personal y terceros relacionados con la OSC y a reembolsarle a Fundación Providencia, a la vista, cualquier cantidad que éste erogue por tal motivo.</span></p>
                    <p class="p3"><span class="s1">Este acuerdo no crea ningún contrato de sociedad, de franquicia, o relación laboral entre Fundación Providencia, la OSC y el Donador.</span></p>
                    <p class="p2"><span class="s1"><strong>XIV. Documentos complementarios.</strong></span></p>
                    <p class="p3"><span class="s1">Forman parte complementaria, integral e inseparable de las presentes Condiciones, según resulte aplicable, los siguientes documentos:</span></p>
                    <ol class="ol2">
                        <li class="li4"><span class="s5">Contrato de Colaboración;</span></li>
                        <li class="li4"><span class="s5">Contrato de autorización de imagen; y</span></li>
                        <li class="li4"><span class="s5">Aviso de Privacidad</span></li>
                    </ol>
                    <p class="p2"><span class="s1"><strong>XV. Jurisdicción y Ley Aplicable.</strong></span></p>
                    <p class="p3"><span class="s1">Las presentes Condiciones se regirán exclusivamente por la legislación y las leyes vigentes en los Estados Unidos Mexicanos, y en particular respecto de mensajes de datos, contratación electrónica y comercio electrónico, éstos se regirán por lo dispuesto por la legislación federal respectiva. Cualquier controversia derivada de las presentes Condiciones, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a los tribunales competentes de la Ciudad de México.</span></p>
                    <p class="p3"><span class="s1">Última revisión Febrero 2019</span></p>

                </div>



                <div style="display:none" role="form" class="wpcf7" id="wpcf7-f77-p600-o1" lang="es-MX" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="/legales/#wpcf7-f77-p600-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="77">
                            <input type="hidden" name="_wpcf7_version" value="5.0.5">
                            <input type="hidden" name="_wpcf7_locale" value="es_MX">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f77-p600-o1">
                            <input type="hidden" name="_wpcf7_container_post" value="600">
                        </div>
                        <div class="column one"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nombre" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;"></span> </div>
                        <div class="column  one-second"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail"></span> </div>
                        <div class="column  one-second"><span class="wpcf7-form-control-wrap tel-861"><input type="tel" name="tel-861" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" placeholder="Teléfono"></span> </div>
                        <div class="column one"><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="5" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Mensaje"></textarea></span></div>
                        <div class="column one"><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></div>
                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>


        </div>

        <div class="row english">
            
        </div>
    </div>
</section>

<!-- BEPART -->
@include('layout.bepart')

<!-- Newsletter -->
@include('layout.newsletter')

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section('js')
<script type="text/javascript" src="{{asset('js/jquery-migrate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    //header slider
    $('.header-fade').slick({
        infinite: true,
        slidesToShow: 1,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
</script>
<!--Alertas de sweet Alert-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>

@if ($errors->any())
<script>
    $(function() {
        swal({
            title: "Error",
            text: "Error en email ingresado",
            icon: "error",
            button: "OK",
        });
    });
</script>
@endif
@if ($message = Session::get('success'))
<script>
    $(function() {
        swal({
            title: "Gracias por registrarte",
            icon: "success",
            button: "OK",
        });
    });
</script>
@endif

@endsection

@section('styles')

<style>
    #animated_image:hover {
        overflow: hidden;
        transition: all 1.5s ease;
        transform: scale(1.3);
    }
</style>
@endsection
