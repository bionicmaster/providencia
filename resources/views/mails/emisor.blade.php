<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Providencia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td align="center" >
                <img src="{{asset('mail/formulario.png')}}"  style="width:650px; height:auto;"/>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff" style="padding-top:20px;">
                <p style="font-family:roboto-light,Arial,Helvetica,sans-serif; color:#999999; font-size:22px; text-align:justify; padding-left:40px; padding-right:40px;" >
                    <b>Se ha generado una nueva solicitud de proyecto.</b> <br><br>
                    Entra al panel de control en la sección de Solicitudes para revisión
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{asset('mail/formulariofoot.png')}}"  style="width:650px; height:auto;"/>
            </td>
        </tr>
    </table>
</body>
</html>