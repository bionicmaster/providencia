<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Recibo de Donación</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="{{asset('images/logo-providencia-2.png')}}" style="width:100%; max-width:170px;">
                        </td>

                        <td>
                            Donación #:don-{{$donation->id}}<br>
                            Fecha: {{$donation->date_parsed}}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Fundación Providencia<br>
                            Paseo de la Reforma 319<br>
                            Lomas de Chapultepec,CDMX 11000<br>
                            5541616861
                        </td>

                        <td>
                            {{$donation->donor_name}}<br>
                            {{$donation->donor_email}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>
                Método de Pago
            </td>

            <td>
                Identificador
            </td>
        </tr>

        <tr class="details">
            <td>
                Tarjeta
            </td>

            <td>
                {{$donation->processor_id}}
            </td>
        </tr>

        <tr class="heading">
            <td>
                {{$donation->project_name}}
            </td>

            <td>
                $ {{$donation->amount}}
            </td>
        </tr>

        <tr class="item">
            <td>
                {{$donation->project_name}}
            </td>


        </tr>

    </table>
    <div style="width: 100%;text-align:center;">

        <p style="font-size: 20px;font-weight: bold;">¡Gracias por tu donativo!</p>
    </div>
</div>
</body>
</html>
