<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Providencia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
            <td align="center" >
                <img src="{{asset('mail/head.png')}}"  style="width:650px; height:auto;"/>
            </td>
        </tr>
        <tr>
            <td bgcolor="#1580AC" style="padding-top:20px;">
                <p style="font-family:roboto-light,Arial,Helvetica,sans-serif; color:#fff; font-size:20px; text-align:justify; padding-left:40px; padding-right:40px;" >
                    Estimado (a) {{$name}}<br><br>
                    {{$organization}} esta esperando su pago<br><br>
                    <b>Fundación Providencia</b> es la plataforma tecnológica gratuita para la procuración de fondos en línea, para las organizaciones de la sociedad civil.<br><br>
                    <b>Entregaremos el 100% de tu donativo</b> (no cobramos comisión) a {{$organization}} y te mantendremos informado del aprovechamiento de los recursos.<br><br>
                    Para revisar tu boleta de pago da clic en el siguiente botton:<br><br>
                    <a style="background-color: black; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer;" color: white; href="{{$linkUrl}}">Ver boleta de pago</a><br><br>
                    Tambien puedes revisarlo copiando y pegando el siguiente link en tu navegador:<br><br>
                    <a style="color: white;" color: white; href="{{$linkUrl}}">{{$linkUrl}}</a><br><br><br>
                    <b>¡Muchas Gracias!</b><br><br>
                    <b style="float:right;">Equipo Fundación Providencia</b>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{asset('mail/formulariofoot.png')}}"  style="width:650px; height:auto;"/>
            </td>
        </tr>
    </table>
</body>
</html>