@extends('layout.master')
@section('head')
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$project->name}}" />
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/project/{{$project->id}}" />
    <meta property="og:description" content="{{$project->description}}">
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('storage/'.$project->main_media)}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>{{$project->name}} | Providencia</title>
@endsection

@section('content')
@include('layout.navigation')
    <!-- HEADER -->

    <section class="providencia-header margin-header" style="background-image: url('{{asset('storage/'.$project->main_media)}}');">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <a href="{{route('home')}}">
                        <img class="providencia-header_logo" src="{{asset('/images/logo-providencia-white.png')}}" alt="Providencia foundation">
                    </a>
                    <h2 class="providencia-header_title text-white espanol">
                            {{$project->subtitulo1}}{{$project->subtitulo2}}
                    </h2>
                    <h2 class="providencia-header_title text-white english">
                        {{$project->subtitulo1_english}}{{$project->subtitulo2_english}}
                    </h2>
                    <a class="providencia-header_btn hvr-grow espanol" href="" data-toggle="modal" data-target="#exampleModal">DONAR</a>
                    <a class="providencia-header_btn hvr-grow english" href="" data-toggle="modal" data-target="#exampleModal">DONATE</a>
                </div>
            </div>
        </div>
    </section>

    <!-- INTRO -->
    <section class="providencia-intro">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center espanol">
                    <img class="providencia-intro_logo img-fluid" src="{{asset('storage/'.$project->logo)}}" alt="{{$project->name}}">
                    <h1 class="text-blue providencia-intro_title" style="text-transform: uppercase">{{$project->name}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$project->subtitulo1}} {{$project->subtitulo2}}</h3>
                    @if(isset($project->new_view) && $project->new_view!=null)
                        {!! $project->new_view !!}
                    @else
                        {!! $project->area1 !!}
                    @endif
                    
                </div>
                <div class="col-12 text-center english">
                    <img class="providencia-intro_logo img-fluid" src="{{asset('storage/'.$project->logo)}}" alt="{{$project->name_english}}">
                    <h1 class="text-blue providencia-intro_title" style="text-transform: uppercase">{{$project->name_english}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$project->subtitulo1_english}} {{$project->subtitulo2_english}}</h3>
                    @if(isset($project->new_view_english) && $project->new_view_english!=null)
                        {!! $project->new_view_english !!}
                    @else
                        {!! $project->area1_english !!}
                    @endif
                </div>
            </div>
        </div>
    </section>


    <!-- MISION VISION -->
    <section class="providencia-vision bg-blue text-white text-center">
        <div class="container-fluid">
            <!-- mission -->
            <div class="row">
                <div class="col-12 espanol">
                    <hr class="providencia-vision_line">
                    {!! $project->area2 !!}
                </div>
                <div class="col-12 english">
                    <hr class="providencia-vision_line">
                    {!! $project->area2_english !!}
                </div>
            </div>
        </div>
    </section>

    <section class="providencia-video">
        <div class="container-fluid">
            <div class="row espanol">
                <div class="col-12 pd-0">
                    {!! $project->extramedia !!}
                </div>
            </div>
            <div class="row english">
                <div class="col-12 pd-0">
                    {!! $project->extramedia_english !!}
                </div>
            </div>
        </div>
    </section>
    <section class="providencia-vision bg-blue text-white text-center" style="margin-top: -17px;">

        <div class="container-fluid espanol">
            {!! $project->area3 !!}
        </div>
        <div class="container-fluid english">
            {!! $project->area3_english !!}
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center espanol">
                    <a class="providencia-vision_btn text-blue hvr-grow" href="" data-toggle="modal" data-target="#exampleModal">DONAR</a>
                </div>
                <div class="col-12 text-center english">
                    <a class="providencia-vision_btn text-blue hvr-grow" href="" data-toggle="modal" data-target="#exampleModal">DONATE</a>
                </div>
            </div>
        </div>
    </section>

<!-- PAYMENT -->
@include('layout.share')

<!-- ODS -->
<section class="providencia-ods text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="providencia-ods_title">ODS</h2>
                <p class="providencia-ods_text gray mx-auto espanol">El 25 de septiembre de 2015, los líderes mundiales
                    adoptaron un conjunto de objetivos globales para erradicar la pobreza, proteger el planeta y
                    asegurar la prosperidad para todos como parte de una nueva agenda de desarrollo sostenible. Cada
                    objetivo tiene metas específicas que deben alcanzarse en los próximos 15 años.</p>
                <p class="providencia-ods_text gray mx-auto english">On September 25, 2015, world leaders adopted a set of global goals to eradicate poverty, protect the planet, and ensure prosperity for all as part of a new sustainable development agenda. Each objective has specific goals that must be achieved in the next 15 years.</p>
            </div>
        </div>



        <div class="row">
            <div class="col-12">
                <div class="projects-objetivos text-center mx-auto">
                    <img class="projects-objetivos_title mx-auto" src="{{asset('/images/logo-objetivos.png')}}" alt="Objetivos de desarrollo sostenible">

                    @foreach($objetivos as $objetivo)
                        <img class="projects-objetivos_elem" src="{{asset('storage/'.$objetivo->logo)}}">
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center espanol">
                <a class="providencia-vision_btn text-blue hvr-grow" href="" data-toggle="modal" data-target="#exampleModal">DONAR</a>
            </div>
            <div class="col-12 text-center english">
                <a class="providencia-vision_btn text-blue hvr-grow" href="" data-toggle="modal" data-target="#exampleModal">DONATE</a>
            </div>
        </div>
    </div>
</section>


<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@include('layout.modal-donate', ['proyecto' => "5" , 'project_id' => "2"])



@endsection

@section('js')
    <script>
        $('body').addClass('providencia-project christel-house');
        $('#botonDonar').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });

        $("#cantidadTexto").change(function () {
            // your code here
            if ($(this).val() == '1') {
                $("#price").val("200");
            } else if ($(this).val() == '2') {

                $("#price").val("500");
            } else if ($(this).val() == '3') {
                $("#price").val("");
            }

        });

        $("#donationForm").submit(function (event) {
            event.preventDefault();
            $(this)[0].submit();

        });

    </script>
    <script>

        $(document).ready(function() {
            setTimeout(function(){ 
                @if ($modal ==1)
                    $('#exampleModal').modal('show'); 
                @endif
            }, 1000);
        });
    
    
    </script>
@endsection