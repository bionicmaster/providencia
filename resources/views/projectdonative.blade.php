@extends('layout.master')
@section('head')
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{$project->name}}"/>
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/project/{{$project->id}}"/>
    <meta property="og:description" content="{{$project->description}}">
    <meta property="og:site_name" content="Providencia"/>
    <meta property="og:image" content="{{asset('storage/'.$project->main_media)}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/socialshare.css" />
    <link rel="stylesheet" href="/images/icons/all.css">
    <title>{{$project->name}} | Providencia</title>
@endsection

@section('content')
    @include('layout.navigation')
    <!-- HEADER -->
    <div class="row providencia-header-donate">
        <div class="col-md-7">
            <section class="providencia-header"
                     style="background-image: url('{{asset('storage/'.$project->main_media)}}');">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{route('home')}}">
                                <img class="providencia-header_logo"
                                     src="{{asset('/images/logo-providencia-white.png')}}" alt="Providencia foundation">
                            </a>
                            <h2 class="providencia-header_title text-white espanol">
                                {{$project->subtitulo1}}{{$project->subtitulo2}}
                            </h2>
                            <h2 class="providencia-header_title text-white english">
                                {{$project->subtitulo1_english}}{{$project->subtitulo2_english}}
                            </h2>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-5 providencia-project-description">
            <h1>{!! $project->name !!}</h1>
            <h2 class="espanol"><i class="fa fa-heart-o" aria-hidden="true"></i> Donativos</h2>
            <h2 class="english"><i class="fa fa-heart-o" aria-hidden="true"></i> Donations</h2>
            <h2>$ {{ number_format($cantidad, 2) }}</h2>
            <h2 class="espanol">Donantes</h2>
            <h2 class="english">Donors</h2>
            <h2>{{ $donantes ?? '' }}</h2>
            <div class="text-center">
                @if($showgoal)
                    <div class="row">
                        <div class="col-10 offset-1 espanol text-left font-weight-bold">RECAUDADO</div>
                        <div class="col-10 offset-1 english text-left font-weight-bold">RAISED</div>
                    </div>
                    <div class="row">
                        <div class="col-10 container-raised offset-1">
                            <div class="row container-raised-percentage" style="background: linear-gradient(to right,
                             rgba(83,180,58,1) 0%,rgba(83,180,58,1) calc({{$porcentaje}}% + 15px),rgba(255,255,255,1)
                                calc({{$porcentaje}}% + 15px),rgba(255,255,255,1) 100%);">
                                <div class="col-6 text-left">
                                    <h6 style="background-image: linear-gradient(90deg, rgba(255,255,255,1) 0,rgba(255,255,255,1) calc({{$por1}}% + 15px),rgba(0,0,0,1) calc({{$por1}}% + 15px),rgba(0,0,0,1) 100%)">
                                        {{ number_format($porcentaje, 0) }}%
                                    </h6>
                                </div>
                                <div class="col-6 text-right">
                                    <h6 style="background-image: linear-gradient(90deg, rgba(255,255,255,1) 0,rgba(255,255,255,1) calc({{$por2}}% + 15px),rgba(0,0,0,1) calc({{$por2}}% + 15px),rgba(0,0,0,1) 100%)">
                                        {{ number_format($cantidad, 0) }}
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-10 offset-1">
                            <div class="row goal-text">
                                <div class="col-6 text-left espanol">La meta es {{ number_format($meta, 0) }}</div>
                                <div class="col-6 text-left english">The goal is {{ number_format($meta, 0) }}</div>
                                <div class="col-6 text-right espanol">{{ $quedan }} días restantes</div>
                                <div class="col-6 text-right english">{{ $quedan }} days left</div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <button class="espanol btn btn-red hvr-grow btn-donate-now col-10 donar_ahora">DONAR AHORA
                        </button>
                        <button class="english btn hvr-grow btn-donate-now col-10 donar_ahora">DONATE NOW
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 social-share social-share-sticky" data-social-share='{"iconSrc":
                    "/images//icons/",
                    "title": "{{$project->name}}", "description": "{{$project->description}}"}' data-share-url="{{
                    URL::current() }}">
                        <button class="espanol btn btn-covid btn-donate-now col-10 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            COMPARTIR
                        </button>
                        <button class="english btn btn-covid btn-donate-now col-10 dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            SHARE
                        </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-multi col-10">
                            <div class="dropdown-row"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- INTRO -->
    <section class="providencia-intro">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center espanol">
                    <img class="providencia-intro_logo img-fluid" src="{{asset('storage/'.$project->logo)}}"
                         alt="{{$project->name}}">
                    <h1 class="text-blue providencia-intro_title"
                        style="text-transform: uppercase">{{$project->name}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$project->subtitulo1}} {{$project->subtitulo2}}</h3>
                    @if(isset($project->new_view) && $project->new_view!=null)
                        {!! $project->new_view !!}
                    @else
                        {!! $project->area1 !!}
                    @endif

                </div>
                <div class="col-12 text-center english">
                    <img class="providencia-intro_logo img-fluid" src="{{asset('storage/'.$project->logo)}}"
                         alt="{{$project->name_english}}">
                    <h1 class="text-blue providencia-intro_title"
                        style="text-transform: uppercase">{{$project->name_english}}</h1>
                    <h3 class="gray providencia-intro_subtitle">{{$project->subtitulo1_english}} {{$project->subtitulo2_english}}</h3>
                    @if(isset($project->new_view_english) && $project->new_view_english!=null)
                        {!! $project->new_view_english !!}
                    @else
                        {!! $project->area1_english !!}
                    @endif
                </div>
            </div>
        </div>
    </section>


    <!-- MISION VISION -->
    <section class="providencia-vision bg-blue text-white text-center">
        <div class="container-fluid">
            <!-- mission -->
            <div class="row">
                <div class="col-12 espanol">
                    <hr class="providencia-vision_line">
                    {!! $project->area2 !!}
                </div>
                <div class="col-12 english">
                    <hr class="providencia-vision_line">
                    {!! $project->area2_english !!}
                </div>
            </div>
        </div>
    </section>

    <section class="providencia-video">
        <div class="container-fluid">
            <div class="row espanol">
                <div class="col-12 pd-0">
                    {!! $project->extramedia !!}
                </div>
            </div>
            <div class="row english">
                <div class="col-12 pd-0">
                    {!! $project->extramedia_english !!}
                </div>
            </div>
        </div>
    </section>
    <section class="providencia-vision bg-blue text-white text-center" style="margin-top: -17px;">

        <div class="container-fluid espanol">
            {!! $project->area3 !!}
        </div>
        <div class="container-fluid english">
            {!! $project->area3_english !!}
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center espanol">
                    <button class="providencia-vision_btn text-blue hvr-grow donar_ahora">DONAR</button>
                </div>
                <div class="col-12 text-center english">
                    <button class="providencia-vision_btn text-blue hvr-grow donar_ahora">DONATE</button>
                </div>
            </div>
        </div>
    </section>
    <section class="donors">

    </section>
    <section class="donors-list text-center">
        <div id="collapse-list" class="list-group">
            @foreach($quienes as $quien)
                <div class="list-group-item">
                    <div class="row">
                        <div class="col-md-4 offset-md-2 list-group-elements">
                            <h3>{{ date_format(date_create_from_format('Y-m-d', $quien['fecha']), 'd m Y') }}</h3>
                            @if($quien['anonimo'])
                                <h3><strong class="espanol">ANÓNIMO</strong><strong class="english">ANONYMOUS</strong>
                                </h3>
                            @else
                                <h3><strong>{!! Str::upper($quien['nombre']) !!}</strong></h3>
                            @endif
                            <h4 class="espanol">Contribuyó con está causa con:
                                ${{ number_format($quien['cantidad'], 2) }}</h4>
                            <h4 class="english">Contributed with this cause with:
                                ${{ number_format($quien['cantidad'], 2) }}</h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @if (count($quienes)>3)
            <button class="toggle-collapse btn-view-more" id="btn-more">
                <strong class="english">VIEW MORE >></strong>
                <strong class="espanol">VER MÁS >></strong>
            </button>
            <button class="toggle-collapse btn-view-more" id="btn-less">
                <strong class="english">VIEW LESS <<</strong>
                <strong class="espanol">VER MENOS <<</strong>
            </button>
        @endif

    </section>

    <!-- PAYMENT -->
    @include('layout.share')

    <!-- ODS -->
    <section class="providencia-ods text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="providencia-ods_title">ODS</h2>
                    <p class="providencia-ods_text gray mx-auto espanol">El 25 de septiembre de 2015, los líderes
                        mundiales
                        adoptaron un conjunto de objetivos globales para erradicar la pobreza, proteger el planeta y
                        asegurar la prosperidad para todos como parte de una nueva agenda de desarrollo sostenible. Cada
                        objetivo tiene metas específicas que deben alcanzarse en los próximos 15 años.</p>
                    <p class="providencia-ods_text gray mx-auto english">On September 25, 2015, world leaders adopted a
                        set of global goals to eradicate poverty, protect the planet, and ensure prosperity for all as
                        part of a new sustainable development agenda. Each objective has specific goals that must be
                        achieved in the next 15 years.</p>
                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div class="projects-objetivos text-center mx-auto">
                        <img class="projects-objetivos_title mx-auto" src="{{asset('/images/logo-objetivos.png')}}"
                             alt="Objetivos de desarrollo sostenible">

                        @foreach($objetivos as $objetivo)
                            <img class="projects-objetivos_elem" src="{{asset('storage/'.$objetivo->logo)}}">
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-center espanol">
                    <button class="providencia-vision_btn text-blue hvr-grow donar_ahora">DONAR</button>
                </div>
                <div class="col-12 text-center english">
                    <button class="providencia-vision_btn text-blue hvr-grow donar_ahora" >DONATE</button>
                </div>
            </div>
        </div>
    </section>


    <!-- PAYMENT -->
    @include('layout.payments')

    <!-- FOOTER -->
    @include('layout.footer')

    @include('layout.modal-donate', ['proyecto' => "5" , 'project_id' => "2"])



@endsection
@section('js')
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/socialshare.js"></script>
    <script>
        $('body').addClass('providencia-project christel-house');
        $('#botonDonar').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });

        $("#cantidadTexto").change(function () {
            // your code here
            if ($(this).val() == '1') {
                $("#price").val("200");
            } else if ($(this).val() == '2') {

                $("#price").val("500");
            } else if ($(this).val() == '3') {
                $("#price").val("");
            }

        });

        $("#donationForm").submit(function (event) {
            event.preventDefault();
            $(this)[0].submit();

        });

    </script>
    <script>
        $( ".donar_ahora" ).click(function() {
            $('#exampleModal').modal('show');
        });

        $(document).ready(function () {
            setTimeout(function () {
                @if ($modal ==1)
                $('#exampleModal').modal('show');
                @endif
            }, 1000);

            let collapsetor = $('.toggle-collapse');
            let less = $('#btn-less');
            let more = $('#btn-more');

            let hideElements = function () {
                $('#collapse-list .list-group-item').each(function (i) {
                    if (i > 2) {
                        $(this).slideToggle(200);
                    }
                    less.hide();
                    more.show();
                });
            };

            hideElements();
            let toggleStatus = true;
            collapsetor.on('click', function () {
                if (toggleStatus) {
                    $('#collapse-list .list-group-item:not(:visible)').slideToggle(200);
                    less.show();
                    more.hide();
                } else {
                    hideElements();
                }
                toggleStatus = !toggleStatus;
            });
        });


    </script>
@endsection
