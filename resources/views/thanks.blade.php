
@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Gracias " /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <title>Gracias  | Providencia</title>
@endsection

@section('content')
@include('layout.navigation')
    <section class="dona">
        <div class="container pt-5 pb-5">
            <div class="row">
                <!-- MÉTODO DE PAGO -->
                <div class="col-12">
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-12 text-center">
                            <div class="row" style="background:#1580b4;">
                                <div class="col-12">
                                    <h2 class="text-center text-white mb-0 espanol"><b>¡Gracias!</b></h2>
                                    <h2 class="text-center text-white mb-0 english"><b>Thanks!!</b></h2>
                                </div>
                            </div>
                            <br>
                            <p class="dona_title" style="border-bottom: 1.5px solid #1580b4!important;font-size: 2vw">

                                @if(isset($mensaje))
                                    {{$mensaje}}
                                @else
                                    Tu pago ha sido realizado con exito, en breve recibirás un correo con la información de tu donativo.
                                @endif
                            </p>
                        </div>
                    </div>


                    <div class="modal-footer text-center">
                        <button onclick="location.href='{{route('home')}}'" type="button" class="btn btn-info espanol" data-dismiss="modal" style="margin: 0 auto;">Regresar al inicio</button>
                        <button onclick="location.href='{{route('home')}}'" type="button" class="btn btn-info english" data-dismiss="modal" style="margin: 0 auto;">Return home</button>
                    </div>
                </div>


            </div>
        </div>
        <div class="modal fade" id="notificationModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content notif-content">
                    <div class="modal-body notif-body">
                        <p class="notif-msg" id="error_msgTxt"></p>
                    </div>

                </div>
            </div>
        </div>



    </section>

<!-- FOOTER -->
@include('layout.footer')

@endsection
