<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Administrador" /> 
    <meta name="keywords" content="providencia, Providencia, Administrador"/>
    <meta property="og:url" content="https://www.providencia.org.mx/" />
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <title>Administrador | Providencia</title>
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- Styles -->
    <!-- Theme included stylesheets -->
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/datatables.min.css"/>
    <link rel="stylesheet" href="{{route('home')}}/css/admin.css">

    <!--datepicker-->
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
        body{
            background: #f0f3f5;
        }
        header{
            background:#1580b4;
            padding:5px;
            margin-bottom: 10px;
            box-shadow: 0px 6px 13px 0px #00000047;
            text-align: center;
        }

        .buscador{
            color: #000;
            padding:5px;
            margin-bottom: 10px;
            text-align: center;
        }
    </style>
</head>
<body>

<div class="container">
    <header class="row justify-content-center" style="padding:60px">
        <div class="col-12" style="margin-bottom: 5px">
            <button class="btn btn-outline-warning" onclick="location.href='{{route('home')}}/admin';">Regresar al administrador</button>
        </div>
        <div class="col-12" style="color:#fff;">
            @if(!isset(Auth::user()->project_id))
            <h3>Busccador de donativos, donadores y proyectos</h3>
            @else
            <h3>Buscador donativos y donadores</h3>
            @endif
        </div>
    </header>


    <!--Quitar-->
    @if(!isset(Auth::user()->project_id))
        <section class="row justify-content-center buscador">
            <div class="col-12  text-center">
                <label for="">¿Que desea buscar?</label><br>
                <input type="checkbox" id="proyecto">
                <label for="proyecto">Proyecto</label>&nbsp; 
                <input type="checkbox" id="donativo">
                <label for="donativo">Donativo ó donador</label>  
                <br><br>
                <div id="filtros-adicionales" style="display: none;">
                    <label for="">Desde</label>
                    <input type="text" id="fromDate">&nbsp;
                    <label for="">Hasta</label>  
                    <input type="text" id="toDate"><br>
                    <input type="checkbox" id="rfc">
                    <label for="rfc">Personas con RFC</label> <br><br>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center" style="margin-bottom: 20px;">
                    <input type="text" id="mainSearchInput" class="col-6" placeholder="Busca proyectos, donadores, etc" style="display: none;">
                </div>
                <div class="row justify-content-center">
                    <div class="col-6 searchResults"></div>
                </div>
            </div>
        </section>
    @else
        <section class="row justify-content-center buscador">
            <div class="col-12  text-center">
                <button class="col-3 btn btn-outline-warning" data-id="@if(isset(Auth::user()->project_id)){{Auth::user()->project_id}}@endif" data-type="project" id="todosLosPagos">Ver todos los pagos</button>
                <br><br>
                <div id="filtros-adicionales">
                    <label for="">Desde</label>
                    <input type="text" id="fromDate">&nbsp;
                    <label for="">Hasta</label>  
                    <input type="text" id="toDate"><br>
                    <input type="checkbox" id="rfc">
                    <label for="rfc">Personas con RFC</label> <br><br>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-center" style="margin-bottom: 20px;">
                    <input type="text" id="mainSearchInput" class="col-6" placeholder="Busca proyectos, donadores, etc" >
                </div>
                <div class="row justify-content-center">
                    <div class="col-6 searchResults"></div>
                </div>
            </div>
        </section>
    @endif

</div>



<section id="projectInfo" class="info-section">

    <div class="projectControls">
        <div>
            <label class="switch">
                <input type="checkbox" checked id="projectStatus_switch">
                <span class="slider round"></span>
            </label>
            <span class="pulse_live" id="projectStatus">Live</span>
        </div>

        <div style="display:none">
            <button class="btn btn-warning" disabled="true">Editar</button>
        </div>

        <div style="display:none">
            <button class="btn btn-danger" onclick="deleteProject()">Borrar</button>
        </div>

        <div>
            <a class="btn btn-info" id="viewProject" target="_blank">Ver</a>
        </div>
    </div>

    <div class="container-fluid loaderCenter" id="projectContainer">
        <button class="info-close"><img src="{{asset('images/close.png')}}" alt="close" class="close-img"></button>
        <div class="row">
            <div class="col">
                <img src="{{asset('images/adminloader.gif')}}" alt="loader" class="sr_loader_info infoloader"
                     style="display: block;">
            </div>
        </div>


        <div class="container-fluid marb-150" id="projectInfoWrapper">

            <div class="row">
                <div id="projectImg"></div>
            </div>

            <div class="row projectNameRow">
                <div class="col text-center">
                    <p id="projectName"></p>
                </div>
            </div>

            <div class="container">
                <div class="row marb-20">
                    <div class="col text-center padding-10">
                        <div class="projectBox">
                            <p class="projectBox_title">Donaciones Totales</p>
                            <p id="projectDonations"></p>
                        </div>
                    </div>
                    <div class="col text-center padding-10">
                        <div class="projectBox">
                            <p class="projectBox_title">Total Recaudado</p>
                            <p id="projectAmount"></p>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col">
                        <table id="projectTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Proyecto</th>
                                <th>Donador</th>
                                <th>Monto</th>
                                <th>Autorización</th>
                                <th>Estatus</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="projectTable_body">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>


<section id="donationInfo" class="info-section">

    <div class="container-fluid loaderCenter" id="donationContainer">
        <button class="info-close"><img src="{{asset('images/close.png')}}" alt="close" class="close-img"></button>
        <div class="row">
            <div class="col">
                <img src="{{asset('images/adminloader.gif')}}" alt="loader" class="sr_loader_donation infoloader"
                     style="display: block;">
            </div>
        </div>
        <div class="container-fluid marb-150" id="donationInfoWrapper">

            <div class="donationId">

            </div>

            <div class="container">

                <div class="row">
                    <div class="col text-center padding-10">
                        <div class="projectBox">
                            <p class="projectBox_title">Status</p>
                            <p id="donationStatus"></p>
                        </div>
                    </div>
                </div>
                <div class="row marb-20">
                    <div class="col text-center padding-10">
                        <div class="projectBox">
                            <p class="projectBox_title">Donador</p>
                            <p id="donationDonor"></p>
                        </div>
                    </div>
                    <div class="col text-center padding-10">
                        <div class="projectBox">
                            <p class="projectBox_title">Proyecto</p>
                            <p id="donationProject"></p>
                        </div>
                    </div>
                </div>


                <div class="col receiptFrame_div">
                    <p style="margin-bottom: 0;">Recibo</p>
                    <iframe src="" frameborder="0" id="receiptFrame"></iframe>
                </div>


            </div>

        </div>
    </div>
</section>


<div class="modal fade " id="notifModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog centermodal" role="document">
        <div class="modal-content">

            <div class="modal-body" id="notifText">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/datatables.min.js"></script>

<script>
    @if(!isset(Auth::user()->project_id))
    //Carga inicial de la pagina administrador
    $( function() {
        $( "#proyecto" ).prop('checked', false);
        $( "#donativo" ).prop('checked', false);
        $( "#mainSearchInput" ).css('display', 'none');
        $( "#filtros-adicionales" ).css('display', 'none');
        $( "#mainSearchInput" ).val('');
        $( ".searchResults").html('');
        $('#fromDate').val('');
        $('#toDate').val('');
        $("#rfc").prop('checked', false);
    } );

    $( "#proyecto" ).click(function() {
        $( "#donativo" ).prop('checked', false);
        $( "#filtros-adicionales" ).css('display', 'none');
        $( ".searchResults").html('');
        if(!$( "#proyecto" ).prop('checked')){
            $( "#mainSearchInput" ).css('display', 'none');
            $( "#mainSearchInput" ).val('');
        }else{
            $( "#mainSearchInput" ).css('display', 'block');
            $( "#mainSearchInput" ).val('');
        }
    });

    $( "#rfc" ).click(function() {
        $( "#mainSearchInput" ).val('');
        $( ".searchResults").html('');
    });

    $( "#donativo" ).click(function() {
        $( "#proyecto" ).prop('checked', false);
        $( ".searchResults").html('');
        if(!$( "#donativo" ).prop('checked')){
            $( "#mainSearchInput" ).css('display', 'none');
            $( "#mainSearchInput" ).val('');
        }else{
            $( "#mainSearchInput" ).css('display', 'block');
            $( "#mainSearchInput" ).val('');
            $( "#filtros-adicionales" ).css('display', 'block');
            $('#fromDate').val('');
            $('#toDate').val('');
            $("#rfc").prop('checked', false);
        }
    });

    function valoresIniciales(){
        $( "#proyecto" ).prop('checked', false);
        $( "#donativo" ).prop('checked', false);
        $( "#mainSearchInput" ).css('display', 'none');
        $( "#mainSearchInput" ).val('');
        $( ".searchResults").html('');
        $( "#filtros-adicionales" ).css('display', 'none');
        $('#fromDate').val('');
        $('#toDate').val('');
        $("#rfc").prop('checked', false);
    }

    @else
    function valoresIniciales(){
        $( "#mainSearchInput" ).val('');
        $( ".searchResults").html('');
        $('#fromDate').val('');
        $('#toDate').val('');
        $("#rfc").prop('checked', false);
    }
    @endif
</script>

<script>
    $( function() {
        $( "#fromDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
        $( "#toDate" ).datepicker({ dateFormat: 'yy-mm-dd' });
    } );
</script>


<script>
    //Yo no hice esto pero sirve mas o menos
    let url_ = "{{route('home')}}";
    var editorOptions = {
        modules: {
            toolbar: '#toolbar-container'
        },
        placeholder: 'Compose an epic...',
        readOnly: false,
        theme: 'snow'
    };
    var editor;
    var container = $('.projectEditor').get(0);
    var textInput, searchTimeout;
    let current;
    let current_id;
    let projectTable;
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    });


    $(document).ready(function () {
        
        axios.defaults.headers.common = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        };
        textInput = document.getElementById('mainSearchInput');
        searchTimeout = null;
        textInput.onkeyup = function (e) {
            let cont = $('.searchResults');

            if (textInput.value.length < 3) {
                cont.empty();
            }

            if (textInput.value.length > 2 && !!textInput.value.replace(/\s/g, '').length && textInput.value != '') {
                if ($('.sr_loader').length < 1) {
                    cont.empty();
                    turnListeners('off');
                    $('.searchResults').append('<div class="row" id="searchLoader"><div class="col"><img src="/images/loader.gif" alt="loader" class="sr_loader"></div></div>');
                }

                $('.sr_loader').fadeIn('fast');
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function () {
                    //Aqui empieeza lo bueno
                    @if(!isset(Auth::user()->project_id))
                        if($( "#proyecto" ).prop('checked')){
                            projectSearchAdmin(textInput.value);
                        }else{
                            projectSearchAdminDonativo(textInput.value);
                        }
                    @else
                        projectSearchAdminDonativoProyecto(textInput.value);
                    @endif
                }, 100);
            }
        };

        //Cerrar la ventana
        $('.info-close').click(function () {
            showSection('none');
            valoresIniciales();
        });

    });

    function projectSearchAdmin(query){
        axios.post("{{route('projects_admin')}}", {
            searchQuery: query
        })
        .then(function (response) {
            let data = response.data;
            let cont = $('.searchResults');
            setTimeout(function () {
                $('.sr_loader').fadeOut('fast', function () {
                        $('.sr_loader').remove();
                        if (data.msg != "no_results") {
                            $(data.content.projects).each(function (index, item) {
                                cont.append('<div class="row justify-content-center sr_row" data-id="'+item.id+'" data-type="project">'
                                +'<div class="searchResultInstance"><div class="sr_type">Proyecto</div><div class="iconContainer">'
                                +'<img src="/images/project.png" alt=""></div><div class="desc"><p class="sr_name">'+item.name+'</p>'
                                +'</div></div></div>');
                            });
                        }
                        turnListeners('on')
                    }
                );
            }, 300);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    function projectSearchAdminDonativo(query){
        axios.post("{{route('donaciones_admin')}}", {
            searchQuery: query,
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            rfc: $("#rfc").prop('checked')?true:false,
        })
        .then(function (response) {
            let data = response.data;
            let cont = $('.searchResults');
            setTimeout(function () {
                $('.sr_loader').fadeOut('fast', function () {
                        $('.sr_loader').remove();
                        if (data.msg != "no_results") {
                            dataFromDonativos = response.data.content;
                            cont.append('<div class="row justify-content-center sr_row" data-id="none" data-type="donation">'
                                +'<div class="searchResultInstance"><div class="sr_type">Donativo</div><div class="iconContainer">'
                                +'<img src="/images/person.png" alt=""></div><div class="desc"><p class="sr_name">'+data.content.donations.length+' Coincidencias</p>'
                                +'</div></div></div>');
                        }
                        turnListeners('on')
                    }
                );
            }, 300);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    @if(isset(Auth::user()->project_id))
    identificador = "{{Auth::user()->project_id}}";
    @else
    identificador = "1";
    @endif

    function projectSearchAdminDonativoProyecto(query){
        axios.post("{{route('donaciones_admin_proyecto')}}", {
            id:identificador,
            searchQuery: query,
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            rfc: $("#rfc").prop('checked')?true:false,
        })
        .then(function (response) {
            let data = response.data;
            let cont = $('.searchResults');
            setTimeout(function () {
                $('.sr_loader').fadeOut('fast', function () {
                        $('.sr_loader').remove();
                        if (data.msg != "no_results") {
                            dataFromDonativos = response.data.content;
                            cont.append('<div class="row justify-content-center sr_row" data-id="none" data-type="donation">'
                                +'<div class="searchResultInstance"><div class="sr_type">Donativo</div><div class="iconContainer">'
                                +'<img src="/images/person.png" alt=""></div><div class="desc"><p class="sr_name">'+data.content.donations.length+' Coincidencias</p>'
                                +'</div></div></div>');
                        }
                        turnListeners('on')
                    }
                );
            }, 300);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    function getProjects() {
        axios.post('/getprojects', {
            data: 'Fred',
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    function turnListeners(state) {
        if (state == 'on') {
            $('.sr_row').on('click', function () {
                showInfo(this);
            })
        }
        else {
            $('.sr_row').off();
        }
    }

    $('#todosLosPagos').click(function() {
        showInfo(this);
    });


    function showInfo(el) {
        let type = $(el).data('type');
        let id = $(el).data('id');
        switch (type) {
            case 'donation':
                showSection('projectInfo');
                setTimeout(function () {
                    $('#projectTable_body').html('');
                    $('#projectImg').css('background', 'url("'+url_+'/images/projectplaceholder.png")')
                    if (dataFromDonativos.donations.length > 0) {
                        $(dataFromDonativos.donations).each(function (index, item) {
                            $('#projectTable_body').append(`
                    <tr>
                    <td>${item.project.name}</td>
                    <td>${item.donor.name}
                        <br>Razón Social: ${item.razon==null?'Sin razón':item.razon}
                        <br>RFC: ${item.rfc==null?'No especificada':item.rfc}
                        <br>¿Desea CFDI?: ${item.wants_cfdi==1?'Si':'No'}
                        <br>Email: ${item.donor.email==null?'No especificado':item.donor.email}
                    </td>
                    <td> ${'$' + item.amount} MXN</td>
                    <td>No: ${item.authorization}<br>Id: ${item.processor_id}</td>
                    <td>Completado</td>
                    <td>${item.created_at}</td>
                    <td><a href="${url_ + '/receipt/' + item.id}" class="receiptBtn" target="_blank">Recibo</a></td>
                    </tr>`)
                        })
                    }
                    initTable();
                    $('#projectName').text('Resultados de la busqueda');
                    $('#projectImg').css('display', 'none');
                    $('.projectBox').css('display', 'none');
                    $('#projectDonations').css('display', 'none');
                    $('#projectAmount').css('display', 'none');
                    $('#viewProject').css('display', 'none');
                    $('.projectControls').css('display', 'none');
                    $('#projectStatus_switch').css('display', 'none');
                    true ? setProjectDOMStatusOn() : setProjectDOMStatusOff();
                    $('.infoloader').fadeOut('fast', function () {
                        $('#projectContainer').removeClass('loaderCenter');
                        $('#projectInfoWrapper').fadeIn();
                        current = 'project';
                        current_id = '1';
                    })
                }, 300)
            break;
            case 'project':
                showSection('projectInfo');
                axios.post("{{route('projects_admin_info')}}", {
                    project_id: id,
                })
                    .then(function (response) {
                        let data = response.data.content;
                        setTimeout(function () {
                            $('#projectTable_body').html('');
                            if (!data.project.main_media) {
                                $('#projectImg').css('background', 'url("'+url_+'/images/projectplaceholder.png")')
                            }
                            else {
                                $('#projectImg').css('background', 'url("'+url_+'/storage/' + data.project.main_media + '")');
                            }
                            if (data.donations.length > 0) {
                                $(data.donations).each(function (index, item) {
                                    $('#projectTable_body').append(`
                            <tr>
                            <td>${data.project.name}</td>
                            <td>${item.donor_name}
                                <br>Razón Social: ${item.razon==null?'Sin razón':item.razon}
                                <br>RFC: ${item.rfc==null?'No especificada':item.rfc}
                                <br>¿Desea CFDI?: ${item.wants_cfdi==1?'Si':'No'}
                                <br>Email: ${item.donor_email==null?'No especificado':item.donor_email}
                            </td>
                            <td> ${'$' + item.amount} MXN</td>
                            <td>No: ${item.authorization}<br>Id: ${item.processor_id}</td>
                            <td>Completado</td>
                            <td>${item.date_parsed}</td>
                            <td><a href="${url_ + '/receipt/' + item.id}" class="receiptBtn" target="_blank">Recibo</a></td>
                            </tr>`)
                                })
                            }
                            initTable();
                            $('#projectName').text(data.project.name);
                            $('#projectDonations').text(data.total_donations);
                            $('#projectAmount').text(formatter.format(data.total_amount));
                            $('#viewProject').attr('href', url_ + '/project/' + data.project.id)
                            $('.projectControls').fadeIn('fast');
                            data.project.is_live ? $('#projectStatus_switch').prop('checked', true) : $('#projectStatus_switch').prop('checked', false);
                            data.project.is_live ? setProjectDOMStatusOn() : setProjectDOMStatusOff();
                            $('.infoloader').fadeOut('fast', function () {
                                $('#projectContainer').removeClass('loaderCenter');
                                $('#projectInfoWrapper').fadeIn();
                                current = 'project';
                                current_id = data.project.id;
                            })
                        }, 300)

                    })
                    .catch(function (error) {
                        console.log(error);
                        showNotif('Ocurrió un error')
                    });

                break;

        }


    }


    function showSection(w) {

        $('.info-section').css('left', '-100%');
        if (w !== 'none') {
            $('#' + w).css('left', '0');
        }
        else {
            switch (current) {
                case 'project':
                    setTimeout(function () {
                        $('#projectContainer').addClass('loaderCenter');
                        $('.infoloader').show();
                        $('#projectInfoWrapper').hide();
                        $('#projectImg').css('background', 'url("images/projectplaceholder.png")');
                        $('.projectControls').fadeOut('fast');
                        projectTable.destroy();
                    }, 230);
                    break;
                case 'donation':
                    setTimeout(function () {
                        $('#donationContainer').addClass('loaderCenter');
                        $('.sr_loader_donation').show();
                        $('#donationInfoWrapper').hide();
                    }, 230);
                    break;
                case 'donor':
                    break;
            }
        }

    }

    function displayText(preParsedArray) {
        let textHtml;
        let textArray = JSON.parse(preParsedArray);

        $(textArray.ops).each(function (ind, it) {

            if (it.attributes) {
                console.log("---init")
                $(Object.keys(it.attributes)).each(function (ind_, it_) {
                    switch (it_) {
                        case 'bold':
                            console.log("boooold");
                            break;
                        case 'italic':
                            console.log("italiccccccc")
                            break;

                    }

                });
                console.log("---end obj")

            }


            else {
                textHtml += '';
            }

        })


    }

    Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };


    function imgSize() {
        var myImg = document.querySelector("#sky");
        var realWidth = myImg.naturalWidth;
        var realHeight = myImg.naturalHeight;
        alert("Original width=" + realWidth + ", " + "Original height=" + realHeight);
    }

    function openFile() {

    }

    function initTable() {
        projectTable = $('#projectTable').DataTable({
            "ordering": false,
            language: {url: url_+'/table.json'}
        });
    }

    function changeProjectStatus(status) {

        axios.post('/projectstatus', {
            type: current,
            type_id: current_id,
            status: status,
        })
            .then(function (response) {
                if (response.data.msg === 'saved') {
                    if (status === 'not-live') {
                        setProjectDOMStatusOff()
                    }
                    else {
                        setProjectDOMStatusOn()
                    }
                }
                else {
                    showNotif('Ocurrió un error')
                }

            })
            .catch(function (error) {
                console.log(error);
            });
    }


    function showNotif(msg) {
        $('#notifText').text(msg);
        $('#notifModal').modal();
    }

    function setProjectDOMStatusOff() {
        $('#projectStatus').text('Off');
        $('#projectStatus').removeClass('pulse_live');
        $('#projectStatus').addClass('pulse_off');
    }

    function setProjectDOMStatusOn() {
        $('#projectStatus').text('Live');
        $('#projectStatus').addClass('pulse_live');
        $('#projectStatus').removeClass('pulse_off');
    }

    function deleteProject() {
        if (confirm("¿Deseas BORRAR este proyecto?")) {
            axios.post('/deleteproject', {
                type: current,
                type_id: current_id,
            })
                .then(function (response) {
                    window.location.reload();

                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }


</script>
</html>



