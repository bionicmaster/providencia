@extends('voyager::master')

@section('content')
<div align="center">
    <h1 style="text-align::center">Descarga de excel de donativos</h1>
    @if(!isset(Auth::user()->project_id))
        <a href="{{ route('excel_dontaion', ['id'=>'all']) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>
    @else
        <a href="{{ route('excel_dontaion', ['id'=>Auth::user()->project_id]) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>
    @endif

    @if(!isset(Auth::user()->project_id))
        <h1 style="text-align::center">Descarga de excel de newsletters</h1>
        <a href="{{ route('excel_newsletter', ['id'=>'all']) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>

        <h1 style="text-align::center">Descarga de excel de solicitudes</h1>
        <a href="{{ route('excel_solicitud', ['id'=>'all']) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>
    @endif

    @if(!isset(Auth::user()->project_id))
        <h1 style="text-align::center">Descarga de excel de proyectos</h1>
        <a href="{{ route('excel_proyecto', ['id'=>'all']) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>
    @else
        <h1 style="text-align::center">Descarga de excel del Proyecto</h1>
        <a href="{{ route('excel_proyecto', ['id'=>Auth::user()->project_id]) }}" target="_blank" class="btn btn-info">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            Descargar
        </a>
    @endif
</div>
@endsection