@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Videos" /> 
    <meta name="keywords" content="providencia, Providencia, Desarrollo Social, Educación, Salud, Nutrición"/>
    <meta property="og:url" content="https://www.providencia.org.mx/video/" />
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
    <title>Videos | Providencia</title>
@endsection

@section('content')

@include('layout.navigation')


<!-- HEADER -->
<section class="header">
    <div class="header-fade">
        <div class="header-slider header-blog">
            <div class="container">
                <div class="row">
                    <div class="col-12 pd-0 pt-5 text-right">
                        
                        <h2 style=" line-height: .9;
                        margin-top: 60px;
                        margin-left: 100px;
                        font-family: roboto-bold,Arial,Helvetica,sans-serif;
                        font-weight: 700;
                        font-size: 59px !important;" class="providencia-header_title text-white">Videos</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Aqui haces tus maravillas miriam-->
<section  class="videos-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-12 videos-border">
                <ul class="video-tabs-nav ">
                    @foreach ($categorias as $english =>$categoria)
                        <li class="espanol">
                            <a id="{{strtolower(str_replace(' ', '', trim($categoria)))}}" onclick="section('{{strtolower(str_replace(' ', '', trim($categoria)))}}')">{{$categoria}}</a>   
                        </li> 
                        <li class="english">
                            <a id="{{strtolower(str_replace(' ', '', trim($categoria)))}}" onclick="section('{{strtolower(str_replace(' ', '', trim($categoria)))}}')">{{$english}}</a>   
                        </li> 
                    @endforeach
                    
                </ul>
            </div>
            {{-- seccion videos --}}
            <?php $inicio=1; ?>
            @foreach ($categorias as $categoria)
                <?php $videosttl=1; ?>
                <div class="col-sm-8 col-12 video" id="{{strtolower(str_replace(' ', '', trim($categoria)))}}div" @if ($inicio !=1) style="display: none;" @endif>
                    <div class="row" >
                        @foreach ($videos as $video)
                            @if ($video->categoria ==$categoria)
                                <div class="col-12 offset-lg-2 col-lg-8 yotu-title visor @if ($videosttl ==1) primero @endif" @if ($videosttl !=1) style="display: none;" @endif id="video{{$video->id}}">
                                    <p class="espanol">{{$video->nombre}}</p>
                                    <p class="english">{{$video->nombre_english}}</p>
                                    <div class="youtu-player">
                                        <iframe class="yotu-video-placeholder" id="yotu-player-5d6fec7fe422c" frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" width="1000" height="600" src="{{$video->video_url}}?controls=1&amp;modestbranding=1&amp;loop=0&amp;autonext=1&amp;showinfo=1&amp;rel=0&amp;thumbnails=0&amp;cc_load_policy=0&amp;cc_lang_pref=0&amp;hl=0&amp;iv_load_policy=0&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fcrowdfunding.providencia.org.mx&amp;widgetid=1"></iframe>
                                    </div> 
                                </div>  
                            <?php $videosttl+=1; ?>
                            @endif
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="yotu-videos">
                            <ul class="pt-3">
                                @foreach ($videos as $video)
                                    @if ($video->categoria ==$categoria)
                                        <li class=" yotu-first col-lg-3 col-sm-12  col-md-6">
                                            <a class="yotu-video" onclick="visor('{{$video->id}}')">
                                                <div class="yotu-video-thumb-wrp">
                                                    <div>
                                                        <img class="yotu-video-thumb" src="https://providencia.org.mx/storage/{{$video->imagen}}" alt="GiveOrg - 1. Sección educativa de GiveOrg (Subtitulado)">
                                                    </div>
                                                </div>
                                                <h3 class="yotu-video-title espanol">{{\Illuminate\Support\Str::words($video->nombre, 4)}}</h3>
                                                <h3 class="yotu-video-title english">{{\Illuminate\Support\Str::words($video->nombre_english, 4)}}</h3>
                                            </a>
                                        </li>   
                                    @endif
                                @endforeach    
                            </ul>
                        </div>
                    </div>
                </div>
            <?php $inicio+=1; ?>
            @endforeach
        </div>
    </div>
</section>

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection

@section('js')
    <script>
        function section(seccion){
            $(".video").css('display', 'none');
            $("#"+seccion+"div").css('display', 'block');
            $(".primero").css('display', 'block');
        }
        function visor(video){
            $(".visor").css('display', 'none');
            $("#video"+video).css('display', 'block');
        }
    </script>
@endsection