@extends('layout.master')
@section('head')
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="¿Que hacemos?" /> 
    <meta name="keywords" content="providencia, Providencia, misión, Misión, Visión, visión, que hacemos, Hacemos, hacemos"/>
    <meta property="og:url" content="https://www.providencia.org.mx" />
    <meta property="og:site_name" content="Providencia" /> 
    <link rel="icon" href="{{asset('favicon.png')}}" type="image/x-icon"/>
    <meta property="og:image" content="{{asset('logo.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}" />
    <title>¿Que hacemos? | Providencia</title>
@endsection


@section('content')

@include('layout.navigation')

<!-- HEADER -->
<section>
  <div class="providencia-header providencia-header--que-hacemos">
  </div>
</section>


<!-- DESCRIPTION -->
<section class="que-hacemos">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center pt-10 pb-10">
        <img class="que-hacemos_img pb-6" src="{{asset('/images/logo-providencia-2.png')}}" alt="Logo providencia fundación">
        <p class="que-hacemos_description providencia-text mx-auto espanol">Fundación Providencia <br> Es una fundación que desarrolla y opera herramientas tecnológicas, con el objetivo de establecer estrategias innovadoras. Lo anterior permite recaudar donativos para proveer de recursos a diversas instituciones y causas que tienen como fin, el impactar positivamente a la sociedad.</p>
        <p class="que-hacemos_description providencia-text mx-auto english">Providence Foundation <br> It is a foundation that develops and operates technological tools, with the aim of establishing innovative strategies. The foregoing allows raising donations to provide resources to various institutions and causes whose purpose is to positively impact society. </p>
      </div>
    </div>
  </div>
</section>

<!-- MISION - VISION -->
<section class="bg-blue que-hacemos text-white text-center">
  <div class="container">
    <div class="row">
      <div class="col-12 pt-6 pb-6">
        <h2 class="pb-2 bold espanol">Misión</h2>
        <p class="que-hacemos_mision-text mx-auto espanol">Ser la plataforma que multiplica el beneficio de las causas que impulsa, creando un vínculo tangible entre las organizaciones de la sociedad civil y la sociedad.</p>
        <h2 class="pt-3 pb-2 bold espanol">Visión</h2>
        <p class="que-hacemos_mision-text mx-auto espanol">Ser un vínculo entre corazones generosos y causas nobles apoyando, fomentando y difundiendo las buenas obras que transforman personas, ciudades y naciones.</p>
        <h2 class="pb-2 bold english">Mission</h2>
        <p class="que-hacemos_mision-text mx-auto english">To be the platform that multiplies the benefit of the causes it promotes, creating a tangible link between civil society organizations and society.</p>
        <h2 class="pt-3 pb-2 bold english">Overview</h2>
        <p class="que-hacemos_mision-text mx-auto english">To be a link between generous hearts and noble causes, supporting, promoting and spreading the good works that transform people, cities and nations.</p>
      </div>
    </div>
  </div>
</section>

<!-- ACERCA DE -->
<section class="que-hacemos">
  <div class="container">
    <div class="row">
      <div class="col-12 providencia-text text-center pt-10 pb-10 espanol">
        <p>Acerca de Nuestra Plataforma <br>Fundación Providencia desarrolla la primera plataforma de crowdfunding 100% filantrópica de México que no cobra comisión por las transacciones. Conectamos héroes con organizaciones de la sociedad civil para promover sus causas, obtener recursos que multipliquen su impacto social y fortalecimiento en las áreas administrativas, legal y de comunicación.</p>
      </div>
      <div class="col-12 providencia-text text-center pt-10 pb-10 english">
        <p>About Our Platform <br>Fundación Providencia develops the first 100% philanthropic crowdfunding platform in Mexico that does not charge a commission for transactions. We connect heroes with civil society organizations to promote their causes, obtain resources that multiply their social impact and strengthen administrative, legal and communication areas..</p>
      </div>
    </div>
  </div>
</section>

<!-- BEPART -->
@include('layout.bepart')

<!-- PAYMENT -->
@include('layout.payments')

<!-- FOOTER -->
@include('layout.footer')

@endsection