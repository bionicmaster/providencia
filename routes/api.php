<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Generate payment Transfer and stores
Route::post('/genera/ficha', 'PaymentController@generaFicha')->name('genera_ficha');

//Pago con tarjeta
Route::post('/card/payment','PaymentController@card')->name('card');

//Suscripción
Route::post('/suscripcion/payment','PaymentController@suscripcion')->name('suscripcion');

//guardar webhook
Route::post('/save/webhook', 'PaymentController@hook')->name('hook');