<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Administrator
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Home
Route::get('/', 'HomeController@index')->name('home');

//Newsletter
Route::post('/newsletter', 'HomeController@newsletter')->name('newsletter');

//Blogs
Route::get('/blogs', 'HomeController@blogs')->name('blogs');
Route::get('/blog/{id}', 'HomeController@blog')->name('blog');

//What we do
Route::get('/whatwedo', function () {return view('whatwedo');})->name('whatwedo');

//How to donate
Route::get('/howtodonate', function () {return view('howtodonate');})->name('howtodonate');

//How to upload a project
Route::get('/howtouploadaproject', function () {return view('howtouploadaproject');})->name('howtouploadaproject');

//Faq
Route::get('/faq', function () {return view('faq');})->name('faq');

//Legales //TODO falta traducir esto
Route::get('/legal', function () {return view('legal');})->name('legal');

//Upload form
Route::get('/upload/form', function () {return view('form');})->name('upload_form');
Route::post('/save/form','FormController@form')->name('save_form');

//I'm providence
Route::get('/iamprovidence', 'HomeController@iamprovidence')->name('iamprovidence');

//videos //TODO Falta traducirlo
Route::get('/video', 'HomeController@video')->name('video');

//Categorias
Route::get('/proyectos/{category}', 'HomeController@category')->name('category');

//crowfunding
Route::get('/crowdfunding', function () {return view('crowdfunding');})->name('crowdfunding');

//Donation
Route::get('/donative', 'PaymentController@donative')->name('donative');


//Checar donativos Administrador
Route::post('/projects/admin', 'AdminController@proyectos')->name('projects_admin');
Route::post('/projects/donaciones', 'AdminController@donaciones')->name('donaciones_admin');
Route::post('/projects/projectinfo', 'AdminController@getProjectInfo')->name('projects_admin_info');
Route::post('/projects/getProjectInfoDonacion', 'AdminController@getProjectInfoDonacion')->name('donaciones_admin_info');
Route::post('/projects/donaciones/proyecto', 'AdminController@donacionesProyecto')->name('donaciones_admin_proyecto');


Route::get('/receipt/{don_id}', 'AdminController@viewReceipt');

//Thanks
Route::get('/thanks', function () {return view('thanks');})->name('thanks');

//Projects
Route::get('project/{id}', 'HomeController@project')->name('project');

//rutas para exceles
Route::get('/excel/donation/{id}', 'ExcelController@donation')->name('excel_dontaion');
Route::get('/excel/newsletter/{id}', 'ExcelController@newsletter')->name('excel_newsletter');
Route::get('/excel/proyecto/{id}', 'ExcelController@proyecto')->name('excel_proyecto');
Route::get('/excel/solicitud/{id}', 'ExcelController@solicitud')->name('excel_solicitud');

//Obtener pagos de Openpay
Route::get('/getDonationOpenpay', 'PaymentController@getDonationOpenpay')->name('getDonationOpenpay');

//Route for any other case not considered on this
Route::get('/{url}', 'HomeController@project')->name('project_url');
